@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
        #body {
            visibility: hidden;
        }
        


    </style>
@stop
@section('content')

    <section id="particle-slider">
        <link href='https://fonts.googleapis.com/css?family=Bungee' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="/css/kavv/vhs.css" >
        <!-- <script src="/js/kavv/vhs.js"></script> -->

        <div class="title">
            <h1>THIS IS<br/>CS50X.ni</h1>

        </div>
        <!-- <div class="slides">
            <div id="first-slide" class="slide" data-src="./img/CS50x-medium.png">
            </div>
            <div id="div-aux" data-src="./img/CS50x-small.png">
            </div>
        </div>
        <canvas class="draw" width="1707" height="850"></canvas>

        <script>
            var $small = document.getElementById("div-aux");
            var $medium = document.getElementById("first-slide");
            var isMobile;
            var isSmall;

            function window_size()
            {

                isMobile = navigator.userAgent &&
                    navigator.userAgent.toLowerCase().indexOf('mobile') >= 0;
                isSmall = window.innerWidth < 800;
            }

            function need_change($div)
            {
                if($div.id != "first-slide")
                {
                    //Removemos las clases y id
                    $small.removeAttribute("id");
                    $small.removeAttribute("class");
                    $medium.removeAttribute("id");
                    $medium.removeAttribute("class");

                    //Asignamos el id y clases que antes tenia el div que se estaba mostrando
                    $div.setAttribute("id","first-slide");
                    $div.setAttribute("class","slide");
                    
                    ps.start();
                    change_force();
                    return true;
                }
                return false;
            }

            window.onresize = function(){
                window_size();
                confirmation();
            }

            function confirmation()
            {
                if (isMobile || isSmall) 
                {
                    need_change($small)
                }
                else
                {
                    need_change($medium)
                }
            }

            var ps;
            function firstime() {
                //Identificamos el tamaÃ±o de la pantalla
                window_size();
                //Inicializamos la particula
                ps = new ParticleSlider({
                    ptlGap: isMobile || isSmall ? 2 : 1,
                    ptlSize: isMobile || isSmall ? 2 : 2,
                    width: 1e9,
                    height: 1e9,
                    mouseForce: isMobile || isSmall ? 5000 : 10000
                });
                //Evaluamos la imagen que se utilizara y damos inicio
                if(!confirmation()){
                    ps.start();
                }
            }
            function change_force() {
                ps.mouseForce = isMobile || isSmall ? 5000 : 10000;
            }

            window.onload = function(){
                firstime();
            };
            

        </script>
        <script src="/js/kavv/CS50x.js" type="text/javascript"></script>

        <canvas width="800" height="142" style="display: none;"></canvas><canvas style="display: none;"></canvas><canvas style="display: none;"></canvas>
 -->
    </section>


@stop
@section('script')
<script>
    $(document).ready(function(){
        $.get("/secure", function(res){
            $("#body").css("visibility","visible");
        }).fail(function() {
            location.href = "/"; 
        });
    });
</script>
@stop