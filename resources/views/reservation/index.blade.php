@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
        .t_button {
            transition: .3s;
        }
        .space-0{
            background: #f25656!important;
        }
        .oh{
            background: #baefff;
        }
        .oh-block {
            background: #cbcbcb;
        }
        .reserved{
            background: #a0e0a0;
            cursor: pointer;
        }
        .reserved:before{
            content: "Reservaste";
        }
        .reserved:hover {
            background: #f25656;
            color: white;
        }
        .reserved:hover:before {
            content: "Eliminar";
        }
        table td, table th {
            border: 1px solid #0000005c!important;
        }
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display:none;
        }
        .td-black
        {
            color: #fff;
            background: #000000;
            border-bottom: 1.4px solid #ffffff!important;
        }
        .th-gray{
            color:#fff;
            background: #3b3b3b;
        }
        .dates {
            background-color: white;
            box-shadow: 2px 2px 20px 0px black;
            border-radius: 25px;
        }
        .cabecera{
            color: #fff;
            font-family: inherit;
            text-shadow: 3px 5px 5px black;
        }
        .tday{
            box-shadow: 4px 4px 15px 0px black;
        }
    </style>
@stop
@section('content')
    <section style="background: #626262;">
        <div class="row text-center py-3">
            <h2 for="" class="cabecera col-md-12">Estas reservaciones son para las fechas</h2>
        </div>
        <div class="row text-center pb-3">
            <div class="col-md-5 dates"><h3>Desde: {{$start_week}}</h3></div>
            <div class="col-md-2"></div>
            <div class="col-md-5 dates"><h3>Hasta: {{$close_week}}</h3></div>
        </div>

        <div class="row my-3 d-flex justify-content-center">
            <div class="col-md-6">
                <button class="btn-block btn btn-warning" data-toggle="modal" data-target="#modal-info"><strong>¿Cómo eliminar mis reservas?</strong></button>
            </div>
        </div>
        @foreach($hours as $day=>$hour)
            <?php $quantity_block = count($hour["block"]);
                if($day == "Mo")
                    $day = "Lunes";
                else if($day == "Tu")
                    $day = "Martes";
                else if($day == "We")
                    $day = "Miércoles";
                else if($day == "Th")
                    $day = "Jueves";
                else if($day == "Fr")
                    $day = "Viernes";
                else if($day == "Sa")
                    $day = "Sábado";
            ?>
            <div class="row">
                <table id="tday_{{$hour['day_id']}}" data-day={{$day}} class="table table-bordered tday">
                    <thead class="text-center">
                        <tr>
                            <th colspan="5" class="td-black"><h4>{{$day}}</h4></th>
                        </tr>
                        <tr>
                            <th colspan="1" class="td-black"></th>
                            <th colspan="2" class="th-gray">Laboratorio</th>
                            <th colspan="2" class="th-gray">Aula</th>
                        </tr>
                        <tr>
                            <th colspan="1" class="td-black">HORA</th>
                            <th colspan="1" class="th-gray">Computadoras CS50X.ni</th>
                            <th colspan="1" class="th-gray">Laptop personal</th>
                            <th colspan="1" class="th-gray">Computadoras CS50X.ni</th>
                            <th colspan="1" class="th-gray">Laptop personal</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @for($i = 0; $i < $quantity_block; $i++)         
                            @if($hour["hour_id"][$i] != 6)
                                <tr>
                                
                                    <?php 
                                        $LT = null; 
                                        $LL = null;
                                        $AL = null;
                                        $ALP = null;
                                        $day_id = $hour["day_id"];
                                        $hour_id = $hour["hour_id"][$i];
                                        $key = "day_".$day_id."hour_".$hour_id;
                                        $key_LT = $key."location_LT";
                                        $key_LL = $key."location_LL";
                                        $key_AL = $key."location_AL";
                                        $key_ALP = $key."location_ALP";
                                        $reserved;
                                        if(array_key_exists($key_LT, $reservations))
                                        {
                                            $LT = "reserved";
                                        }
                                        if(array_key_exists($key_LL, $reservations))
                                        {
                                            $LL = "reserved";
                                        }
                                        if(array_key_exists($key_AL, $reservations))
                                        {
                                            $AL = "reserved";
                                        }
                                        if(array_key_exists($key_ALP, $reservations))
                                        {
                                            $ALP = "reserved";
                                        }
                                    ?>
                                    <td class="td-black">{!!$hour["block"][$i]!!}</td>   
                                    @if($hour["active_lt"][$i] == false)
                                        <td class="oh-block " >-</td>
                                    @else
                                        <td class="oh t_button {{$LT}}" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="LT" data-aux="Laborato Terminal"></td>
                                    @endif   
                                    @if($hour["active_llp"][$i] == false)
                                        <td class="oh-block " >-</td>
                                    @else
                                        <td class="oh t_button {{$LL}}" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="LL" data-aux="Laborato Laptop"></td>
                                    @endif    
                                    @if($hour["active_al"][$i] == false)
                                        <td class="oh-block " >-</td>
                                    @else
                                        <td class="oh t_button {{$AL}}" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="AL" data-aux="Aula Laptop"></td>
                                    @endif    
                                    @if($hour["active_alp"][$i] == false)
                                        <td class="oh-block " >-</td>
                                    @else
                                        <td class="oh t_button {{$ALP}}" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="ALP" data-aux="Laptop Personal"></td>
                                    @endif      
                                </tr>
                            @endif
                        @endfor
                    </tbody>
                </table>
            </div>
        @endforeach
    </section>

    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title">Eliminar Asistencia</h5>
                </div>
                <div class="modal-body">
                    <div id="mensaje"></div>
                    <button type="button" id="btn-delete" class="btn btn-danger btn-block" onclick="eliminar();">Eliminar</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="infomodal" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title">Información</h5>
                </div>
                <div class="modal-body">
                    <div class="row pb-3">
                        <div class="col-md-12">
                            <ol>
                                <li>Las celdas se ponen de color verde cuando usted ya ha reservado en ese bloque.</li>
                                <li>Al posicionarce sobre esas celdas se pondran en rojo y diran "Eliminar".</li>
                                <li>Al dar click sobre la celda se mostrara una ventana emergente con un boton rojo que dice "eliminar".</li>
                                <li>Si realente desea eliminar esta reserva entonces de click sobre el boton de eliminar.</li>
                                <li>Requisitos para eliminar una reservación:</li>
                                <ol style="list-style: lower-alpha;">
                                    <li>La eliminación debe realizarse un día antes del día en el que ha reservado.</li>
                                </ol>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.cargando.cargando')

    @include("layouts/validation-viewport")

@stop
@section('script')
<script>
    var data;
    $(document).ready(function(){
        $.get("/secure", function(res){
            $("body").css("display","block");
        }).fail(function() {
            location.href = "/"; 
        });
    });
    
    $(".reserved").click(function(){
        $("#mensaje").slideUp();
        $("#mensaje").empty();
        $("#modal-delete").modal("show");

        data = $(this).data();
    });
    function eliminar()
    {
        var ruta = "/reservation/remove";
        $("#mensaje").slideUp();
        $("#btn-delete").attr("disabled",true);
        $("#loading").css('display','block');
        return $.ajax({
            url: ruta,
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            type: 'POST',
            dataType: 'json',
            data:data,
            success: function(res){

                if(res.code == 200)
                {
                    message(res.message,{manual:true,tipo:"success"});
                    location.href = "/reservation";
                }
                else
                {
                    message(res.message,{manual:true,tipo:"danger"});
                    $("#loading").css('display','none');
                    $("#btn-delete").attr("disabled",false);
                }

                $("#mensaje").slideDown();
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            $("#loading").css('display','none');
            $("#btn-delete").attr("disabled",false);
            message(jqXHR,{tipo:"danger"});
            $("#mensaje").slideDown();
        });
    }
</script>
@stop