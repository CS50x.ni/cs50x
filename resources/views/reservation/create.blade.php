
@extends('layouts.dashboard.dashboard')

@section('style')
    <style>
        .t_button {
            cursor: pointer;
            transition: .3s;
        }
        .space-0{
            background: #f25656!important;
        }
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display: none;
        }
        .oh{
            background: #fff;
            color:black;
        }
        .oh:hover{
            background: #baefff;
        }
        .oh-block {
            background: #cbcbcb;
        }
        .td-black
        {
            color: #fff;
            background: #000000;
            border-bottom: 1.4px solid #ffffff!important;
        }
        .th-gray{
            color:#fff;
            background: #3b3b3b;
        }
        .dates {
            background-color: white;
            box-shadow: 2px 2px 20px 0px black;
            border-radius: 25px;
        }
        .cabecera{
            color: #fff;
            font-family: inherit;
            text-shadow: 3px 5px 5px black;
        }
        .tday{
            box-shadow: 4px 4px 15px 0px black;
        }
    </style>
@stop

@section('content')
    <section style="background: #626262;">
        <div class="row text-center py-3">
            <h2 for="" class="cabecera col-md-12">Estas reservaciones son para las fechas</h2>
        </div>
        <div class="row text-center pb-3">
            <div class="col-md-5 dates"><h3>Desde: {{$start_week}}</h3></div>
            <div class="col-md-2"></div>
            <div class="col-md-5 dates"><h3>Hasta: {{$close_week}}</h3></div>
        </div>

        <div class="row my-3 d-flex justify-content-center">
            <div class="col-md-6">
                <button class="btn-block btn btn-warning" data-toggle="modal" data-target="#modal-info"><strong>¿Cómo reservar mis OH?</strong></button>
            </div>
        </div>

        @foreach($hours as $day=>$hour)
            <?php $quantity_block = count($hour["block"]);
                if($day == "Mo")
                    $day = "Lunes";
                else if($day == "Tu")
                    $day = "Martes";
                else if($day == "We")
                    $day = "Miércoles";
                else if($day == "Th")
                    $day = "Jueves";
                else if($day == "Fr")
                    $day = "Viernes";
                else if($day == "Sa")
                    $day = "Sábado";
            ?>
            <div class="row">
                <table id="tday_{{$hour['day_id']}}" data-day={{$day}} class="table table-bordered tday">
                    <thead class="text-center">
                        <tr>
                            <th colspan="5" class="td-black"><h4>{{$day}}</h4></th>
                        </tr>
                        <tr>
                            <th colspan="1" class="td-black"></th>
                            <th colspan="2" class="th-gray">Laboratorio</th>
                            <th colspan="2" class="th-gray">Aula</th>
                        </tr>
                        <tr>
                            <th colspan="1" class="td-black">HORA</th>
                            <th colspan="1" class="th-gray">Computadoras CS50X.ni</th>
                            <th colspan="1" class="th-gray">Laptop personal</th>
                            <th colspan="1" class="th-gray">Computadoras CS50X.ni</th>
                            <th colspan="1" class="th-gray">Laptop personal</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @for($i = 0; $i < $quantity_block; $i++)         
                            @if($hour["hour_id"][$i] != 6)
                                <tr>
                                    <td class="td-black">{!!$hour["block"][$i]!!}</td>   
                                    <?php 
                                        $LT = 30; 
                                        $LL = 15;
                                        $AL = 15;
                                        $ALP = 30;
                                        $day_id = $hour["day_id"];
                                        $hour_id = $hour["hour_id"][$i];
                                        $key = "day_".$day_id."hour_".$hour_id;
                                        $key_LT = $key."location_LT";
                                        $key_LL = $key."location_LL";
                                        $key_AL = $key."location_AL";
                                        $key_ALP = $key."location_ALP";
                                        if(array_key_exists($key_LT, $reservations))
                                        {
                                            $aux = $reservations[$key_LT]["space_used"]*1;
                                            $LT = $LT - $aux;
                                        }
                                        if(array_key_exists($key_LL, $reservations))
                                        {
                                            $aux = $reservations[$key_LL]["space_used"]*1;
                                            $LL = $LL - $aux;
                                        }
                                        if(array_key_exists($key_AL, $reservations))
                                        {
                                            $aux = $reservations[$key_AL]["space_used"]*1;
                                            $AL = $AL - $aux;
                                        }
                                        if(array_key_exists($key_ALP, $reservations))
                                        {
                                            $aux = $reservations[$key_ALP]["space_used"]*1;
                                            $ALP = $ALP - $aux;
                                        }
                                    ?>
                                    {{--Todo este if se puede mejorar un monton pero con tiempo y calma--}}
                                
                                    @if($hour["active_lt"][$i] == false)
                                        <td class="oh-block" >-</td>
                                    @else
                                        <td class="oh t_button space-{{$LT}}" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="LT" data-aux="Laborato Terminal">Disponible: {{$LT}}</td>
                                    @endif    
                                    @if($hour["active_llp"][$i] == false)
                                        <td class="oh-block" >-</td>
                                    @else
                                        <td class="oh t_button space-{{$LL}}" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="LL" data-aux="Laborato Laptop">Disponible: {{$LL}}</td>
                                    @endif    
                                    @if($hour["active_al"][$i] == false)
                                        <td class="oh-block" >-</td>
                                    @else
                                        <td class="oh t_button space-{{$AL}}" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="AL" data-aux="Aula Laptop">Disponible: {{$AL}}</td>
                                    @endif    
                                    @if($hour["active_alp"][$i] == false)
                                        <td class="oh-block" >-</td>
                                    @else
                                        <td class="oh t_button space-{{$ALP}}" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="ALP" data-aux="Laptop Personal">Disponible: {{$ALP}}</td>
                                    @endif     
                                </tr>
                            @endif
                        @endfor
                    </tbody>
                </table>
            </div>
            
        @endforeach
    </section>

    <!-- Modal -->
    <div class="modal fade" id="modal-reservation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title"></h5>
                </div>
                <div class="modal-body">
                    <div id="mensaje"></div>
                    <button type="button" id="btn-sent" class="btn btn-success btn-block" onclick="save();">RESERVAR</button>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>
    

    <div class="modal fade" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="infomodal" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title">Información</h5>
                </div>
                <div class="modal-body">
                    <div class="row pb-3">
                        <div class="col-md-12">
                            <ol>
                                <li>Cada tabla es equivalente a un día.</li>
                                <li>Cada tabla tiene dos locaciones: Laboratorio y Aula.</li>
                                <li>La primer columna de cada tabla es la hora por bloque de OH.</li>
                                <li>Si la celda tiene un "-" es porqué no se puede reservar en ese bloque.</li>
                                <li>Cada celda especifica la cantidad de espacios disponibles.</li>
                                <li>No se pueden realizar reservaciones en días que ya pasarón.</li>
                                <li>Si existe disponibilidad en un bloque de OH y quiere reservarlo:</li>
                                <ol style="list-style: lower-alpha;">
                                    <li>Usted debe dar <strong>click</strong> sobre la celda correspondiente.</li>
                                    <li>Se abrira una ventana con un boton verde.</li>
                                    <li>Dar click al boton verde si realmente desea realizar la reservación.</li>
                                </ol>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>
    
    @include("layouts/validation-viewport")
    @include('layouts.cargando.cargando')
    
@stop
@section('script')
<script>

    let x;
    let data;
    $(document).ready(function(){

        
        $.get("/secure", function(res){
            $("body").css("display","block");
        }).fail(function() {
            location.href = "/"; 
        });
        
        /* $(".oh").css("background","#a0e0a0");
        $(".oh-block").css("background","#cbcbcb"); */
        $oh_off = $(".oh:contains('Disponible: 0')");
        $oh_off.css("background","#f25656");
        $oh_off.removeClass("oh");
        

        $(".oh").click(function(){
            data = $(this).data();
            var day = $("#tday_"+data.day).data().day;
            $("#m-title").text(day +" "+ $(this).siblings()[0].textContent +" "+ data.aux);

            $("#mensaje").empty();

            $("#modal-reservation").modal("show");
            x = $(this);
        });
    });


    function save()
    {
        var ruta = "/reservation";
        
        $("#mensaje").slideUp();
        $("#btn-sent").attr("disabled",true);
        $("#loading").css("display","block");
        return $.ajax({
            url: ruta,
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            type: 'POST',
            dataType: 'json',
            data:data,
            success: function(res){
                if(res.code === 1)
                {
                    message(res.message,{manual:true,tipo:"success"});
                    location.href = "/reservation";
                }
                else
                {
                    message(res.message,{manual:true,tipo:"danger"});
                    $("#btn-sent").attr("disabled",false);
                    $("#loading").css("display","none");
                }
                $("#mensaje").slideDown();
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            message(jqXHR,{tipo:"danger"});
            $("#mensaje").slideDown();
            $("#btn-sent").attr("disabled",false);
            $("#loading").css("display","none");
            //console.log("erro");
        });
    }
</script>
@stop