
@extends('layouts.dashboard.dashboard')

@section('style')
    <style>
        
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        .dates {
            background-color: white;
            box-shadow: 2px 2px 20px 0px black;
            border-radius: 25px;
        }
        .cabecera{
            color: #fff;
            font-family: inherit;
            text-shadow: 3px 5px 5px black;
        }
    </style>
@stop

@section('content')
    <section style="background: #626262;">
        <div class="row text-center py-3">
            <h1 for="" class="cabecera col-md-12">Actualmente no se pueden realizar reservaciones</h2>
        </div>
    </section>

    
@stop
@section('script')

@stop