
    <style>
        
        #only-portrait {
            display:none!important;
            z-index: -1;
        }
        @media only screen and (orientation:portrait){
            body section {  
                display:none!important;
            }
            #only-portrait {
                display:flex!important;
            }
        }
        @media only screen and (orientation:landscape){
            body section {  
                display:block!important;
            }
            #only-portrait {
                display:none!important;
            }
        }
    </style>

    <div id="only-portrait" class="d-flex justify-content-center" style="position: absolute;left: 0;bottom: 0;right: 0;top: 60px;align-items: center;">
        <div style="width: 100%;">
            <div class="card text-center" style="">
                <h5 class="card-header">NOTIFICACIÓN</h5>
                <div class="card-body">
                    <h5 class="card-title">Unicamente en landscape</h5>
                    <p class="card-text">Gire su dispositivo.</p>
                </div>
            </div>
        </div>
    </div>