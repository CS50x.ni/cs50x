<div id="loading" class="position-fixed " style="display:none; z-index: 1050;left: 0;background-color: #0000006b;height:100%;right: 0;top: 0;bottom: 0;">
    <div class="d-flex justify-content-center align-items-center" style="height: 100%;">
        <div class="col-lg-6 col-xs-2 text-center">
            <label style="color: #d4d4d4;font-size: -webkit-xxx-large;font-family: fantasy;background: #000000a6;border-radius: 50px;padding-left: 10px;padding-right: 10px;">CARGANDO</label>
            <div class="progress" style="width:100%;">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
            </div>
        </div>
    </div>
</div>