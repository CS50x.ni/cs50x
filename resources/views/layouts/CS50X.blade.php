<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <title>ParticleSlider</title>
        <style>
            html,
            body {
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0;
                overflow: hidden;

                .slides,
                &>.dg {
                    display: none;
                }
            }

            @media (min-width: 1000px) {
                body {
                    background-image: @hintImg;
                    background-repeat: no-repeat;
                    
                    &>.dg {
                        display: block;
                    }
                }
            }
        </style>
    </head>

    <body id="particle-slider">
        <div class="slides">
            <div id="first-slide" class="slide" data-src="./img/CS50x-medium.png">
            </div>
            <div id="div-aux" data-src="./img/CS50x-small.png">
            </div>
        </div>
        <canvas class="draw" width="1707" height="850"></canvas>

        <script>
            var $small = document.getElementById("div-aux");
            var $medium = document.getElementById("first-slide");
            var isMobile;
            var isSmall;

            function window_size()
            {

                isMobile = navigator.userAgent &&
                    navigator.userAgent.toLowerCase().indexOf('mobile') >= 0;
                isSmall = window.innerWidth < 800;
            }

            function need_change($div)
            {
                if($div.id != "first-slide")
                {
                    //Removemos las clases y id
                    $small.removeAttribute("id");
                    $small.removeAttribute("class");
                    $medium.removeAttribute("id");
                    $medium.removeAttribute("class");

                    //Asignamos el id y clases que antes tenia el div que se estaba mostrando
                    $div.setAttribute("id","first-slide");
                    $div.setAttribute("class","slide");
                    
                    ps.start();
                    change_force();
                    return true;
                }
                return false;
            }

            window.onresize = function(){
                window_size();
                confirmation();
            }

            function confirmation()
            {
                if (isMobile || isSmall) 
                {
                    need_change($small)
                }
                else
                {
                    need_change($medium)
                }
            }

            var ps;
            function firstime() {
                //Identificamos el tamaÃ±o de la pantalla
                window_size();
                //Inicializamos la particula
                ps = new ParticleSlider({
                    ptlGap: isMobile || isSmall ? 2 : 1,
                    ptlSize: isMobile || isSmall ? 2 : 2,
                    width: 1e9,
                    height: 1e9,
                    mouseForce: isMobile || isSmall ? 5000 : 10000
                });
                //Evaluamos la imagen que se utilizara y damos inicio
                if(!confirmation()){
                    ps.start();
                }
            }
            function change_force() {
                ps.mouseForce = isMobile || isSmall ? 5000 : 10000;
            }

            window.onload = function(){
                firstime();
            };
            

        </script>
        <script src="/js/kavv/CS50x.js" type="text/javascript"></script>

        <canvas width="800" height="142" style="display: none;"></canvas><canvas style="display: none;"></canvas><canvas style="display: none;"></canvas>
    </body>
</html>