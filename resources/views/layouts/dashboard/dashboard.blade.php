<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CS50-Reservaciones</title>
    <link rel="shortcut icon" href="/img/CS50-icon.png">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    @section('style')
    @show
</head>
<body id="body">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="/">CS50X.ni Reservaciones</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
            
                <a class="btn btn-secondary" href="/">Inicio</a>
                <!-- Reservaciones -->
                <!-- PARA EL CICLO Y21C1 NO SE PERMITIRA ESTA FUNCIONALIDAD -->
                @can('admin')
                <div class="dropdown" style="margin-left: 10px;">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownclass" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Reservaciones
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownclass">
                        <a class="dropdown-item" href="/reservation/add">Hacer Reservaciones</a>
                        <a class="dropdown-item" href="/reservation">Ver Reservaciones</a>
                    </div>
                </div>
                @endcan

                <!-- Reportes -->
                @can('reservation.index')
                <div class="dropdown" style="margin-left: 10px;">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownclass" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Asistencias
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownclass">
                        <a class="dropdown-item" href="/reservation/report">Reporte de OH</a>
                        @can('reservation.index')
                            <a class="dropdown-item" href="/class/report">Reporte de Clases</a>
                        @endcan
                    </div>
                </div>
                @endcan

                
                <!-- Reportes -->
                
                @can('reservation.index')
                <!-- <div class="dropdown" style="margin-left: 10px;">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownclass" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        PSETS
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownclass">
                        <a class="dropdown-item" target="_blank" href="/pset0">PSET 0</a>
                    </div>
                </div> -->
                @endcan
                

                @can('assistance.index')
                <!-- <div class="dropdown" style="margin-left: 10px;">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownclass" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        WEB50
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownclass">
                        <a class="dropdown-item" target="_blank" href="/project0">PROJECT 0</a>
                    </div>
                </div> -->
                @endcan
                @can('assistance.index')
                <div class="dropdown" style="margin-left: 10px;">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownclass" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        OH
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownclass">
                        <a class="dropdown-item" href="/assistance">Subir asistencias</a>
                    </div>
                </div>
                @endcan

                @can('assistance_class.index')
                <div class="dropdown" style="margin-left: 10px;">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownclass" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Clases
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownclass">
                        <a class="dropdown-item" href="/class/create">Subir asistencia</a>
                        <a class="dropdown-item" href="/lesson">Ver asistencias</a>
                    </div>
                </div>
                @endcan

                
                @can('assistance_class.index')
                <div class="dropdown" style="margin-left: 10px;">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownclass" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Seguimiento
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownclass">
                        <a class="dropdown-item" href="/admin/students">Ver Alumnos</a>
                    </div>
                </div>
                @endcan
                
                @can('admin')
                <div class="dropdown" style="margin-left: 10px;">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownclass" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Admin
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownclass">
                        <a class="dropdown-item" href="/admin/students">Ver estudiantes</a>
                        <a class="dropdown-item" href="/admin/sumary">Sumary</a>
                    </div>
                </div>
                @endcan

                @can('sumary.show')
                <div class="dropdown" style="margin-left: 10px;">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownclass" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Admin
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownclass">
                        <a class="dropdown-item" href="/admin/show-sumary">Sumary</a>
                    </div>
                </div>
                @endcan
            </ul>
            <ul class="navbar-nav ml-auto">
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{Auth::User()->name}}
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        
                        <a class="dropdown-item" href="/usuario">Cuenta</a>
                        
                        @can('admin')
                        <a class="dropdown-item" href="/configuration">Configuración</a>
                        @endcan

                        <form action="/logout" method="get">
                            {{csrf_field()}}
                            <a class="dropdown-item" href="/logout">Cerrar Sesión</a>
                        </form>
                    </div>
                </div>
            </ul>
        </div>
    </nav>

    @section('content')
    @show

</body>

<script src="/js/kavv/tiposmensajes.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


@section('script')



    <script>
        var timer = setTimeout(SessionOut,(1000*60*10));
        var logout;
        function SessionOut() {
            $.ajax({
            url: "/",
            }).fail( function( jqXHR, textStatus, errorThrown ) {
            //console.log(jqXHR.responseJSON);
            logout = jqXHR;
            location.href = "/";
            });
            clearTimeout(timer);
            timer = setTimeout(SessionOut,(1000*60*10));
        }
    </script>
@show

</html>