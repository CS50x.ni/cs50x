@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
    
        .reserved:hover {
            background: #f25656;
            color: white;
        }
        .reserved:hover:before {
            content: "Eliminar";
        }
        table td, table th {
            border: 1px solid #0000005c!important;
        }
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display:none;
        }
        .cabecera{
            background: #6c757d;
            color: #fff;
        }
        .sub-cabecera{
            background: #000;
            border-radius: 25px;
            color: #fff;
            box-shadow: 3px 3px 4px #b5b5b5;
        }
        .sub-cabecera h2{
            font-size: 2vw;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" >

@stop
@section('content')
    <div class="cabecera row text-center py-3">
        <h2 for="" class="col-md-12">REPORTE DE CLASES</h2>
    </div>
    <section>

        <div class="row">
            <div class="sub-cabecera col-md-5 text-center my-3">
                <h2 for="" class="col-md-12">{{$student->name." ".$student->last_name}}</h2>
            </div>
            <div class="col-md-1">
            </div>
            <div class="sub-cabecera col-md-3 text-center my-3">
                <h2 for="" class="col-md-12">Total de clases: {{$total_class}}</h2>
            </div>
            <div class="sub-cabecera col-md-3 text-center my-3">
                <h2 for="" class="col-md-12">Justifiaciones: {{$justifications}}</h2>
            </div>
        </div>

        <div class="row my-3 d-flex justify-content-center">
            <div class="col-md-6">
                <button class="btn-block btn btn-warning" data-toggle="modal" data-target="#modal-info"><strong>Infomación</strong></button>
            </div>
        </div>

        <div id="oh-carousel" class="carousel slide" data-interval="false" data-ride="false" style="border: 3px #000 ridge;">
            <div class="carousel-inner">
                {{--Step de las OH detalladas--}}
                <div class="carousel-item active">
                    <div class="row my-3">
                        <div class="col-md-12 text-center">
                            <button id="class-top" class="btn btn-info">Top de clases</button>
                        </div>
                    </div>
                    <table id="t-class" class="table table-hover text-center" cellspacing="0" style="width:100%;">
                        <thead>
                            <tr>
                                <th>Clase</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $class)
                                <tr>
                                    <td>{{$class["name"]}}</td>
                                    <td>{{$class["assistance"]}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                {{--TOP OH--}}
                <div class="carousel-item">
                    <div class="row my-3">
                        <div class="col-md-12 text-center">
                            <button id="class-history" class="btn btn-info">Historial detallado</button>
                        </div>
                    </div>
                    <table id="t-top" class="table table-hover text-center" cellspacing="0" style="width:100%;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $index = 1;?>
                            @foreach($top as $data_top)
                                <tr>
                                    <td>{{$index}}</td>
                                    <td>{{$data_top[1]}}</td>
                                    <td>{{$data_top[0]}}</td>
                                </tr>
                                <?php $index++;?>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>


    @include('layouts.cargando.cargando')

    @include("layouts/validation-viewport")
    
    <div class="modal fade" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="infomodal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title">Información</h5>
                </div>
                <div class="modal-body">
                    <div class="row pb-3">
                        <div class="col-md-12">
                            <ol>
                                <li>Puede visualizar el detalle de cada una de sus clases.</li>
                                <li>Puede ordenar/agrupar los datos, dando click en las columnas.</li>
                                <li>El total de sus asistencias y justificaciones se encuentra en la parte superio derecha.</li>
                                <li>De click en el boton que dice "Top de clases" para ver quienes son los alumnos con mayor cantidad de clases asistidas.</li>
                                <li>Si esta en la vista de "top de clases" puede regresar a su reporte detallado dando click en "Historial detallado"</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script')
<script src="/js/kavv/kavvdt.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="http://cdn.datatables.net/plug-ins/1.10.20/filtering/type-based/accent-neutralise.js"></script>
<script>
    var data;
    var row, dt_class, dt_top;
    $(document).ready(function(){
        $.get("/secure", function(res){
            $("body").css("display","block");
            var class_columns = [{"width":"50%"},{"width":"50%"}];
            var top_columns = [{"width":"10%"},{"width":"60%"},{"width":"30%"}];
            var visible_row =  [[20, 10], [20, 10]];
            var t_visible_row =  [[40, 20, 10], [40, 20, 10]];
            dt_class = createdt($('#t-class'),{col:0,visible_row:visible_row, columns_width:class_columns});
            dt_top = createdt($('#t-top'),{col:0,visible_row:t_visible_row, columns_width:top_columns});
        }).fail(function() {
            location.href = "/"; 
        });
    });
    

    $("#class-top").click(function(){
        $("#oh-carousel").carousel("next");
        dt_top.draw();
    });
    $("#class-history").click(function(){
        $("#oh-carousel").carousel("prev");
        dt_oh.draw();
    });
</script>
@stop