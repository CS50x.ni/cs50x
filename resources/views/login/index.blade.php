<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CS50X.ni</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <style>
        html, body{
            height: 100%;
        }
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        .m-error{
            color: red;
            padding-left: 10px;
        }
        .main-section{
            height: -webkit-fill-available;
            align-items: center;
        }
    </style>
</head>
<body>

    <section class="main-section d-flex justify-content-center">

        <div class="col-md-8 card mb-3" style="padding:0px;">
        
            <div id="mensaje"></div>
            <div class="row no-gutters px-2" style="display: flex; align-items: center;">
                <div class="col-md-4 pt-4">
                    <img src="/img/CS50x.png" class="card-img" alt="...">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title text-center">Inicia sesión</h5>
                        <form id="data" >
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Correo</label>
                                <input type="text" name="email" id="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                                <span id="message-email" class="m-error"></span>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Contraseña</label>
                                <input type="password" name="password" id="password" class="form-control" id="exampleInputPassword1">
                                <span id="message-password" class="m-error"></span>
                            </div>
                            <div class="text-center">
                                <button type="submit" id="btn-login" onclick="login();" class="btn btn-primary">Iniciar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        

    </section>
    @include('layouts.cargando.cargando')

</body>

<script src="/js/kavv/tiposmensajes.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script>
    function login()
    {
        var ruta = "/login";
        //var token = $("#token").val();
        
        //$("#mensaje").slideUp();
        var data = $("#data").serialize();
        $("#btn-login").attr("disabled",true);
        //$("#loading").css("display","flex");
        $("#loading").css("display","block");
        return $.ajax({
            url: ruta,
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            type: 'POST',
            dataType: 'json',
            data:data,
            success: function(res){
                input_require(res.message);
                if(res.code != 0)
                {

                    if(res.code === 2)
                    {
                        message(res.message,{manual:true,tipo:"danger"});
                        $("#btn-login").attr("disabled",false);
                        $("#loading").css("display","none");
                        //$("#email").val("");
                        $("#password").val("");
                    }
                    else
                    {
                        location.href = "/";
                    }
                }
                else
                {
                    $("#btn-login").attr("disabled",false);
                    $("#loading").css("display","none");
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            //message(jqXHR,{tipo:"danger"});
            //$("#mensaje").slideDown();
            $("#btn-login").attr("disabled",false);
            $("#loading").css("display","none");
            console.log("erro");
        });
    }
    function input_require(message)
    {
        
        if($("#email").val()=="")
            $("#message-email").text(message);
        else
            $("#message-email").text("");

        if($("#password").val()=="")
            $("#message-password").text(message);
        else
            $("#message-password").text("");
    }
    $("#data").submit(function(e){
        e.preventDefault();
    });
    
    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    }
</script>