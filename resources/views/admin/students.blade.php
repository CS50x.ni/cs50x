@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
        .space-0{
            background: #f25656!important;
        }
        .oh{
            background: #888888;
            cursor: pointer;
            transition: .3s;
        }
        .oh-block {
            background: #cbcbcb;
        }
        .t-data td, .t-data th {
            border: 1px solid #0000005c!important;
        }
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display:none;
        }
        .t-data .td-black
        {
            color: #fff;
            background: #000000;
            border-bottom: 1.4px solid #ffffff!important;
        }
        .t-data .th-gray{
            color:#fff;
            background: #3b3b3b;
        }
        .dates {
            background-color: white;
            box-shadow: 2px 2px 20px 0px black;
            border-radius: 25px;
        }
        .cabecera{
            background: #6c757d;
            color: #fff;
        }
        .tday{
            box-shadow: 4px 4px 15px 0px black;
        }
        
        .btn-hour {
            transition: .3s;
            cursor: pointer;
        }

        .btn-hour:hover {
            background: #fff;
            color: #000;
        }

        .btn-hour:after {
            content: "\f06e"; /* Valor unicode */
            font-family: FontAwesome;
            font-style: normal;
            font-weight: normal;
            text-decoration: inherit;

            /* Estos ajustes son opcionales, se aplican con la finalidad de dar diseño */
            display: inline-block;
            font-size: 18px;
            color: #000;
            margin-left: 10px;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" >
    <!-- DatePicker -->
    <link href="/css/gijgo/css/gijgo.css" rel="stylesheet" type="text/css" />

    


    
    
@stop
@section('content')

    <div class="row text-center">
        <h2 for="" class="cabecera col-md-12 py-3">Lista de alumnos del ciclo Y21C1</h2>
    </div>
    <section>
        <div id="mensaje"></div>
        <table id="t-students" class="table table-hover text-center t-data" cellspacing="0" style="width:100%;">
            <thead>
                <tr>
                    <th data-orderable="false"></th>
                    <th>Creación del dato</th>
                    <th>Código</th>
                    <th>Correo</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>gender</th>
                    <th>birthdate</th>
                    <th>address</th>
                    <th>phone</th>
                    <th>level</th>
                    <th>career</th>
                    <th>educational_center</th>
                    <th>shirt_size</th>
                    <th>state</th>
                    <th>coming</th>
                    <th>Grupo</th>
                    <th data-orderable="false"></th>
                    <th data-orderable="false"></th>
                </tr>
            </thead>
            <tbody id="tb-reservations">
                @foreach($students as $key=>$student)
                
                    <tr>
                        <td></td>
                        <td>{{$student->created_at}}</td>
                        <td>{{$student->code}}</td>

                        <td>{{$student->email}}</td>
                        <td>{{$student->name}}</td>
                        <td>{{$student->last_name}}</td>

                        
                        <td>{{$student->gender}}</td>
                        <td>{{$student->birthdate}}</td>
                        <td>{{$student->address}}</td>
                        <td>{{$student->phone}}</td>
                        <td>{{$student->level}}</td>
                        <td>{{$student->career}}</td>
                        <td>{{$student->educational_center}}</td>
                        <td>{{$student->shirt_size}}</td>
                        <td>{{$student->state}}</td>
                        <td>{{$student->coming}}</td>
                        <td>{{$student->group}}</td>
                    
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Opciones
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" onclick="active_modal(1,{{$student->id}})" href="#" data-toggle="modal" data-target="#modal-data">Detalle del alumno</a>
                                    @can('admin')
                                    <a class="dropdown-item" onclick="active_modal(2,{{$student->id}})" href="#" data-toggle="modal" data-target="#modal-data">Editar datos</a>
                                    <a class="dropdown-item" onclick="active_modal(3,{{$student->id}})" href="#" data-toggle="modal" data-target="#modal-data">Resetear contraseña</a>
                                    <a class="dropdown-item" onclick="active_modal(4,{{$student->id}})" href="#" data-toggle="modal" data-target="#modal-data">Eliminar registro</a>
                                    @endcan
                                    <a class="dropdown-item" target="_blank" href="/admin/students_oh/{{$student->id}}">Ver asistencias OH</a>
                                    <a class="dropdown-item" target="_blank" href="/admin/students_class/{{$student->id}}">Ver asistencias clases</a>
                                </div>
                            </div>
                        </td>
                        <td>{{$student->id}}</td>
                    </tr>

                @endforeach
            </tbody>
            <tfoot id="tb-foot">
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><input class="form-control" type="text" placeholder="Correo"></td>
                    <td><input class="form-control" type="text" placeholder="Nombre"></td>
                    <td><input class="form-control" type="text" placeholder="Apellido"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><input class="form-control" type="text" placeholder="Grupo"></td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>

        <!-- Modal para detalle, editar y resetear contraseña -->
        <div class="modal fade" id="modal-data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="m-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="msg-modal"></div>
                        <!-- Body details contiende los imputs que se rellenaran con las consultas ajax -->
                        <div id="body-details" class="view-modal text-center" style="display:none;">
                            <form id="data">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Código</label>
                                        <input class="form-control" type="text" name="code" id="code" value="">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Correo</label>
                                        <input class="form-control" type="text" name="email" id="email" value="">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Nombre</label>
                                        <input class="form-control" type="text" name="name" id="name" value="">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Apellido</label>
                                        <input class="form-control" type="text" name="last_name" id="last_name" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Genero</label>
                                        <Select class="form-control" name="gender" id="gender">
                                            <option value=""></option>
                                            <option value="Femenino">Femenino</option>
                                            <option value="Masculino">Masculino</option>
                                        </Select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Fecha de nacimiento</label>
                                        <input class="form-control datepicker" type="text" name="birthdate" id="birthdate" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Dirección</label>
                                        <input class="form-control" type="text" name="address" id="address" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Nivel academico</label>
                                        <Select class="form-control" name="level" id="level">
                                            <option value=""></option>
                                            <option value="Primaria">Primaria</option>
                                            <option value="Secundaria">Secundaria</option>
                                            <option value="Universidad">Universidad</option>
                                            <option value="Superior a universidad">Superior a universidad</option>
                                        </Select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Carrera</label>
                                        <input class="form-control" type="text" name="career" id="career" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Centro de estudio</label>
                                        <input class="form-control" type="text" name="educational_center" id="educational_center" value="">
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">Talla</label>
                                        <Select class="form-control" name="shirt_size" id="shirt_size">
                                            <option value="14">14</option>
                                            <option value="16">16</option>
                                            <option value="S">S</option>
                                            <option value="M">M</option>
                                            <option value="L">L</option>
                                            <option value="XL">XL</option>
                                            <option value="XXL">XXL</option>
                                        </Select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Telefono</label>
                                        <input class="form-control" type="text" name="phone" id="phone" value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="">Estado</label>
                                        <input class="form-control" type="text" name="state" id="state" value="">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Proveniencia</label>
                                        <input class="form-control" type="text" name="coming" id="coming" value="">
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">Registro creado el</label>
                                        <input class="form-control" type="text" id="created_at" value="">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="body-password-reset" class="view-modal text-center" style="display:none;">
                            <button class="btn btn-danger btn-block" onclick="password_update();">Resetar contraseña</button>
                            <em>La contraseña por defecto es 123456</em>
                        </div>
                        <div id="body-student-delete" class="view-modal text-center" style="display:none;">
                            <button class="btn btn-danger btn-block" onclick="student_delete();">Eliminar registro</button>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btn-update" style="display:none">Actualizar</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        
    @include('layouts.cargando.cargando')
    @include("layouts/validation-viewport")
@stop
@section('script')
<script src="/js/kavv/kavvdt.js"></script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="http://cdn.datatables.net/plug-ins/1.10.20/filtering/type-based/accent-neutralise.js"></script>
<!-- DatePicker -->
<script src="/js/gijgo/js/gijgo.js" type="text/javascript"></script>

<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js" type="text/javascript"></script>








<script>
    var dt = null;
    var data= new Object()
    var row;
    $('#birthdate').datepicker({
        locale: 'es-es',
        format: 'yyyy-mm-dd',
        uiLibrary: 'bootstrap4',
        iconsLibrary: 'fontawesome',
        header: true,
    });

    $(document).ready(function(){
        //La consulta secure es para verificar que esta funcionando correctamente del lado del servidor
        $.get("/secure", function(res){
            //En caso de estar bien, mostramos el contenido de la web
            $("body").css("display","block");
            //Variables de configuración para la dt
            var columns = [{"width":"5%"},{"width":"0%"},{"width":"0%"},{"width":"30%"},{"width":"25%"},{"width":"25%"}
            ,{"width":"0%"},{"width":"0%"},{"width":"0%"},{"width":"0%"},{"width":"0%"},{"width":"0%"},{"width":"0%"}
            ,{"width":"0%"},{"width":"0%"},{"width":"0%"},{"width":"10%"}, {"width":"15%"},{"width":"0%"}];

            var visible_row =  [[30, 20, 10, -1], [30, 20, 10, "Todo"]];
            var colum_def = [{
                "targets": [ 1,2,6,7,8,9,10,11,12,13,14,15,18 ],
                "visible": false,
                "searchable": false
            }];

            dt = createdt($('#t-students'),{col:3, visible_row:visible_row, columns_width:columns, columdef:colum_def});

            //Establecemos el indice dinamico de la cantidad total de filas
            dt.on( 'order.dt search.dt', function () {
                dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();

            //Genera el proceso de busqueda en cada una de las columnas
            dt.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
            //Reubica las busquedas personalizadas
            $(".dataTables_scrollFoot").prependTo(".dataTables_scroll");

            $("#loading").css('z-index',1060);
        }).fail(function() {
            //Si se detecta algun error del lado del servidor se redirecciona al index
            location.href = "/"; 
        });
    });

    var student;
    //En base a la opcion seleccionada se realizaran cambios en el modal
    function active_modal(option, id)
    {
        student = id;
        $("#loading").show();
        if(option == 1)
        {
            $("#m-title").text("Detalle del alumno");
            $("#body-details").show();
            get_student();
        }
        if(option == 2)
        {
            $("#btn-update").show();
            $("#m-title").text("Editar alumno");
            $("#body-details").show();
            get_student();
        }
        if(option == 3)
        {
            $("#m-title").text("Resetear contraseña");
            $("#body-password-reset").show();
        }
        if(option == 4)
        {
            $("#m-title").text("Eliminar registro");
            $("#body-student-delete").show();
        }
        $("#loading").hide();
    }

    $('#modal-data').on('hidden.bs.modal', function (e) {
        $("#btn-update").hide();
        $("#msg-modal").hide();
        $(".view-modal").hide();
    });

    $("#btn-update").click(function(){
        student_update();
    });

    //Obtener studiante
    function get_student()
    {
        $.get("/admin/student/"+student, function(res){

            if(res.code == 200)
            {
                //Asignamos los datos del estudiante en los respectivos inputs
                $("#code").val(res.data.code);
                $("#email").val(res.data.email);
                $("#name").val(res.data.name);
                $("#last_name").val(res.data.last_name);
                $("#gender").val(res.data.gender);
                $("#birthdate").val(res.data.birthdate);
                $("#address").val(res.data.address);
                $("#phone").val(res.data.phone);
                $("#level").val(res.data.level);
                $("#career").val(res.data.career);
                $("#educational_center").val(res.data.educational_center);
                $("#shirt_size").val(res.data.shirt_size);
                $("#state").val(res.data.state);
                $("#coming").val(res.data.coming);
                $("#created_at").val(res.data.created_at);
            }
            else
            {
                message(res.message,{manual:true,tipo:"danger"});
            }
            
        });
    }

    //Actualizar estudiante
    function student_update()
    {
        data = $("#data").serialize();
        return $.ajax({
            url: "/admin/student_update/"+student,
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            type: 'PUT',
            dataType: 'json',
            data: data,
            success: function(res){
                if(res.code === 200)
                {
                    message(res.message,{manual:true});
                    $("#modal-data").modal("hide");
                }
                else
                {
                    message(res.message,{objeto:$("#msg-modal"),manual:true,tipo:"danger"});
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            message(jqXHR,{objeto:$("#msg-modal"),tipo:"danger"});
        });
    }
     //Actualizar password
     function password_update()
    {
        return $.ajax({
            url: "/admin/password_update/"+student,
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            type: 'PUT',
            dataType: 'json',
            success: function(res){
                if(res.code === 200)
                {
                    message(res.message,{manual:true});
                    $("#modal-data").modal("hide");
                }
                else
                {
                    message(res.message,{objeto:$("#msg-modal"),manual:true,tipo:"danger"});
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            message(jqXHR,{objeto:$("#msg-modal"),tipo:"danger"});
        });
    }

    //Eliminar registro
    function student_delete()
    {
        return $.ajax({
            url: "/admin/student_delete/"+student,
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            type: 'DELETE',
            dataType: 'json',
            success: function(res){
                if(res.code === 200)
                {
                    message(res.message,{manual:true});
                    $("#modal-data").modal("hide");
                }
                else
                {
                    message(res.message,{objeto:$("#msg-modal"),manual:true,tipo:"danger"});
                }
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            message(jqXHR,{objeto:$("#msg-modal"),tipo:"danger"});
        });
    }

</script>
@stop

