@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
    
        .reserved:hover {
            background: #f25656;
            color: white;
        }
        .reserved:hover:before {
            content: "Eliminar";
        }
        table td, table th {
            border: 1px solid #0000005c!important;
        }
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display:none;
        }
        .cabecera{
            background: #6c757d;
            color: #fff;
        }
        .sub-cabecera{
            background: #000;
            border-radius: 25px;
            color: #fff;
            box-shadow: 3px 3px 4px #b5b5b5;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" >

@stop
@section('content')
    <div class="cabecera row text-center py-3">
        <h2 for="" class="col-md-12">REPORTE DE TODAS LAS OH</h2>
    </div>
    <section>


        <div class="row my-3 d-flex justify-content-center">
            <div class="col-md-6">
                <button class="btn-block btn btn-warning" data-toggle="modal" data-target="#modal-info"><strong>Infomación</strong></button>
            </div>
        </div>

        <table id="t-oh" class="table table-hover text-center" cellspacing="0" style="width:100%;">
            <thead>
                <tr>
                    <th data-orderable="false"></th>
                    <th>Semana</th>
                    <th>Día</th>
                    <th>Bloque</th>
                    <th>Locación</th>
                    <th>Reservo</th>
                    <th>Horas</th>
                    <th>Staff</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $oh)
                    <tr>
                        <td></td>
                        <td>{{$oh["week"]}}</td>
                        <td>{{$oh["day"]}}</td>
                        <td>{{$oh["hour"]}}</td>
                        <td>{{$oh["location"]}}</td>
                        <td>{{$oh["reserved"]}}</td>
                        <td>{{$oh["assistance"]}}</td>
                        <td>{{$oh["staff"]}}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot id="tb-foot">
                <tr>
                    <td></td>
                    <td><input class="form-control" type="text" placeholder="Semana"></td>
                    <td><input class="form-control" type="text" placeholder="Día"></td>
                    <td><input class="form-control" type="text" placeholder="Bloque"></td>
                    <td><input class="form-control" type="text" placeholder="Locación"></td>
                    <td><input class="form-control" type="text" placeholder="Reservo"></td>
                    <td><input class="form-control" type="text" placeholder="Horas"></td>
                    <td><input class="form-control" type="text" placeholder="Staff"></td>
                </tr>
            </tfoot>
        </table>

    </section>


    @include('layouts.cargando.cargando')

    @include("layouts/validation-viewport")
    
    <div class="modal fade" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="infomodal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title">Información</h5>
                </div>
                <div class="modal-body">
                    <div class="row pb-3">
                        <div class="col-md-12">
                            <ol>
                                <li>Puede visualizar el detalle de cada una de sus OH.</li>
                                <li>Puede ordenar/agrupar los datos, dando click en las columnas.</li>
                                <li>El total de sus OH se encuentra en la parte superio derecha.</li>
                                <li>De click en el boton que dice "Top de OH" para ver quienes son los alumnos con mayor cantidad de OH.</li>
                                <li>Si esta en la vista de "top de OH" puede regresar a su reporte detallado dando click en "Historial detallado"</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script')
<script src="/js/kavv/kavvdt.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="http://cdn.datatables.net/plug-ins/1.10.20/filtering/type-based/accent-neutralise.js"></script>
<script>
    var data;
    var row, dt_oh, dt_top;
    $(document).ready(function(){
        $.get("/secure", function(res){
            $("body").css("display","block");
            var oh_columns = [{"width":"5%"},{"width":"10%"},{"width":"15%"},{"width":"20%"},{"width":"10%"},{"width":"10%"},{"width":"15%"},{"width":"15%"}];
            var visible_row =  [[40, 20, 10, -1], [40, 20, 10, "Todo"]];
            dt_oh = createdt($('#t-oh'),{col:1, com:"desc",visible_row:visible_row, columns_width:oh_columns});

            //Genera los indices de cada fila dinamicamente
            dt_oh.on( 'order.dt search.dt', function () {
                dt_oh.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();

            //Genera el proceso de busqueda en cada una de las columnas
            dt_oh.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
            //Reubica las busquedas personalizadas
            $(".dataTables_scrollFoot").prependTo(".dataTables_scroll");
        }).fail(function() {
            location.href = "/"; 
        });
    });
    
</script>
@stop


