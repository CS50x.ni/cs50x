@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
    
        .reserved:hover {
            background: #f25656;
            color: white;
        }
        .reserved:hover:before {
            content: "Eliminar";
        }
        table td, table th {
            border: 1px solid #0000005c!important;
        }
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display:none;
        }
        .cabecera{
            background: #6c757d;
            color: #fff;
        }
        .sub-cabecera{
            background: #000;
            border-radius: 25px;
            color: #fff;
            box-shadow: 3px 3px 4px #b5b5b5;
        }
        .sub-cabecera h2{
            font-size: 2vw;
        }
        .td-black{
            background: #000; 
            color:#fff; 
            border-bottom: 1px solid #ededed!important;
        }
        .th-skyblue {
            background: #a9d4ff;
        }
        .th-total{
            background: #66e250;
        }
        .row-gray{
            background: #c3c3c3;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" >

@stop
@section('content')
    <div class="cabecera row text-center py-3 mb-3">
        <h2 for="" class="col-md-12">Sumary</h2>
    </div>
    <section>


        <table id="t-class" class="table table-hover text-center" cellspacing="0" style="width:100%;">
            <thead>
                <tr class="row-gray">
                    <th colspan="14">Asistencia a Clases</th>
                </tr>
                <tr>
                    <th class="td-black" data-orderable="false">Semana</th>
                    <th class="td-black">Clase</th>
                    <th class="th-skyblue">A</th>
                    <th class="th-skyblue">B</th>
                    <th class="th-skyblue">C</th>
                    <th class="th-skyblue">D</th>
                    <th class="th-skyblue">E</th>
                    <th class="th-skyblue">F</th>
                    <th class="th-skyblue">G</th>
                    <th class="th-skyblue">H</th>
                    <th class="th-skyblue">I</th>
                    <th class="th-skyblue">J</th>
                    <th class="th-skyblue">X</th>
                    <th class="th-total">Total</th>
                </tr>
            </thead>
            <tbody>
                <?php $weeks = [ 
                                "S02", "S02", "S03", "S03", "S04", "S04", "S05", "S05", "S06", "S06", "S07", "S07", "S08", "S08", "S09", "S09", "S10", "S10", 
                                "S11", "S11", "S12", "S12", "S13", "S13", "S14", "S14", "S15", "S15", "S16", "S16", "S17", "S17", "S18", "S18", "S19", "S19", "S20", "S20", 
                            ] 
                ?>
                @foreach($data as $index => $class)
                    <tr>
                        <td class="td-black">{{$weeks[$index]}}</td>
                        <td class="td-black">{{$class["name"]}}</td>
                        <td>{{$class["group_a"]}}</td>
                        <td>{{$class["group_b"]}}</td>
                        <td>{{$class["group_c"]}}</td>
                        <td>{{$class["group_d"]}}</td>
                        <td>{{$class["group_e"]}}</td>
                        <td>{{$class["group_f"]}}</td>
                        <td>{{$class["group_g"]}}</td>
                        <td>{{$class["group_h"]}}</td>
                        <td>{{$class["group_i"]}}</td>
                        <td>{{$class["group_j"]}}</td>
                        <td>{{$class["group_x"]}}</td>
                        <td style="background: {{$class['color']}}">{{$class["total"]}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <table id="t-oh" class="table table-hover text-center mt-3" cellspacing="0" style="width:100%;">
            <thead>
                <tr class="row-gray">
                    <th colspan="17">Totales de Office Hours por Semana</th>
                </tr>
                <tr>
                    <th class="td-black"></th>
                    <th class="td-black">1-S3</th>
                    <th class="td-black">2-S4</th>
                    <th class="td-black">3-S5</th>
                    <th class="td-black">4-S6</th>
                    <th class="td-black">5-S7</th>
                    <th class="td-black">6-S8</th>
                    <th class="td-black">7-S9</th>
                    <th class="td-black">8-S10</th>
                    <th class="td-black">9-S11</th>
                    <th class="td-black">10-S12</th>
                    <th class="td-black">11-S13</th>
                    <th class="td-black">12-S14</th>
                    <th class="td-black">13-S15</th>
                    <th class="td-black">14-S16</th>
                    <th class="td-black">15-S17</th>
                    <th class="td-black">16-S18</th>
                </tr>
            </thead>
            <tbody>
                <?php $week_count = 1; //$aux = [154, 1008, 1066, 968]?>
                <tr>
                    <td>Horas reservadas</td>
                    {{-- Recorre todas las reservaciones realizadas por semana --}}
                    @foreach($reservations as $key => $reservation)
                        {{-- Este for completa en caso de que el dato no corresponda a la semana en el orden generico --}}
                        @for($i = $week_count; $i <= 16 ;$i++)
                            {{-- Corroboramos que la semana sea la correspondiente de la celda --}}
                            @if($key == "w".$i)
                                <!-- Para el ciclo Y20C1 aqui seria $i <= 4 para cualquier otro ciclo < 0 -->
                                @if($i < 0)<td>{{$aux[$i-1]}}</td> @else<td>{{$reservation}}</td>@endif
                                {{-- igulamos week_count por si existe existio un hueco --}}
                                <?php $week_count = $i; ?>
                                @break
                            @else
                                {{-- Si existe un hueco entonces se mostrara un guion  --}}
                                <td>-</td>
                            @endif
                        @endfor
                        <?php $week_count++; ?>
                    @endforeach
                    @for($i = $week_count; $i <= 16 ;$i++)
                        <td>-</td>
                    @endfor
                </tr>
                <?php $week_count = 1; ?>
                <tr>
                    <td>Horas assistidas</td>
                    @foreach($assistances as $key => $assistance)
                        @for($i = $week_count; $i <= 16 ;$i++)
                            @if($key == "w".$i)
                                <td>{{$assistance}}</td>
                                <?php $week_count = $i; ?>
                                @break
                            @else
                                <td>-</td>
                            @endif
                        @endfor
                        <?php $week_count++; ?>
                    @endforeach
                    @for($i = $week_count; $i <= 16 ;$i++)
                        <td>-</td>
                    @endfor
                </tr>
            </tbody>
        </table>

    </section>
    <div id="chart-class" style="width:95%; height:600px;"></div>
    <div id="chart-oh" style="width:95%; height:600px;"></div>

    @include('layouts.cargando.cargando')

    @include("layouts/validation-viewport")


@stop
@section('script')
<script src="/js/kavv/kavvdt.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="http://cdn.datatables.net/plug-ins/1.10.20/filtering/type-based/accent-neutralise.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

<script>
    var data = {!!json_encode($data)!!};
    var assistances = {!!json_encode($assistances)!!};
    var reservations = {!!json_encode($reservations)!!};
    var row, dt_class, dt_top;


    
    var chart_class = {
        chart: {
          renderTo: "chart-class",
          type: "column"  
        },
        
        lang: {
            months: [
                'Enero', 'Febrero', 'Marzo', 'Abril',
                'Mayo', 'Junio', 'Julio', 'Agosto',
                'Septembre', 'Octobre', 'Novembre', 'Décembre'
            ],
            weekdays: [
                'Domingo', 'Lunes', 'Martes', 'Miercoles',
                'Jueves', 'Viernes', 'Sabado'
            ],
            downloadCSV: "Descargar CSV",
            downloadJPEG: "Descargar JPEG image",
            downloadPDF: "Descargar PDF document",
            downloadPNG: "Descargar PNG image",
            downloadSVG: "Descargar SVG vector image",
            downloadXLS: "Descargar XLS",
            loading: "Cargando...",
            noData: "No hay informacion que mostrar",
            openInCloud: "Abrir en Highcharts",
            printChart: "Imprimir Grafica",
            shortMonths:["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
            viewData: "Ver tabla",
            viewFullscreen: "Ver pantalla completa",
        },
        exporting: {
            buttons: {
                contextButton: {
                    menuItems: ['viewFullscreen', 'printChart', 'separator', 'downloadPDF', 'downloadPNG','downloadJPEG', 'downloadSVG', 'separator', 'downloadCSV', 'downloadXLS']
                }
            }
        },
        title: {
            text: ''
        },

        subtitle: {
            text: ''
        },

        yAxis: {
            title: {
                text: 'Total de personas'
            },/*,
            labels: {
            formatter: function() {
                return this.value + ' %';
            }*/
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: ( // theme
                        Highcharts.defaultOptions.title.style &&
                        Highcharts.defaultOptions.title.style.color
                    ) || 'gray'
                }
            }
        },
        xAxis: {
            title: {
                text: 'Clases'
            },
            categories:[],
            tickInterval: 1,
            layout: 'vertical',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        /*
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2010
            }
        },*/
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },

        series: [],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        },
        tooltip: {
        },
    };

    var chart_oh = {
        chart: {
          renderTo: "chart-oh",
          type: "line"  
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ["S01", "S02", "S03", "S04", "S05", "S06", "S07", "S08", "S09", "S10", "S11", "S12", "S13", "S14", "S15", "S16", "S17", "S18", "S19", "S20"]
        },
        yAxis: {
            title: {
                text: 'Cantidad de alumnos'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Reservaciones',
            data: []
        }, {
            name: 'Asistencias',
            data: []
        }]
    };


    $(document).ready(function(){
        $.get("/secure", function(res){
            $("body").css("display","block");
            var class_columns = [{"width":"10%"}, {"width":"10%"}, {"width":"10%"}, {"width":"10%"}, {"width":"10%"}, {"width":"10%"}, {"width":"10%"}, {"width":"10%"}, {"width":"10%"}, {"width":"10%"}, {"width":"10%"}, {"width":"10%"}, {"width":"10%"}, {"width":"10%"}];
            var visible_row =  [[20, 10, -1], [20, 10, "Todos"]];
            dt_class = createdt($('#t-class'),{col:1,visible_row:visible_row});
            chartClass();
            chartOh();
        }).fail(function() {
            location.href = "/"; 
        });
    });
    
    function chartClass()
    {
        
        chart_class.title.text = "Asistencia a clases - General";

        //creamos las series en base a la cantidad de grupos
        var groups = ["A", "B",  "C",  "D",  "E",  "F",  "G",  "H",  "I",  "J",  "X"];
        for(i = 0; i < groups.length; i++)
        {
            chart_class.series.push({"name":groups[i], "data":[], "extra":[]});
        }
        var aux;
        for(index in data)
        {
            //console.log(data[index].name);
            //config.series.push({"name":[], "data":[], "extra":[]});
            aux = data[index];
            //Se especifica
            chart_class.series[0].data.push(aux.group_a);
            chart_class.series[1].data.push(aux.group_b);
            chart_class.series[2].data.push(aux.group_c);
            chart_class.series[3].data.push(aux.group_d);
            chart_class.series[4].data.push(aux.group_e);
            chart_class.series[5].data.push(aux.group_f);
            chart_class.series[6].data.push(aux.group_g);
            chart_class.series[7].data.push(aux.group_h);
            chart_class.series[8].data.push(aux.group_i);
            chart_class.series[9].data.push(aux.group_j);
            chart_class.series[10].data.push(aux.group_x);
            //Se define los label del eje X
            chart_class.xAxis.categories.push(data[index].name);
        }
        //Se renderiza la grafica
        diagrama_clase = new Highcharts.chart(chart_class);
    
    }

    function chartOh()
    {
        chart_oh.title.text = "Asistencia a OH - General";
        var aux = 0;
        var data_aux = [154, 1008, 1066, 968];
        //Agregamos la data de la serie 0 para graficar las reservaciones 
        for(index in reservations)
        {
            // Para el ciclo Y20C1 debria ser aux < 4
            if(aux < 0)
            {                
                chart_oh.series[0].data.push(data_aux[aux]);
                aux++
            }
            else
                chart_oh.series[0].data.push(reservations[index]);
        }
        for(index in assistances)
        {
            console.log(index);
            chart_oh.series[1].data.push(assistances[index]);
        }
        //Se renderiza la grafica
        diagrama_oh = new Highcharts.chart(chart_oh);
    }
</script>
@stop