@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
    
        .reserved:hover {
            background: #f25656;
            color: white;
        }
        .reserved:hover:before {
            content: "Eliminar";
        }
        table td, table th {
            border: 1px solid #0000005c!important;
        }
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display:none;
        }
        .cabecera{
            background: #6c757d;
            color: #fff;
        }
        .sub-cabecera{
            background: #000;
            border-radius: 25px;
            color: #fff;
            box-shadow: 3px 3px 4px #b5b5b5;
        }
        .sub-cabecera h2{
            font-size: 2vw;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" >

@stop
@section('content')
    <div class="cabecera row text-center py-3">
        <h2 for="" class="col-md-12">REPORTE DE CLASES</h2>
    </div>
    <section>

        <div class="row">
            <div class="sub-cabecera col-md-5 text-center my-3">
                <h2 for="" class="col-md-12">{{$student->name." ".$student->last_name}}</h2>
            </div>
            <div class="col-md-1">
            </div>
            <div class="sub-cabecera col-md-3 text-center my-3">
                <h2 for="" class="col-md-12">Total de clases: {{$total_class}}</h2>
            </div>
            <div class="sub-cabecera col-md-3 text-center my-3">
                <h2 for="" class="col-md-12">Justifiaciones: {{$justifications}}</h2>
            </div>
        </div>

        <div class="row my-3 d-flex justify-content-center">
            <div class="col-md-6">
                <button class="btn-block btn btn-warning" data-toggle="modal" data-target="#modal-info"><strong>Infomación</strong></button>
            </div>
        </div>

        <table id="t-class" class="table table-hover text-center" cellspacing="0" style="width:100%;">
            <thead>
                <tr>
                    <th data-orderable="false"></th>
                    <th>Subida</th>
                    <th>Clase</th>
                    <th>Grupo</th>
                    <th>Estado</th>
                    <th>Staff</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $class)
                    <tr>
                        <td></td>
                        <td>{{$class["date"]}}</td>
                        <td>{{$class["name"]}}</td>
                        <td>{{$class["group"]}}</td>
                        <td>{{$class["assistance"]}}</td>
                        <td>{{$class["staff"]}}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot id="tb-foot">
                <tr>
                    <td></td>
                    <td><input class="form-control" type="text" placeholder="Subida"></td>
                    <td><input class="form-control" type="text" placeholder="Clase"></td>
                    <td><input class="form-control" type="text" placeholder="Grupo"></td>
                    <td><input class="form-control" type="text" placeholder="Estado"></td>
                    <td><input class="form-control" type="text" placeholder="Staff"></td>
                </tr>
            </tfoot>
        </table>


    </section>


    @include('layouts.cargando.cargando')

    @include("layouts/validation-viewport")
    
    <div class="modal fade" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="infomodal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title">Información</h5>
                </div>
                <div class="modal-body">
                    <div class="row pb-3">
                        <div class="col-md-12">
                            <ol>
                                <li>Puede visualizar el detalle de cada una de sus clases.</li>
                                <li>Puede ordenar/agrupar los datos, dando click en las columnas.</li>
                                <li>El total de sus asistencias y justificaciones se encuentra en la parte superio derecha.</li>
                                <li>De click en el boton que dice "Top de clases" para ver quienes son los alumnos con mayor cantidad de clases asistidas.</li>
                                <li>Si esta en la vista de "top de clases" puede regresar a su reporte detallado dando click en "Historial detallado"</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

@stop
@section('script')
<script src="/js/kavv/kavvdt.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="http://cdn.datatables.net/plug-ins/1.10.20/filtering/type-based/accent-neutralise.js"></script>
<script>
    var data;
    var row, dt_class, dt_top;
    $(document).ready(function(){
        $.get("/secure", function(res){
            $("body").css("display","block");
            var class_columns = [{"width":"10%"},{"width":"20%"},{"width":"20%"},{"width":"20%"},{"width":"10%"},{"width":"20%"}];
            var visible_row =  [[20, 10, -1], [20, 10, "Todos"]];
            dt_class = createdt($('#t-class'),{col:1,com:"desc",visible_row:visible_row, columns_width:class_columns});

            //Genera los indices de cada fila dinamicamente
            dt_class.on( 'order.dt search.dt', function () {
                dt_class.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();

            //Genera el proceso de busqueda en cada una de las columnas
            dt_class.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
            //Reubica las busquedas personalizadas
            $(".dataTables_scrollFoot").prependTo(".dataTables_scroll");
        }).fail(function() {
            location.href = "/"; 
        });
    });
    

</script>
@stop