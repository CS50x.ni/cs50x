@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display:none;
        }
        .pass-message{
            color: red;
        }
        .input-error{
            box-shadow: 0px 0px 3px rgba(255, 0, 0, 1);
        }
    </style>
@stop
@section('content')

    <section>
        <div id="mensaje"></div>
        <div class="row mt-5 d-flex justify-content-center">
            <div class="col-md-3 text-center">
                <label for="">Contraseña actual</label>
                <select id="cycle" class="form-control">
                    @foreach($cycles as $cycle)
                        @if(session("cycle") == $cycle->id)
                        <option value="{{$cycle->id}}" selected>{{$cycle->name}}</option>
                        @else    
                        <option value="{{$cycle->id}}">{{$cycle->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="row mt-3 d-flex justify-content-center">
            <div class="col-md-6 text-center">
                <button class="btn btn-primary" id="btn-save">Guardar</button>
            </div>
        </div>
    </section>


    @include('layouts.cargando.cargando')
@stop
@section('script')
<script src="/js/kavv/full_height.js"></script>
<script>
    $(document).ready(function(){
        $.get("/secure", function(res){
            $("body").css("display","block");
        }).fail(function() {
            location.href = "/"; 
        });
    });
    

    $("#btn-save").click(function(){
        var ruta = "/usuario/changecycle";
        $("#mensaje").slideUp();
        $("#btn-save").attr("disabled",true);
        $("#loading").css('display','block');
        
        return $.ajax({
            url: ruta,
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            type: 'POST',
            dataType: 'json',
            data:{'cycle':$("#cycle").val()},
            success: function(res){
                $("#loading").css('display','none');
                $("#btn-save").attr("disabled",false);

                if(res.code == 200)
                {
                    message(res.message,{manual:true,tipo:"success"});
                }
                else if(res.code == 1)
                {
                    message(res.message,{manual:true,tipo:"danger"});
                }
                $("#mensaje").slideDown();
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            $("#loading").css('display','none');
            $("#btn-save").attr("disabled",false);
            message(jqXHR,{tipo:"danger"});
            $("#mensaje").slideDown();
        });
    });
</script>
@stop