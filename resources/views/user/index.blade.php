@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display:none;
        }
        .pass-message{
            color: red;
        }
        .input-error{
            box-shadow: 0px 0px 3px rgba(255, 0, 0, 1);
        }
    </style>
@stop
@section('content')

    <section>
    
            <div id="mensaje"></div>
            <div class="row mt-5 d-flex justify-content-center">
                <div class="col-md-6 text-center">
                    <label for="">Contraseña actual</label>
                    <input class="form-control" type="password" name="password-old" id="password-old">
                </div>
            </div>
            <div class="row mt-3 d-flex justify-content-center">
                <div class="col-md-6 text-center">
                    <label for="">Nueva contraseña</label>
                    <input class="form-control" type="password" name="password-new" id="password-new" onkeyup="validation();">
                    <div class="text-left">
                        <spam id="message1" class="pass-message"></spam>
                    </div>
                </div>
            </div>
            <div class="row mt-3 d-flex justify-content-center">
                <div class="col-md-6 text-center">
                    <label for="">Confirme la contraseña</label>
                    <input class="form-control" type="password" name="password-ok" id="password-ok" onkeyup="validation();">
                    <div class="text-left">
                        <spam id="message1" class="pass-message"></spam>
                    </div>
                </div>
            </div>
            <div class="row mt-3 d-flex justify-content-center">
                <div class="col-md-6 text-center">
                    <button class="btn btn-primary" id="btn-save" disabled>Guardar</button>
                </div>
            </div>
    </section>


    @include('layouts.cargando.cargando')
@stop
@section('script')
<script src="/js/kavv/full_height.js"></script>
<script>
    $(document).ready(function(){
        $.get("/secure", function(res){
            $("body").css("display","block");
        }).fail(function() {
            location.href = "/"; 
        });
    });
    
    function validation()
    {
        var pass_new = $("#password-new").val();
        var pass_ok = $("#password-ok").val();

        if(pass_new.length < 6)
        {
            $("#message1").text("La longitud de la contraseña debe ser mayor a 5");
        }
        else
        {
            $("#message1").text("");
            if(pass_new != "" && pass_ok != "")
            {
                if(pass_new != pass_ok)
                {
                    $(".pass-message").text("Las contraseñas no coinciden");
                    $("#btn-save").attr('disabled',true);
                }
                else
                {
                    $("#btn-save").attr('disabled',false);
                    $(".pass-message").text("");
                }
            }   
        }
    }

    $("#btn-save").click(function(){
        var ruta = "/usuario/changepassword";
        $("#mensaje").slideUp();
        $("#btn-save").attr("disabled",true);
        $("#loading").css('display','block');
        $("form input").removeClass("input-error");
        return $.ajax({
            url: ruta,
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            type: 'POST',
            dataType: 'json',
            data:{'password-old':$("#password-old").val(),'password-new':$("#password-new").val(),'password-ok':$("#password-ok").val()},
            success: function(res){
                $("#loading").css('display','none');
                $("#btn-save").attr("disabled",false);

                if(res.code == 200)
                {
                    message(res.message,{manual:true,tipo:"success"});
                    $("#password-old").val("");
                    $("#password-new").val("");
                    $("#password-ok").val("");
                }
                else if(res.code == 1)
                {
                    message(res.message,{manual:true,tipo:"danger"});
                    $("#password-old").addClass("input-error");
                }
                else if(res.code == 2)
                {
                    message(res.message,{manual:true,tipo:"danger"});
                    $("#password-new").addClass("input-error");
                    $("#password-ok").addClass("input-error");
                    $(".pass-message").text("Las contraseñas no coinciden");
                }
                $("#mensaje").slideDown();
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            $("#loading").css('display','none');
            $("#btn-save").attr("disabled",false);
            message(jqXHR,{tipo:"danger"});
            $("#mensaje").slideDown();
        });
    });
</script>
@stop