<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PSET 0</title>
    <link rel="stylesheet" href="/css/docs/main.css">
</head>

<body>
    <div id="header">
        <div id="content">
            <h1 class="psetheader">Problem Set 0</h1>
            <div class="sect1">
                <h2 id="tl-dr" class=""><a class="psetheader" href="#tl-dr">tl;dr</a></h2>
                <div class="sectionbody">
                    <div class="olist arabic">
                        <ol class="arabic" start="0">
                            <li>Mira el material de la <a href="/y21c1#semana2" target="_blank"
                                    rel="noopener noreferrer">semana 2</a>.</li>
                            <li>Programa en Scratch.</li>
                            <li>Sube un archivo.</li>
                            <li>Llena el formulario.</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="separador"></div>
            <div class="sect1">
                <h2 id="ayuda" class=""><a class="psetheader" href="#ayuda">Ayuda</a></h2>
                <div class="sectionbody">
                    <div class="ulist">
                        <ul>
                            <li>Mira la <a href="https://www.youtube.com/watch?v=T8RWhT3rgX0" target="_blank"
                                    rel="noopener noreferrer">Inducción de Zamyla</a>.</li>
                            <li>Preséntate a las Office Hours.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="separador"></div>
            <div class="sect1" style="margin-top: 35px;">
                <h2 id="ansioso-por-programar"><a class="psetheader" href="#ansioso-por-programar">¿Ansioso por
                        programar?</a></h2>
                <div class="sectionbody">
                    <div class="paragraph">Dirígete a <a target="_blank" class="bare"
                            href="https://scratch.mit.edu/">https://scratch.mit.edu/</a> y regístrate. Para crear una
                        cuenta
                        en el sitio web del MIT, haz clic en <b>Únete a Scratch</b> en la parte superior de la página.
                        Cualquier nombre de usuario (que esté disponible) está bien, pero asegúrate de recordarlo así
                        como
                        también tu elección de contraseña.</div>
                    <div class="paragraph">Luego dirígete a <a target="_blank" class="bare"
                            href="https://scratch.mit.edu/help/">https://scratch.mit.edu/help/</a> y toma nota de los
                        recursos disponibles para tí antes de que te sumerjas en Scratch. En particular, tal
                        vez
                        quisieras hacer una lectura rápida a la <a target="_blank"
                            href="https://cdn.scratch.mit.edu/scratchr2/static/__95f8025b5d5663c8eca07b96a66ef8d6__/pdfs/help/Getting-Started-Guide-Scratch2.pdf">Guía de
                            inicio</a> o bien a <a target="_blank" href="https://scratch.mit.edu/ideas">Tutoriales de
                            Scratch (elige tu
                            idioma preferido)</a>.</div>
                    <div class="paragraph">Ahora, prueba el <em>Pikachu’s Pastry Catch</em> elaborado por el (ex)
                        alumno, ¡Gabe Walker! Haz clic en la bandera verde y luego, según las instrucciones de Gabe,
                        presiona la barra espaciadora de tu teclado, ¡en este punto el juego comienza! Siéntete libre de
                        procrastinar un poquito.</div>
                    <iframe src="https://scratch.mit.edu/projects/embed/26329354/?autostart=false" width="485"
                        height="402" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                    <div class="paragraph">Si eres curioso, el código fuente de Gabe puede ser visto en <a
                            target="_blank" class="bare"
                            href="http://scratch.mit.edu/projects/26329354/">http://scratch.mit.edu/projects/26329354/</a>.
                        (Puedes poner el juego en pantalla completa en esa misma URL, ya que la pantalla completa del
                        juego
                        incrustado en esta página web puede que no funcione.)</div>
                    <!-- POR AQUI VOY -->
                    <div class="paragraph">Ahora, asegúrate de conocer qué cosas son reciclables y qué son compostables
                        hoy en día al probar este remix de <em>Oscartime</em> elaborado por ¡Jordan Hayashi!</div>
                    <iframe src="https://scratch.mit.edu/projects/embed/71161586/?autostart=false" width="485"
                        height="402" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
                    <div class="paragraph">El código fuente de Jordan se puede encontrar en <a target="_blank"
                            class="bare"
                            href="https://scratch.mit.edu/projects/71161586/">https://scratch.mit.edu/projects/71161586/</a>.
                        (Puedes poner el juego en pantalla completa en esa misma URL).</div>
                    <div class="paragraph">Si no has experimentado (o no te sientes cómodo) con la programación, puedes
                        estar seguro de que los proyectos de Gabe y Jordan son más complejos de lo que esperamos para
                        este
                        primer problema. (Haz clic en el botón <strong>Ver dentro</strong> en la esquina
                        superior derecha de Scratch para mirar los detalles de implementación debajo de cada proyecto.)
                        Sin
                        embargo, ellos revelan lo que puede hacer en Scratch.</div>
                    <div class="paragraph">
                        <!-- POR ACA VOY -->
                        De hecho, para una introducción más moderada a Scratch (y en general a la programación), tu
                        quisieras revisar algunos de los ejemplos que hemos visto en la semana 0, el “código fuente” lo
                        puedes encontrar en <a target="_blank" class="bare"
                            href="https://scratch.mit.edu/studios/3003963/">https://scratch.mit.edu/studios/3003963/</a>.
                        Una vez que te digas a tí mismo, “Oh, creo que ya lo entiendo”, estás listo para proceder.
                    </div>
                    <div class="paragraph">¡Ahora es el tiempo de escoger tu propia aventura! Tu misión es bastante
                        simple:
                        divertirte con Scratch e implementar el proyecto de tu elección (puede ser una animación, un
                        juego,
                        un arte interactivo, o cualquier otra cosa), sujeto únicamente a los siguientes requerimientos:
                    </div>
                    <div class="ulist">
                        <ul>
                            <li>Tu proyecto debe tener al menos dos sprites, al menos uno de ellos debe parecerse a algo
                                distinto de un gato.</li>
                            <li>Tu proyecto debe tener al menos tres conjuntos de instrucciones en total (por ejemplo,
                                no es
                                necesario que haya tres por sprite).</li>
                            <li>Tu proyecto debe usar al menos una condición, un bucle, y una variable.</li>
                            <li>Tu proyecto debe usar al menos un sonido.</li>
                            <li>Tu proyecto debe ser más complejo que la mayoría de los que se mostraron en la clase
                                (muchos
                                de ellos, aunque eran instructivos, eran bastante pequeños) pero puede ser menos
                                complejo
                                que Oscartime. Dado lo anterior, tu proyecto probablemente al menos debería usar una
                                docena
                                de piezas en total.</li>
                        </ul>
                    </div>
                    <div class="paragraph">Siéntete libre de leer detenidamente <a target="_blank"
                            href="https://scratch.mit.edu/studios/3009443/">algunos de los proyectos del último año</a> para
                            inspirarte. Pero tu proyecto no debería ser increíblemente similar a cualquiera de ellos.
                        Trata
                        de pensar en una idea propia y luego ponte a implementarla. No trates de implementar de una sola
                        vez
                        la totalidad de tu idea, agrega piezas una por una. Para darte un ejemplo, Gabe probablemente
                        primero implementó solo una torta (pastry) antes de a los otros sprites del juego. En otras
                        palabras, da pasitos de bebé: escribe un poco del código (por ejemplo, arrastra y suelta algunas
                        piezas del rompecabezas), pruébalo, escribe un poco más, vuelve a probar y así sucesivamente.
                    </div>
                    <div class="paragraph">Si, en el camino, encuentras que una característica es muy difícil
                        implementar,
                        intenta no alterarte; modifica tu diseño o trabaja alrededor del problema. Si te decidiste a
                        implementar una idea que encuentras graciosa, no debería ser difícil de satisfacer los
                        requerimientos de de este conjunto de problemas (pset).</div>
                    <div class="paragraph">Bueno, te dejamos hacer lo tuyo. ¡Haznos sentir orgullosos!</div>
                    <div class="paragraph">Por cierto, si no tienes el mejor acceso a internet, eres bienvenido a
                        descargar
                        el editor offline de Scratch en <a target="_blank" class="bare"
                            href="https://scratch.mit.edu/scratch2download/">https://scratch.mit.edu/scratch2download/</a>.
                        Sin embargo, una vez que hayas terminado tu proyecto offline, asegúrate de subirlo a tu cuenta
                        en <a class="bare" href="http://scratch.mit.edu/" target="_blank">http://scratch.mit.edu/</a>
                        vía
                        Archivo > Compartir
                        en el editor offline.</div>
                    <div class="paragraph">Una vez que hayas terminado tu proyecto, haz clic en “Ver propiedades de
                        página”
                        en la esquina superior derecha. Asegúrate de que tu proyecto tenga título (en la esquina
                        superior
                        izquierda), algunas instrucciones (en la esquina superior derecha) y algunas notas y/o créditos
                        (en
                        la esquina inferior derecha). Luego haz clic en <strong>Compartir</strong> en la esquina
                        superior
                        derecha de tal forma que otros puedan ver tu proyecto. Finalmente, guarda la URL que aparece en
                        la
                        barra de direcciones de tu navegador. Esa es la URL de tu proyecto en el sitio del MIT, y la vas
                        a
                        necesitar luego.</div>
                    <!--
    <div class="paragraph">
                        Oh, y si quieres exhibir tu proyecto en el estudio del Y19C1, dirígete a <a class="bare" href="https://scratch.mit.edu/studios/4248580/">https://scratch.mit.edu/studios/4248580/</a>, luego haz clic en <strong>Agregar proyectos</strong> y pega la URL de tu proyecto.</div>
    -->

                </div>
            </div>
            <div class="separador"></div>
            <div class="sect1" style="margin-top: 35px;">
                <h2 id="como-enviarlo" class=""><a class="psetheader" href="#como-enviarlo">¿Cómo enviarlo?</a></h2>
                <div class="sectionbody">
                    <div class="sect2" style="margin-top: 15px;">
                        <h3 id="step-1-of-2" class=""><a class="psetheader " href="#step-1-of-2">Paso 1 de 2</a></h3>
                        <div class="olist arabic">
                            <ol class="arabic">
                                <li>Entrar a tu cuenta de Scratch <a href="https://scratch.mit.edu" target="_blank"
                                        rel="noopener noreferrer">https://scratch.mit.edu</a> e inicia sesión</li>
                                <li>Ve a la seccion de tus proyectos... ¿cómo? si ya <strong>inciaste sesión</strong>
                                    podrás
                                    acceder desde este enlace <a href="https://scratch.mit.edu/mystuff/" target="_blank"
                                        rel="noopener noreferrer">https://scratch.mit.edu/mystuff/</a></li>
                            </ol>
                        </div>
                    </div>
                    <div class="sect2" style="margin-top: 15px;">
                        <h3 id="step-2-of-2" class=""><a class="psetheader" href="#step-2-of-2">Paso 2 de 2</a></h3>
                        <div class="paragraph">Completar el siguiente formulario a más tardar el día 8 de febrero del
                            año
                            2021.
                        </div>
                        <div class="paragraph">Nota: El campo en el formulario que dice "enlace compartido de Scratch"
                            es la
                            <strong>URL</strong> del proyecto que quiera enviarnos para ser revisado como su
                            <strong>Pset0</strong>.
                        </div>
                        <div class="paragraph">
                            <iframe
                                src="https://docs.google.com/forms/d/e/1FAIpQLSc7-Pi9lSlYp943Vfk-ZCKSDpMhFxIxLuoa0svH4YIz4Fx1FQ/viewform?embedded=true"
                                width="640" height="1400" frameborder="0" marginheight="0"
                                marginwidth="0">Cargando…</iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="separador"></div>
        <div class="sect1" style="margin-top: 35px;">
            <h2 id="registro-de-cambios" class=""><a class="psetheader" href="#registro-de-cambios">Registro de
                    cambios</a>
            </h2>
            <div class="sectionbody">
                <ul>
                    <li>28-01-2021
                        <ul>
                            <li>Version Inicial</li>
                            <ul>
                                <li><a href="http://code-fu.net.ni/staff/#kevin-valverde" target="_blank"
                                        rel="noopener noreferrer">Kevin Valverde</a>
                                </li>
                            </ul>
                            <li>Traducción
                                <ul>
                                    <li><a href="http://code-fu.net.ni/staff/#hans-castro" target="_blank"
                                            rel="noopener noreferrer">Hans Castro</a>
                                    </li>
                                </ul>
                            </li>
                            <li>Colaboración</li>
                            <ul>
                                <li>
                                    <a href="http://code-fu.net.ni/staff/#luisangel-marcia" target="_blank"
                                        rel="noopener noreferrer">Luisángel Marcia</a>
                                </li>
                                <li>
                                    <a href="http://code-fu.net.ni/staff/#eduardo-mayorga" target="_blank"
                                        rel="noopener noreferrer">Eduardo Mayorga</a>
                                </li>
                            </ul>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <button id="botonmenumostrar"
        class="et_pb_button et_pb_button_0 et_pb_module et_pb_bg_layout_light boton-contenidos"
        style="background-color: #fff !important; color: #a51c30 !important; border-color: #a51C30 !important;">+
        Contenidos</button>

    <nav id="botonmenu" class="menu-emergente" style="display: none;">
        <div>Tabla de contenidos:</div>
        <a href="#tl-dr">tl;dr</a><br>
        <a href="#ayuda">Ayuda</a><br>
        <a href="#ansioso-por-programar">¿Ansioso por programar?</a><br>
        <a href="#como-enviarlo">¿Cómo enviarlo?</a><br>
        <a href="#registro-de-cambios">Registro de cambios</a><br>
    </nav>
</body>

</html>
