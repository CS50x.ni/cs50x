<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PSET 0</title>
    <link rel="stylesheet" href="/css/web50/main.css">
</head>
<body>
    <div id="header">
        <div id="content">
            <h1 class="psetheader">Project 0: Homepage</h1>
            <div class="sect1">
                <h2 id="tl-dr" class=""><a class="psetheader" href="#tl-dr">Objetivos</a></h2>
                <div class="sectionbody">
                    <div class="olist arabic">
                        <ol class="arabic" start="0">
                            <li>Mirar el material de la <a href="https://docs.code-fu.net.ni/web50-y21c1/#lecture2" target="_blank" rel="noopener noreferrer">Semana 2</a>.</li>
                            <li>Familiarizarse más con HTML y CSS para diseñar páginas web estilizadas</li>
                            <li>Aprender a usar SCSS para crear hojas de estilo más complejas para tus páginas</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="separador"></div>
            <div class="sect1" style="margin-top: 35px;">
                <h2 id="ansioso-por-programar"><a class="psetheader" href="#ansioso-por-programar">Para Empezar</a></h2>
                <div class="sectionbody">
                    <div class="paragraph">Descarga el codigo distribuido desde <a class="bare" href="https://cdn.cs50.net/web/2020/x/projects/0/project0.zip">aquí</a> y extraelo, lo que deberia resultar en un directorio llamado project0.
                        Luego, en una terminal (dependiendo del sistema  operativo que uses), accede al directorio extraído project0, mediante el siguiente comando:
                    </div>
                        <code> cd project0</code> <br>
                    <div class="paragraph">
                        para entrar al directorio del proyecto, ahora debes ejecutar
                    </div>
                        <code> touch index.html </code> <br>
    
                    <div class="paragraph"> Para crear un archivo index.html en tu repositorio. Abre el archivo con tu editor de texto favorito (Puedes usar <a href="https://atom.io/" class="bare">Atom</a>, <a href="https://www.sublimetext.com/" class="bare">Sublime Text</a>, y <a href="https://code.visualstudio.com/" class="bare">VS Code</a>. Luego copia y pega lo sigiente:
                    </div>
                            <pre>
                                &#60;!DOCTYPE html&#62;
                                &#60;html&#62;
                                    &#60;head&#62;
                                        &#60;title>My Webpage&#60;/title&#62;
                                    &#60;/head&#62;
                                    &#60;body&#62;
                                        Hello, world!
                                    &#60;/body&#62;
                                &#60;/html&#62;
                            </pre>
                            <br>
                    <div class="paragraph">Finalmente guarda tu archivo index.html</div>
                </div>
            </div>
            <div class="separador"></div>
            <div class="sect1" style="margin-top: 35px;">
                <h2 id="requerimientos"><a class="psetheader" href="#requerimientos">Requerimientos</a></h2>
                <div class="sectionbody">
                    <div class="paragraph"> Ok, es momento de que crees tu propio sitio web. Diseña una página sobre ti mismo, 
                        alguno de tus intereses o algún otro tópico de tu interés. El tema a tratar, la apariencia y el diseño 
                        del sitio están enteramente a tu interés, siempre que cumplas los siguientes requisitos:
                    </div>
                    <div class="ulist">
                        <ul>
                            <li>Tu sitio debe contener al menos 4 diferentes páginas .html, y debe ser posible entrar desde una página hasta otra siguiendo uno o más hipervínculos.</li>
                            <li>Tu sitio debe contener al menos una lista (ordenada o desordenada), al menos una tabla y al menos una imagen.</li>
                            <li>Tu sitio debe contener al menos una hoja de estilos.</li>
                            <li>Tu sitio debe usar al menos 5 diferentes propiedades CSS, y al menos 5 selectores diferentes. Debes usar el selector de #id y el de .class al menos una vez.</li>
                            <li>Tu sitio debe contener al menos un selector responsivo @media query, que debe aplicarse a pantallas más pequeñas.</li>
                            <li>Debes usar bootstrap 4 en tu sitio, para utilizar al menos un componente de bootstrap, y al menos dos columnas de bootstrap usando su grid layout.</li>
                            <li>Tus hojas de estilo deben contener al menos una variable SCSS, al menos un ejemplo de anidamiento SCSS y al menos un uso de herencia SCSS.</li>
                            <li>En README.md, incluye una breve descripción de tu proyecto, opcionalmente, que contiene cada archivo, y cualquier otra información adicional para el staff.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="separador"></div>
            <div class="sect1" style="margin-top: 35px;">
                <h2 id="como-enviarlo" class=""><a class="psetheader" href="#como-enviarlo">¿Cómo enviarlo?</a></h2>
                <div class="sectionbody">
                    <div class="sect2" style="margin-top: 15px;">
                        <h3 id="step-1-of-2" class=""><a class="psetheader " href="#step-1-of-2">Paso 1 de 2</a></h3>
                        <div class="olist arabic">
                            <ol class="arabic">
                                <li>Sube tu proyecto al <a href="https://ide.cs50.io/" target="_blank"
                                        rel="noopener noreferrer">IDE</a> utilizando el siguiente arbol de ficheros "WEB50xni/Projects/Project0"</li>
                                <li>Sube tu proyecto a <a href="https://www.github.com" target="_blank"
                                        rel="noopener noreferrer">github</a> en un repositorio <strong>publico</strong> (por esta vez) llamado "cs50w-project0" </li>
                                <li>Utiliza Github Pages para publicar tu Homepage y envianos el link en el formulario de más abajo.
                                    
                                </li>
                                <li>Comparte tu repositorio de Github al usuario "WEB50XNI" o bien con el correo "web50xni@code-fu.net.ni"</li>
                            </ol>
                        </div>
                    </div>
                    <div class="sect2" style="margin-top: 15px;">
                        <h3 id="step-2-of-2" class=""><a class="psetheader" href="#step-2-of-2">Paso 2 de 2</a></h3>
                        <div class="paragraph">Completar el siguiente formulario a más tardar el día viernes 19 de febrero de 2021.
                        </div>
                        <div class="paragraph">
                            <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSd7V1-9TeNbNi4jl8zPkf_rlWvx6_78UQMdxMGZkzoOsIDa6Q/viewform?embedded=true" width="640" height="1055" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="separador"></div>
        <div class="sect1" style="margin-top: 35px;">
            <h2 id="registro-de-cambios" class=""><a class="psetheader" href="#registro-de-cambios">Registro de cambios</a>
            </h2>
            <div class="sectionbody">
                <ul>
                    <li>26/01/2021
                        <ul>
                            <li>Version Inicial</li>
                            <ul>
                                <li><a href="http://code-fu.net.ni/staff/#ricardo-chavez" target="_blank" rel="noopener noreferrer">Ricardo Chávez</a></li>
                            </ul>
                            <li>Traducción
                                <ul>
                                    <li><a href="http://code-fu.net.ni/staff/#luisangel-marcia" target="_blank"
                                            rel="noopener noreferrer">Luisangel Marcia</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <button id="botonmenumostrar" class="et_pb_button et_pb_button_0 et_pb_module et_pb_bg_layout_light boton-contenidos"
        style="background-color: #fff !important; color: #a51c30 !important; border-color: #a51C30 !important; ">+
        Contenidos</button>
    
    <nav id="botonmenu" class="menu-emergente" style="display: none;">
        <div>Tabla de contenidos:</div>
        <a href="#tl-dr">Objetivos</a><br>
        <a href="#ansioso-por-programar">Para Empezar</a><br>
        <a href="#requerimientos">Objetivos</a><br>
        <a href="#como-enviarlo">¿Cómo enviarlo?</a><br>
        <a href="#registro-de-cambios">Registro de cambios</a><br>
    </nav>
</body>
</html>