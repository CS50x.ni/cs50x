@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
        .space-0{
            background: #f25656!important;
        }
        .oh{
            background: #888888;
            cursor: pointer;
            transition: .3s;
        }
        .oh-block {
            background: #cbcbcb;
        }
        table td, table th {
            border: 1px solid #0000005c!important;
        }
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display:none;
        }
        .td-black
        {
            color: #fff;
            background: #000000;
            border-bottom: 1.4px solid #ffffff!important;
        }
        .th-gray{
            color:#fff;
            background: #3b3b3b;
        }
        .dates {
            background-color: white;
            box-shadow: 2px 2px 20px 0px black;
            border-radius: 25px;
        }
        .cabecera{
            background: #6c757d;
            color: #fff;
        }
        .tday{
            box-shadow: 4px 4px 15px 0px black;
        }
        
        .btn-hour {
            transition: .3s;
            cursor: pointer;
        }

        .btn-hour:hover {
            background: #fff;
            color: #000;
        }

        .btn-hour:after {
            content: "\f06e"; /* Valor unicode */
            font-family: FontAwesome;
            font-style: normal;
            font-weight: normal;
            text-decoration: inherit;

            /* Estos ajustes son opcionales, se aplican con la finalidad de dar diseño */
            display: inline-block;
            font-size: 18px;
            color: #000;
            margin-left: 10px;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" >
    
    
@stop
@section('content')
    
    <div class="row text-center">
        <h2 for="" class="cabecera col-md-12 py-3">Assistencias a clases del ciclo Y21C1</h2>
    </div>
    <section >
        
        <div id="mensaje"></div>
        <table id="t-students" class="table table-hover text-center" cellspacing="0" style="width:100%;">
            <thead>
                <tr>
                    <th data-orderable="false"></th>
                    <th>Correo</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Clase</th>
                    <th>Grupo</th>
                    <th>Justificado</th>
                    <th>Comentario</th>
                    <th>Staff</th>
                    <th data-orderable="false"></th>
                </tr>
            </thead>
            <tbody id="tb-reservations">
                <?php use App\Student;?>
                @foreach($all_assistances as $key=>$class_assistances)
                
                    @foreach($class_assistances as $assistance)
                    
                        <tr>
                            <td></td>
                            <td>{{$assistance->email}}</td>
                            <td>{{$assistance->name}}</td>
                            <td>{{$assistance->last_name}}</td>
                            <td>{{$class_name[$key]}}</td>
                            <td>{{$assistance->pivot->class_group}}</td>
                            <td>@if($assistance->pivot->justified) Sí @else No @endif</td>
                            <td>{{$assistance->pivot->comment}}</td>
                            <td>{{substr(Student::find($assistance->pivot->staff_id)->code,6)}}</td>
                            
                            <td>
                                <button type="button" class="btn-edit btn btn-warning fa fa-pencil" data-student="{{$assistance->student}}" value="{{$assistance->pivot->id}}"></button>
                                <button type="button" class="btn-delete btn btn-danger fa fa-trash" data-student="{{$assistance->student}}" value="{{$assistance->pivot->id}}"></button>
                            </td>
                        </tr>
                    @endforeach
                @endforeach
            </tbody>
            <tfoot id="tb-foot">
                <tr>
                    <td></td>
                    <td><input class="form-control" type="text" placeholder="Correo"></td>
                    <td><input class="form-control" type="text" placeholder="Nombre"></td>
                    <td><input class="form-control" type="text" placeholder="Apellido"></td>
                    <td><input class="form-control" type="text" placeholder="Clase"></td>
                    <td><input class="form-control" type="text" placeholder="Grupo"></td>
                    <td><input class="form-control" type="text" placeholder="Justificado"></td>
                    <td><input class="form-control" type="text" placeholder="Comentario"></td>
                    <td><input class="form-control" type="text" placeholder="Staff"></td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
        
    {{--Modal de edición--}}
    <div class="modal fade" id="modal-assistance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title"></h5>
                </div>
                <div class="modal-body" id="m-body" style="overflow: overlay;">

                    <div class="row text-center">
                        <div class="col-md-2">
                            <label for="">Clase</label>
                            <select class="form-control" name="class" id="class">
                                @foreach($lessons as $class)
                                    <option value="{{$class->id}}" data-info="{{$class->name}}">{{$class->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-2">
                            <label for="">Justificado</label>
                            <select class="form-control" name="justified" id="justified">
                                <option value="0" data-info="No">No</option>
                                <option value="1" data-info="Sí">Sí</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label for="">Grupo</label>
                            <select class="form-control" name="group" id="group">
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                                <option value="D">D</option>
                                <option value="E">E</option>
                                <option value="F">F</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="">Comentario</label>
                            <textarea class='form-control' name='comment' class='comment' rows='1' id="comment"></textarea>
                        </div>

                        <div class="col-md-2">
                            <label for="">Guardar</label>
                            <div>
                                <button type="button" id="btn-edit" class="btn btn-block btn-success fa fa-save btn-save" value="2" onclick="save(this);"></button>
                                <button type="button" id="btn-delete" class="btn btn-danger btn-block fa fa-trash btn-save" value="3" onclick="save(this);"></button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.cargando.cargando')
    @include("layouts/validation-viewport")
@stop
@section('script')
<script src="/js/kavv/kavvdt.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="http://cdn.datatables.net/plug-ins/1.10.20/filtering/type-based/accent-neutralise.js"></script>

<script>
    var dt = null;
    var data= new Object()
    var row;

    $(document).ready(function(){
        $.get("/secure", function(res){
            $("body").css("display","block");
            var columns = [{"width":"5%"},{"width":"15%"},{"width":"15%"},{"width":"15%"},{"width":"5%"},{"width":"5%"},{"width":"5%"},{"width":"15%"},{"width":"15%"},{"width":"5%"}];
            var visible_row =  [[30, 20, 10, -1], [30, 20, 10, "Todo"]];
            dt = createdt($('#t-students'),{col:4,com:"desc",visible_row:visible_row, columns_width:columns});

            //Genera los indices de cada fila dinamicamente
            dt.on( 'order.dt search.dt', function () {
                dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();

            //Genera el proceso de busqueda en cada una de las columnas
            dt.columns().every( function () {
                var that = this;
        
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
            //Reubica las busquedas personalizadas
            $(".dataTables_scrollFoot").prependTo(".dataTables_scroll");

            $("#loading").css('z-index',1060);
        }).fail(function() {
            location.href = "/"; 
        });
    });
    
    $(".btn-edit").click(function(){
        $("#btn-delete").hide();
        $("#btn-edit").show();
        $("#modal-assistance").modal("show");

        data.student = $(this).data("student");
        data.class_edit = $(this).val();
        row = $(this).parents('tr');
        
        refill();
    });
    $(".btn-delete").click(function(){
        $("#btn-delete").show();
        $("#btn-edit").hide();
        $("#modal-assistance").modal("show");

        data.student = $(this).data("student");
        data.class_edit = $(this).val();
        row = $(this).parents('tr');
        refill();
    });

    function refill()
    {
        var old_class = row.children('td')[4].innerText;
        var old_group = row.children('td')[5].innerText;
        var justified = row.children('td')[6].innerText;
        $("#class option[data-info='"+old_class+"']").attr("selected",true);
        $("#group option[value='"+old_group+"']").attr("selected",true);
        $("#justified option[data-info='"+justified+"']").attr("selected",true);
        $("#comment").val(row.children('td')[7].innerText);
        data.old_class = $("#class").val();
    }

    function save(element)
    {
        var ruta = "/class";
        //ocultamos el msj
        $("#mensaje").slideUp();
        //Deshabilitamos los botones para guardar
        $(".btn-save").attr("disabled",true);
        
        //Guardamos los datos que se enviaran al controlador en el obj data.
        data.class = $("#class").val();
        data.group = $("#group").val();
        data.justified = $("#justified").val();
        data.comment = $("#comment").val();
        data.option = $(element).val();
        //Mostramos la vista de carga
        $("#loading").css("display","block");
        //Consulta ajax
        return $.ajax({
            url: ruta,
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(res){
                if(res.code === 200)
                {
                    //Editar
                    if(data.option == 2)
                    {
                        //Actualizamos la celda de clases
                        dt.cell(row.children('td')[4]).data($("#class option:selected").text());
                        //Actualizamos la celda de Grupos
                        dt.cell(row.children('td')[5]).data($("#group option:selected").text());
                        //Actualizamos la celda de Justificación
                        dt.cell(row.children('td')[6]).data($("#justified option:selected").text());
                        //Actualizamos la celda de Comentario
                        dt.cell(row.children('td')[7]).data($("#comment").val());
                    }
                    //Eliminar
                    else if(data.option == 3)
                    {
                        dt.row(row).remove().draw( false );
                    }
                    //Mensaje
                    message(res.message,{manual:true,tipo:"success"});
                    //Limpiamos el campo del comentario
                    $("#comment").val("");
                }
                else
                {
                    //Msj de error
                    message(res.message,{manual:true,tipo:"danger"});
                }
                //Ocultamos el modal
                $("#modal-assistance").modal("hide");
                //Mostramos el msj
                $("#mensaje").slideDown();
                //Especificamos que el boton ya no esta deshabilitado
                $(".btn-save").attr("disabled",false);
                //Ocultamos la vista de carga
                $("#loading").css("display","none");
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            //Error desde el servidor

            message(jqXHR,{tipo:"danger"});
            $("#mensaje").slideDown();
            $(".btn-save").attr("disabled",false);
            $("#loading").css("display","none");
            
        });
        
    }
    

</script>
@stop

