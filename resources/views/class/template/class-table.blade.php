
                <?php 
                    use App\Student;
                    use App\Comment;
                ?>
                @foreach($all_assistances as $key=>$assistance)
                    <?php
                    
                        $comment_id = $assistance->pivot->comment_id;
                        $comment = "";
                        if($comment_id != null)
                            $comment = Comment::find($comment_id)->description;

                        $staff_id = $assistance->pivot->staff_id;
                        $staff = "";
                        if($staff_id != null)
                        $staff = substr(Student::find($assistance->pivot->staff_id)->code,6);

                    ?>

                    <tr>
                        <td></td>
                        <td>{{$assistance->email}}</td>
                        <td>{{$assistance->name}}</td>
                        <td>{{$assistance->last_name}}</td>
                        <td>{{$class_name}}</td>
                        <td>{{$assistance->pivot->class_group}}</td>
                        <td>@if($assistance->pivot->justified) Sí @else No @endif</td>
                        <td>{{$comment}}</td>
                        <td>{{$staff}}</td>
                        
                        <td>
                            <button type="button" onclick="edit(this)" class="btn-edit btn btn-warning fa fa-pencil" data-student="{{$assistance->student}}" value="{{$assistance->pivot->id}}"></button>
                            <button type="button" onclick="remove(this)" class="btn-delete btn btn-danger fa fa-trash" data-student="{{$assistance->student}}" value="{{$assistance->pivot->id}}"></button>
                        </td>
                    </tr>
                @endforeach