@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
        .space-0{
            background: #f25656!important;
        }
        .oh{
            background: #888888;
            cursor: pointer;
            transition: .3s;
        }
        .oh-block {
            background: #cbcbcb;
        }
        table td, table th {
            border: 1px solid #0000005c!important;
        }
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display:none;
        }
        .td-black
        {
            color: #fff;
            background: #000000;
            border-bottom: 1.4px solid #ffffff!important;
        }
        .th-gray{
            color:#fff;
            background: #3b3b3b;
        }
        .dates {
            background-color: white;
            box-shadow: 2px 2px 20px 0px black;
            border-radius: 25px;
        }
        .cabecera{
            background: #6c757d;
            color: #fff;
        }
        .tday{
            box-shadow: 4px 4px 15px 0px black;
        }
        
        .btn-hour {
            transition: .3s;
            cursor: pointer;
        }

        .btn-hour:hover {
            background: #fff;
            color: #000;
        }

        .btn-hour:after {
            content: "\f06e"; /* Valor unicode */
            font-family: FontAwesome;
            font-style: normal;
            font-weight: normal;
            text-decoration: inherit;

            /* Estos ajustes son opcionales, se aplican con la finalidad de dar diseño */
            display: inline-block;
            font-size: 18px;
            color: #000;
            margin-left: 10px;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" >
    
    
@stop
@section('content')
    <div class="row text-center">
        <h2 for="" class="cabecera col-md-12 py-3">Lista de alumnos del ciclo Y21C1</h2>
    </div>
    <section >
        
        <div class="row my-3 d-flex justify-content-center">
            <div class="col-md-6">
                <button class="btn-block btn btn-warning" data-toggle="modal" data-target="#modal-info"><strong>Infomación</strong></button>
            </div>
            
        </div>
        
        <div id="mensaje"></div>
        <table id="t-students" class="table table-hover text-center" cellspacing="0" style="width:100%;">
            <thead>
                <tr>
                    <th>Correo</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th data-orderable="false"></th>
                </tr>
            </thead>
            <tbody id="tb-reservations">
                @foreach($students as $student)
                    <tr>
                        <td>{{$student->email}}</td>
                        <td>{{$student->name}}</td>
                        <td>{{$student->last_name}}</td>
                        <td><button class="btn-student btn btn-success fa fa-upload" value="{{$student->id}}"></button></td>
                        
                    </tr>
                @endforeach
            </tbody>
        </table>
        

    <!-- Modal -->
    <div class="modal fade" id="modal-assistance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title"></h5>
                </div>
                <div class="modal-body" id="m-body" style="overflow: overlay;">

                    <div class="row text-center">
                        <div class="col-md-2">
                            <label for="">Clase</label>
                            <select class="form-control" name="class" id="class">
                                @foreach($lessons as $class)
                                    <option value="{{$class->id}}">{{$class->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-2">
                            <label for="">Justificado</label>
                            <select class="form-control" name="justified" id="justified">
                                <option value="0">No</option>
                                <option value="1">Sí</option>
                            </select>
                        </div>
                        
                        <div class="col-md-2">
                            <label for="">Grupo</label>
                            <select class="form-control" name="group" id="group">
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                                <option value="D">D</option>
                                <option value="E">E</option>
                                <option value="F">F</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="">Comentario</label>
                            <textarea class='form-control' name='comment' class='comment' rows='1' id="comment"></textarea>
                        </div>

                        <div class="col-md-2">
                            <label for="">Guardar</label>
                            <div>
                                <button type="button" class="btn btn-block btn-success fa fa-save btn-save" value="1" onclick="save(this);"></button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="infomodal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title">Información</h5>
                </div>
                <div class="modal-body">
                    <div class="row pb-3">
                        <div class="col-md-12">
                            <ol style="list-style: lower-alpha;">
                                <li><strong>¿Cómo subir la asistencia de un alumno?</strong></li>
                                <ol>
                                    <li>Buscar al alumno por nombre o apellido.</li>
                                    <li>Una vez que encontrarón al alumno dar click en <button class="btn btn-success fa fa-upload"></button></li>
                                    <li>Les mostrara un modal.</li>
                                    <li>Deberán seleccionar la clase correspondiente.</li>
                                    <li>Especificar si es justificación.</li>
                                    <li>Opcional hacer un comentario.</li>
                                    <li>Para guardar la asistencia de click en <button class="btn btn-success fa fa-save"></button></li>
                                </ol>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.cargando.cargando')
    @include("layouts/validation-viewport")
@stop
@section('script')
<script src="/js/kavv/kavvdt.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="http://cdn.datatables.net/plug-ins/1.10.20/filtering/type-based/accent-neutralise.js"></script>

<script>
    var dt = null;
    var data= new Object()
    var row;
    $(document).ready(function(){
        $.get("/secure", function(res){
            $("body").css("display","block");
            var columns = [{"width":"30%"},{"width":"30%"},{"width":"30%"},{"width":"10%"}];
            var visible_row =  [[30, 20, 10, -1], [30, 20, 10, "Todo"]];
            dt = createdt($('#t-students'),{col:1,visible_row:visible_row, columns_width:columns});
            $("#loading").css('z-index',1060);
        }).fail(function() {
            location.href = "/"; 
        });
    });
    $(".btn-student").click(function(){
        $("#modal-assistance").modal("show");

        data.student = $(this).val();
    });

    function save(element)
    {
        var ruta = "/class";

        $("#mensaje").slideUp();
        $("#btn-save").attr("disabled",true);
        
        data.class = $("#class").val();
        data.justified = $("#justified").val();
        data.group = $("#group").val();
        data.comment = $("#comment").val();
        data.option = $(element).val();
        $("#loading").css("display","block");

        return $.ajax({
            url: ruta,
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(res){
                if(res.code === 200)
                {
                    message(res.message,{manual:true,tipo:"success"});
                    $("#comment").val("");
                }
                else
                {
                    message(res.message,{manual:true,tipo:"danger"});
                }
                $("#modal-assistance").modal("hide");
                $("#mensaje").slideDown();
                $("#btn-save").attr("disabled",false);
                $("#loading").css("display","none");
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            message(jqXHR,{tipo:"danger"});
            $("#mensaje").slideDown();
            $("#btn-save").attr("disabled",false);
            $("#loading").css("display","none");
            
        });
        
    }
    
</script>
@stop

