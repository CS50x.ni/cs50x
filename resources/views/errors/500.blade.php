<!DOCTYPE html>
<html>
	<head>
	 	<title></title>    
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	</head>
	<body>
		<div class="d-flex justify-content-center align-items-center" style="position: fixed; height: 100%; width: 100%; overflow: auto;">
 			<div class="center_body col-md-12 text-center">
 				<img class="img-fluid mt-5" src="/img/CS50x.png" alt="404">
			  	<h2>PÁGINA NO ENCONTRADA</h2>
			  	<p>
			  		¡Ooooops! ¡Ocurrio un error en la pagina!
			  		<br>
			  		Si el error permanece contacte a los administradores 
			  	</p>
			  	<a href="/" id="btn_principal"> Pagina Principal </a>
 			</div>
		</div>
	</body>
</html>