@extends('layouts.dashboard.dashboard')
@section('style')
    <style>
        .space-0{
            background: #f25656!important;
        }
        .oh{
            background: #888888;
            cursor: pointer;
            transition: .3s;
        }
        .oh-block {
            background: #cbcbcb;
        }
        table td, table th {
            border: 1px solid #0000005c!important;
        }
        section{
            padding-left: 50px;
            padding-right: 50px;
        }
        body{
            display:none;
        }
        .td-black
        {
            color: #fff;
            background: #000000;
            border-bottom: 1.4px solid #ffffff!important;
        }
        .th-gray{
            color:#fff;
            background: #3b3b3b;
        }
        .dates {
            background-color: white;
            box-shadow: 2px 2px 20px 0px black;
            border-radius: 25px;
        }
        .cabecera{
            color: #fff;
            font-family: inherit;
            text-shadow: 3px 5px 5px black;
        }
        .tday{
            box-shadow: 4px 4px 15px 0px black;
        }
        
        .btn-hour {
            transition: .3s;
            cursor: pointer;
        }

        .btn-hour:hover {
            background: #fff;
            color: #000;
        }

        .btn-hour:after {
            content: "\f06e"; /* Valor unicode */
            font-family: FontAwesome;
            font-style: normal;
            font-weight: normal;
            text-decoration: inherit;

            /* Estos ajustes son opcionales, se aplican con la finalidad de dar diseño */
            display: inline-block;
            font-size: 18px;
            color: #000;
            margin-left: 10px;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" >
    
    
@stop
@section('content')
    <section  style="background: #626262;">
        <div class="row text-center">
            <h2 for="" class="cabecera col-md-12 py-3" >Reservaciones de la semana <span id="title-week">{{ substr($last_week["week"]->name,1) }}</span> </h2>
        </div>
        
        <div class="row cabecera text-center">
            <h5 for="" class="col-md-12">Seleccione una semana diferente de ser necesario</h5>
        </div>
        <div class="row text-center d-flex justify-content-center">

            <div class="col-md-3 col-sm-6 input-group mb-4">
                <select class="form-control" name="class" id="select-week">
                    <option value="-1">Semana</option>
                    @foreach($all_weeks as $key => $data)
                        <option value="{{$data->id}}" data-info="{{$data->name}}">Semana {{$key+1}}</option>
                        @if($data->id == $last_week["week"]->id)
                            @break
                        @endif
                    @endforeach
                </select>
                <div class="input-group-append">
                    <button class="btn btn-info fa fa-search" id="search-week"></button>
                </div>
            </div>
        </div>
        <div class="row text-center pb-3">
            <div class="col-md-5 dates mb-2"><h3 id="date-start">Desde: {{$start_week}}</h3></div>
            <div class="col-md-2  mb-2"></div>
            <div class="col-md-5 dates  mb-2"><h3 id="date-close">Hasta: {{$close_week}}</h3></div>
        </div>

        <div class="row mb-3 d-flex justify-content-center">
            <div class="col-md-6">
                <button class="btn-block btn btn-warning" data-toggle="modal" data-target="#modal-info"><strong>Infomación</strong></button>
            </div>
        </div>
        @foreach($hours as $day=>$hour)
            <?php $quantity_block = count($hour["block"]);
                if($day == "Mo")
                    $day = "Lunes";
                else if($day == "Tu")
                    $day = "Martes";
                else if($day == "We")
                    $day = "Miércoles";
                else if($day == "Th")
                    $day = "Jueves";
                else if($day == "Fr")
                    $day = "Viernes";
                else if($day == "Sa")
                    $day = "Sábado";
            ?>
            <div class="row">
                <table id="tday_{{$hour['day_id']}}" data-day={{$day}} class="table table-bordered tday">
                    <thead class="text-center">
                        <tr>
                            <th colspan="5" class="td-black"><h4>{{$day}}</h4></th>
                        </tr>
                        <tr>
                            <th colspan="1" class="td-black"></th>
                            <th colspan="2" class="th-gray">Laboratorio</th>
                            <th colspan="2" class="th-gray">Aula</th>
                        </tr>
                        <tr>
                            <th colspan="1" class="td-black">HORA</th>
                            <th colspan="1" class="th-gray">Computadoras CS50X.ni</th>
                            <th colspan="1" class="th-gray">Laptop personal</th>
                            <th colspan="1" class="th-gray">Computadoras CS50X.ni</th>
                            <th colspan="1" class="th-gray">Laptop personal</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @for($i = 0; $i < $quantity_block; $i++)         
                            @if($hour["hour_id"][$i] != 6)
                                <tr>
                                    <?php 
                                        $day_id = $hour["day_id"];
                                        $hour_id = $hour["hour_id"][$i];
                                    ?>  

                                    @if($hour["active_lt"][$i] == true || $hour["active_llp"][$i] == true || $hour["active_al"][$i] == true || $hour["active_alp"][$i] == true)
                                        <td class="td-black btn-hour btn-students" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-option="5">{!!$hour["block"][$i]!!}</td> 
                                    @else
                                        <td class="td-black">{!!$hour["block"][$i]!!}</td> 
                                    @endif

                                    @if($hour["active_lt"][$i] == false) 
                                        <td class="oh-block t_button" >-</td>
                                    @else
                                        <td class="oh t_button" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="LT" data-aux="Laborato Terminal">
                                            <button type="button" class="btn-students btn btn-success fa fa-plus" value="1"></button>
                                            <button type="button" class="btn-students btn btn-warning fa fa-pencil" value="2"></button>
                                            <button type="button" class="btn-students btn btn-danger fa fa-trash" value="3"></button>
                                        </td>
                                    @endif  
                                    @if($hour["active_llp"][$i] == false) 
                                        <td class="oh-block t_button" >-</td>
                                    @else
                                        <td class="oh t_button" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="LL" data-aux="Laborato Laptop">
                                            <button type="button" class="btn-students btn btn-success fa fa-plus" value="1"></button>
                                            <button type="button" class="btn-students btn btn-warning fa fa-pencil" value="2"></button>
                                            <button type="button" class="btn-students btn btn-danger fa fa-trash" value="3"></button>
                                        </td>
                                    @endif  
                                    @if($hour["active_al"][$i] == false) 
                                        <td class="oh-block t_button" >-</td>
                                    @else
                                        <td class="oh t_button" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="AL" data-aux="Aula Laptop">
                                            <button type="button" class="btn-students btn btn-success fa fa-plus" value="1"></button>
                                            <button type="button" class="btn-students btn btn-warning fa fa-pencil" value="2"></button>
                                            <button type="button" class="btn-students btn btn-danger fa fa-trash" value="3"></button>
                                        </td>
                                    @endif  
                                    @if($hour["active_alp"][$i] == false) 
                                        <td class="oh-block t_button" >-</td>
                                    @else
                                        <td class="oh t_button" data-day="{{$day_id}}" data-hour="{{$hour_id}}" data-location="ALP" data-aux="Laptop Personal">
                                            <button type="button" class="btn-students btn btn-success fa fa-plus" value="1"></button>
                                            <button type="button" class="btn-students btn btn-warning fa fa-pencil" value="2"></button>
                                            <button type="button" class="btn-students btn btn-danger fa fa-trash" value="3"></button>
                                        </td>
                                    @endif  
                                     
                                </tr>
                            @endif
                        @endfor
                    </tbody>
                </table>
            </div>
        @endforeach
    </section>

    <!-- Modal -->
    <div class="modal fade" id="modal-assistance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title"></h5>
                </div>
                <div class="modal-body" id="m-body" style="overflow: overlay;">
                    <div id="mensaje"></div>
                    <table id="t-reservations" class="table table-hover text-center" cellspacing="0" style="width:100%;">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Correo</th>
                                <th>Nombre Completo</th>
                                <th data-orderable="false">Comentario</th>
                                <th data-orderable="false">Horas</th>
                                <th ></th>
                            </tr>
                        </thead>
                        <tbody id="tb-reservations">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn-manual" class="btn-students btn btn-info" value="4">Manual</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="infomodal" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h5 class="modal-title col-md-12" id="m-title">Información</h5>
                </div>
                <div class="modal-body">
                    <div class="row pb-3">
                        <div class="col-md-12">
                            <ol>
                                <li>Cada tabla es equivalente a un día.</li>
                                <li>Cada tabla tiene dos locaciones: Laboratorio y Aula.</li>
                                <li>La primer columna de cada tabla es la hora por bloque de OH.</li>
                                <li>Si la celda tiene un "-" es porqué no se puede subir asistencia en ese bloque.</li>
                                <li>Cada celda es un bloque de OH especifico</li>
                                <li>Cada celda tiene 3 opciones</li>
                                <li>Subir asistencia: <button type="button" class="btn btn-success fa fa-plus"></button></li>
                                <ol style="list-style: lower-alpha;">
                                    <li>Muestra una lista con los alumnos que reservaron.</li>
                                    <li>Al momento de subir la asistencia solamente debera seleccionar la cantidad de horas que estuvo el alumno.</li>
                                    <li>Dar click en el boton verde respectivo.</li>
                                </ol>
                                <li>Editar asistencia: <button type="button" class="btn btn-warning fa fa-pencil"></button></li>
                                <ol style="list-style: lower-alpha;">
                                    <li>Muestra una lista de los alumnos que ya tienen asistencia especificando la cantidad de horas que se les asigno.</li>
                                    <li>Si la cantidad de horas era incorrecta puede cambiar la cantidad.</li>
                                    <li>Dar click en el boton amarillo respectivo.</li>
                                </ol>
                                <li>Eliminar asistencia: <button type="button" class="btn btn-danger fa fa-trash"></button></li>
                                <ol style="list-style: lower-alpha;">
                                    <li>Muestra una lista de los alumnos que ya tienen asistencia.</li>
                                    <li>Si se subio la asistencia de un alumno que realmente no se presento a OH, el registro puede eliminarse.</li>
                                    <li>Dar click en el boton rojo respectivo.</li>
                                </ol>
                                <li>¿Cómo verificar la asistencia subida?</li>
                                <ol style="list-style: lower-alpha;">
                                    <li>Posicionate sobre la celda de la hora y podras apreciar el icono <button type="button" class="btn btn-secondary fa fa-eye"></button> </li>
                                    <li>Da click sobre la celda de la hora.</li>
                                    <li>Mostrará una lista de los alumnos que ya tienen asistencia.</li>
                                    <li>Se puede apreciar el codigo del staff que subio la asistencia.</li>
                                    <li>Existe con el fin de que se pueda verificar facilmente si la asistencia se subio correctamente.</li>
                                </ol>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.cargando.cargando')
    @include("layouts/validation-viewport")
@stop
@section('script')
<script src="/js/kavv/kavvdt.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="http://cdn.datatables.net/plug-ins/1.10.20/filtering/type-based/accent-neutralise.js"></script>

<script>
    var dt = null;
    var data, row;
    var week = @if(isset($week)) {{$week}} @else null @endif;
    $(document).ready(function(){
        $.get("/secure", function(res){
            $("body").css("display","block");
            var columns = [{"width":"25%"},{"width":"25%"},{"width":"20%"},{"width":"10%"},{"width":"10%"},{"width":"10%"}];
            var visible_row =  [[40, 20, 10, -1], [40, 20, 10, "Todo"]];
            dt = createdt($('#t-reservations'),{col:1,visible_row:visible_row, columns_width:columns});
            
            //Genera los indices de cada fila dinamicamente
            dt.on( 'order.dt search.dt', function () {
                dt.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();
            $("#loading").css('z-index',1060);

            if(week != null)
                $("#select-week option[value='"+week+"']").prop("selected",true);
            else
                $("#select-week option").last().prop("selected",true);

        }).fail(function() {
            location.href = "/"; 
        });
    });
    $(".btn-students").click(function(){

        
        $("#m-title").text("Agregar asistencia");
        $("#mensaje").empty();
        $("#tb-resevations").empty();
        $("#modal-assistance").modal("show");

        
        if($(this).val() != 4)
            data = $(this).parent().data();
        if($(this).val() == 1)
            $("#btn-manual").css('display','block');
        else
            $("#btn-manual").css('display','none');

        if($(this).data().option == 5)
            data = $(this).data();
        else
            data.option = $(this).val();
        
        var ruta = "/assistance_list/"
        if(week != null)
            ruta += week;
        $("#mensaje").slideUp();
        //$("#btn-sent").attr("disabled",true);
        $("#loading").css('display','block');
        return $.ajax({
            url: ruta,
            headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
            type: 'POST',
            dataType: 'json',
            data:data,
            success: function(res){
                //Si es agregar
                if((data.option == 1 || data.option == 4) && res.code == 200)
                {
                    res.data.forEach(function(student){
                        dt.row.add([
                            "",
                            student.email, 
                            student.name+" "+student.last_name,  
                            
                            "<textarea class='form-control' name='comment' class='comment' rows='3'></textarea>",
                            "<select class='form-control'><option value='2'>2</option><option value='1.5'>1.5</option><option value='1'>1</option><option value='0.5'>0.5</option></select>",
                            "<button onclick='save(this);' class='btn btn-success fa fa-check' value='" + student.id + "'></button>"
                        ]);
                    });
                }
                //Si es editar
                else if(data.option == 2 && res.code == 200)
                {
                    res.data.forEach(function(student){
                        var assistance = student.assistance;
                        var select = document.createElement('select');
                        select.append(new Option("2", "2", assistance == 2));
                        select.append(new Option("1.5", "1.5", assistance == 1.5));
                        select.append(new Option("1", "1", assistance == 1));
                        select.append(new Option("0.5", "0.5", assistance == 0.5)); 
                        var comment = student.comment;
                        if(comment == null)
                            comment = ""; 
                        dt.row.add([
                            "",
                            student.email, 
                            student.name+" "+student.last_name,  
                            
                            "<textarea class='form-control' name='comment' class='comment' rows='3'>"+comment+"</textarea>",
                            "<select class='form-control'>" + select.innerHTML + "</select>",
                            "<button onclick='save(this);' class='btn btn-success fa fa-check' value='" + student.id + "'></button>"
                        ]);
                    });
                }   
                //Si es eliminar
                else if(data.option == 3 && res.code == 200)
                {
                    
                    res.data.forEach(function(student){
                        var assistance = student.assistance;
                        var select = document.createElement('select');
                        select.append(new Option("2", "2", assistance == 2));
                        select.append(new Option("1.5", "1.5", assistance == 1.5));
                        select.append(new Option("1", "1", assistance == 1));
                        select.append(new Option("0.5", "0.5", assistance == 0.5)); 
                        var comment = student.comment;
                        if(comment == null)
                            comment = ""; 
                        dt.row.add([
                            "",
                            student.email, 
                            student.name+" "+student.last_name,  
                            
                            "<textarea class='form-control' name='comment' class='comment' rows='3'>"+comment+"</textarea>",
                            "<select class='form-control'>" + select.innerHTML + "</select>",
                            "<button onclick='save(this);' class='btn btn-danger fa fa-trash' value='" + student.id + "'></button>"
                        ]);
                    });
                }   
                //Si es ver todas las asistencias subidas
                else if(data.option == 5 && res.code == 200)
                {
                    $("#m-title").append("<span> (Resultados: "+res.data.length+")</span>");
                    res.data.forEach(function(student){
                        var comment = student.comment;
                        if(comment == null)
                            comment = ""; 
                        dt.row.add([
                            "",
                            student.email, 
                            student.name+" "+student.last_name,  
                            
                            "<div class='style-remove' style='width:200px;'><label>"+comment+"</label></div>",
                            "<div class='style-remove' style='width:50px;'><label>"+student.assistance+"</label></div>",
                            "<strong>"+student.staff.substring(6)+"</strong>",
                        ]);
                    });
                }
                
                dt.draw();
                $(".style-remove").removeAttr("style");
                $("#loading").css('display','none');
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            message(jqXHR,{tipo:"danger"});
            $("#mensaje").slideDown();
            $("#loading").css('display','none');
            //$("#btn-sent").attr("disabled",false);
            //console.log("erro");
        });

    });
    
    
    $(".btn_edit").click(function(){
        $("#m-title").text("Editar asistencia");
        $("#mensaje").empty();
        $("#modal-assistance").modal("show");
        console.log("edit");
    });

    $(".btn_remove").click(function(){

        $("#m-title").text("Eliminar asistencia");
        $("#mensaje").empty();
        $("#modal-assistance").modal("show");
        console.log("plus");
    });

    $("#modal-assistance").on('hide.bs.modal', function(){
        dt.clear().draw();
    });

    function save(element)
    {
        $("#mensaje").slideUp();
        $("#btn-sent").attr("disabled",true);
        $(element).attr("disabled",true);
        $("#loading").css('display','block');
        prueba = $(element);
        data.student = $(element).val();
        var quantity = $(element).parents("tr").first().find("select").val();
        //Seleccionamos la fila del alumno al cual se le agregara la asistencia
        row = $(element).parents('tr');
        //agregamos el comentario
        data.comment = row.find("textarea").val();
        
        var ruta = "/assistance/";
        if(week != null)
            ruta += week;

        if(quantity == 0.5 || quantity == 1 || quantity == 1.5 || quantity == 2)
        {
            data.quantity = $(element).parents("tr").first().find("select").val();
            return $.ajax({
                url: ruta,
                headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                type: 'POST',
                dataType: 'json',
                data: data,
                success: function(res){
                    if(res.code === 200)
                    {
                        message(res.message,{manual:true,tipo:"success"});
                        if(data.option != 2)
                            dt.row(row).remove().draw( false );
                    }
                    else
                    {
                        message(res.message,{manual:true,tipo:"danger"});
                    }
                    $("#mensaje").slideDown();
                    $(element).attr("disabled",false);
                    $("#loading").css('display','none');
                }
            }).fail( function( jqXHR, textStatus, errorThrown ) {
                message(jqXHR,{tipo:"danger"});
                $("#mensaje").slideDown();
                $(element).attr("disabled",false);
                $("#loading").css('display','none');
                //console.log("erro");
            });
        }
        else
        {
            message(["¡La cantidad de horas no es valida!"],{manual:true,tipo:"danger"});
            $("#mensaje").slideDown();
        }
    }
    $("#btn-manual").click(function(){
        dt.clear().draw();
        $(this).css('display','none');
    });

    $("#search-week").click(function(){
        //desactivamos la propiedad selected del option que es seleccionado por default
        $("#select-week option[selected]").prop("selected",false);
        //Obtenemos la semana
        var route = "/week/show/"+$("#select-week").val(); 
        $("#loading").css('display','block');
        $.get(route, function(res){
            //Actualizamos
            if(res.code == 200)
            {
                $("#title-week").text(res.name.substring(1));
                $("#date-start").text("Desde: "+res.start);
                $("#date-close").text("Hasta: "+res.close);
                week = $("#select-week").val();
            }
            $("#loading").css('display','none');
        });
    });
</script>
@stop

