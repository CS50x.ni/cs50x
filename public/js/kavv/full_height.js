
let aux_height =null;
function full_height({reponsive=900, $body =  $("body"), $section}={}){

    //Altura da la ventana menos la altura del body para saber la diferencia
    let space = window.innerHeight-$body.height();
    //Verificamos si el alto anterior y el alto actual del body sigue siendo igual
    if($body.height() != aux_height)
    {
        //Si hay diferencia entonces
        if(space > 0)
        {
            //A la seccion principal se le sumara esta diferencia(la posee signo positivo) para que siempre sea full height
            $section.height($section.height()+space);
        }
        else
        {
            $section.removeAttr("style");
            //A la seccion principal se le sumara esta diferencia para que siempre sea full height
            $section.height($body.height());
        }

    }
    else
    {
        //Si el ancho de la pantalla es inferior a 770px entonces 
        if(window.innerWidth < reponsive)
        {
            //Se remueve el atributo style que unicamente posee un alto especifico, haciendo que el alto se ajuste segun el contenido del elemento
            $section.removeAttr("style");
            //Pero es necesario tener un alto de referencia por ello le asignamos el alto actual que obtubo automaticamente al remover el attr style
            $section.height($section.height());
        }
        else
        {
            //A la seccion principal se le sumara esta diferencia(la posee signo negativo) para que siempre sea full height
            $section.height($section.height()+space);
        }
    }
    aux_height = $body.height();
}