<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\RedirectResponse;

route::get("/pset0", function(){return view("psets.pset0");})->name("pset0-y21c1");

Route::middleware(["auth"])->group(function(){

    route::get("/secure", function(){})->name("secure");

    //Rutas para los estudiantes
    route::get("/reservation", "ReservationController@index")->name("reservation.index")
    //->middleware('can:reservation.index');
    ->middleware('can:admin');
    //Retorna la vista para reservar OH
    route::get("reservation/add", "ReservationController@create")->name("reservation.create")
    //->middleware('can:reservation.create');
    ->middleware('can:admin');
    //Guarda la reservacion del estudiante
    route::post("/reservation", "ReservationController@store")->name("reservation.store")
    ->middleware('can:reservation.create');
    //Elimina la reservación realizada por el estudiante
    route::post("/reservation/remove", "ReservationController@remove")->name("reservation.remove")
    ->middleware('can:reservation.index');
    //Muesta las Reservaciones/OH realizadas por el estudiante (vista publica, no tiene rol asignado)
    Route::get('reservation/report','ReservationController@reporte')->name('reservation.reporte');
    //Muestra las asistencias/inasistencias a clase del estudiante logeado
    Route::get('class/report','ClassController@reporte')->name('class.reporte')
    ->middleware('can:reservation.index');


    //Rutas para el staff
    //Ver todas las reservas realizadas por los estudiantes
    route::get("/assistance/{valor?}", "StaffController@index")->name("assistance.index")
    ->middleware('can:assistance.index');
    //Mostrar clase por clase
    route::get("/oh_list/{valor?}", "OhController@oh_list")->name("assistance.oh_list")
    ->middleware('can:admin');
    //Permite al staff subir, editar, eliminar asistencia de OH
    route::post("/assistance/{valor?}", "StaffController@store")->name("assistance.store")
    ->middleware('can:assistance.create');
    //Lista todas las asistencias de OH subida por el staff en un bloque de hora, ignorando la locación
    route::post("/assistance_list/{valor?}", "StaffController@list")->name("assistance.list")
    ->middleware('can:assistance.index'); 



    //Usuario
    //Retorna la vista para cambiar el pass
    route::get("usuario", "UserController@index")->name("usuario.index");
    //Metodo que hace el cambio de la contraseña
    route::post("usuario/changepassword", "UserController@changepassword")->name("usuario.changepassword");
    
    // Configuración de la cuenta
    route::get("configuration", "UserController@configuration")->name("usuario.config")
    ->middleware('can:admin');
    // Cambiar ciclo
    route::post("usuario/changecycle", "UserController@changecycle")->name("usuario.changecycle")
    ->middleware('can:admin');



    
    //Rutas para Academic
    
    //Mostrar todas las clases
    //DESUSO
    /* route::get("/class", "ClassController@index")->name("class.index")
    ->middleware('can:assistance_class.index'); */

    //Mostrar clase por clase
    route::get("/lesson/{valor?}", "ClassController@lesson")->name("class.lesson")
    ->middleware('can:assistance_class.index');
    //Mostrar la vista para agregar asistencias de clases
    route::get("/class/create", "ClassController@create")->name("class.create")
    ->middleware('can:assistance_class.create');
    //Guardar la asistencia de clase
    route::post("/class", "ClassController@store")->name("class.store")
    ->middleware('can:assistance_class.create');


    //Rutas del Super Admin
    //Muestra todos los alumnos

    // Se le dio permiso al staff academic para facilitarles el seguimiento (ORIGINALMENTE SOLO ADMIN DEBE TENER ACCESO)
    route::get("admin/students","AdminController@students")->name("admin.students")
    ->middleware('can:assistance_class.create'); 
    //Muestra todas las reservaciones/OH de un alumno en especifico
    route::get("admin/students_oh/{valor}","AdminController@ohs_student")->name("admin.ohs_student")
    ->middleware('can:assistance_class.create');
    //Muestra todas las assistencias a clase de un alumno en especifico
    route::get("admin/students_class/{valor}","AdminController@class_student")->name("admin.class_student")
    ->middleware('can:assistance_class.create');

    //Muestra todas las OH (No terminadas)
    route::get("admin/all_ohs/","AdminController@ohs")->name("admin.all_ohs")
    ->middleware('can:admin');
    //Muestra todas las clases (No terminadas)
    route::get("admin/all_class/","AdminController@lessons")->name("admin.all_ohs")
    ->middleware('can:admin');
    //Sumary
    route::get("admin/sumary/","AdminController@sumary")->name("admin.sumary")
    ->middleware('can:admin');
    route::get("admin/show-sumary/","AdminController@sumary")->name("admin.sumary")
    ->middleware('can:sumary.show');

    route::get("week/show/{valor}","WeekController@show")->name("week.show")
    ->middleware('can:assistance.index');

    // Se le dio permiso al staff academic para facilitarles el seguimiento (ORIGINALMENTE SOLO ADMIN DEBE TENER ACCESO)
    //Obtener los datos de un alumno en especifico
    route::get("admin/student/{valor}","StudentController@show")->name("student.show")
    ->middleware("can:assistance_class.create");
    //Actualizar los datos de un alumno en especifico
    route::put("admin/student_update/{valor}","StudentController@update")->name("student.update")
    ->middleware("can:admin");
    //Reestablecer contraseña del alumno
    route::put("admin/password_update/{valor}","StudentController@password_update")->name("student.password_update")
    ->middleware("can:admin");
    //Eliminar registro
    route::delete("admin/student_delete/{valor}","StudentController@destroy")->name("student.student_delete")
    ->middleware("can:admin");


    route::get("student_role", "QueryController@student_role")->name("student_prueba")
    ->middleware("can:admin");

    // WEB50 SOLO EL STAFF PUEDE VERLO
    route::get("/project0", function(){return view("web50_project.project0");})->name("project0-y21c1")
    ->middleware('can:assistance.index');



});




route::get("/","LoginController@index")->name("index");
route::post("/login","LoginController@login");
route::get("/logout","LoginController@logout");


















//route::get("/makeuser","UserController@makeuser");
/* route::get("/oh-migration","ReservationController@migration")
->middleware('can:admin'); */

/* route::get("/class-migration","ClassController@migration")
->middleware('can:admin'); */


/* route::get("/comment_class","QueryController@comments_class")
->middleware('can:admin');
route::get("/update_comments_class","QueryController@update_comments_class")
->middleware('can:admin');

route::get("/comment_oh","QueryController@comments_oh")
->middleware('can:admin');
route::get("/update_comments_oh","QueryController@update_comments_oh")
->middleware('can:admin'); */



/* route::get("admin/ohs_duplicated","AdminController@ohs_duplicated")->name("admin.ohs_duplicated")
->middleware('can:admin'); */

//route::get("/inicio",function(){return view("layouts.cargando.inicio");});

//Route::get('/home', 'HomeController@index')->name('home');
