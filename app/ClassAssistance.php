<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassAssistance extends Model
{
    use SoftDeletes;
    protected $table='class';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
       'id','name',
    ];
    public function Cycles()
    {
        return $this->belongsTo('App\Cycle', 'cycle_id');
    }
    public function Students()
    {
        return $this->belongsToMany('App\Student', 'class_student', 'class_id', 'student_id')->withTimestamps()->WherePivot('deleted_at',null);;
    }
}
