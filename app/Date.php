<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Date extends Model
{
    use SoftDeletes;
    protected $table='date';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
       'id','code', 'email', 'name', 'last_name', 'birthdate', 'address', 'phone', 'level', 'career', 'educarional_center', 'shirt_size', 'state', 'coming', 'user_id'
    ];

    public function Weeks()
    {
        return $this->belongsToMany('App\Week', 'date_week', 'date_id', 'week_id');
    }
}
