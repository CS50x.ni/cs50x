<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Week extends Model
{
    use SoftDeletes;
    protected $table='week';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
       'id','code', 'email', 'name', 'last_name', 'birthdate', 'address', 'phone', 'level', 'career', 'educarional_center', 'shirt_size', 'state', 'coming', 'user_id'
    ];
    
    public function Days()
    {
        return $this->belongsToMany('App\Day', 'day_week', 'week_id', 'day_id');
    }
    
    public function Dates()
    {
        return $this->belongsToMany('App\Date', 'date_week', 'week_id', 'date_id');
    }
    public function OHs()
    {
        return $this->hasMany('App\Oh', 'week_id', 'id');
    }
}
