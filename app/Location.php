<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    
    use SoftDeletes;

    protected $table='location';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
       'id','code', 'email', 'name', 'last_name', 'birthdate', 'address', 'phone', 'level', 'career', 'educarional_center', 'shirt_size', 'state', 'coming', 'user_id'
    ];
    
    public function OHs()
    {
        return $this->hasMany('App\Oh', 'location_id', 'id');
    }
}
