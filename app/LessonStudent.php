<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LessonStudent extends Model
{
    use SoftDeletes;
    protected $table='class_student';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
       'id','class_id', "student_id", "justified", "comment", "class_group", "staff_id", "quantity", 
    ];
    
}
