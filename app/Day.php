<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Day extends Model
{
    use SoftDeletes;
    protected $table='day';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
       'id','code', 'email', 'name', 'last_name', 'birthdate', 'address', 'phone', 'level', 'career', 'educarional_center', 'shirt_size', 'state', 'coming', 'user_id'
    ];

    public function Hours()
    {
        return $this->belongsToMany('App\Hour', 'days_hours', 'day_id', 'hour_id');
    }
    
    public function Weeks()
    {
        return $this->belongsToMany('App\Week', 'day_week', 'day_id', 'week_id');
    }
}
