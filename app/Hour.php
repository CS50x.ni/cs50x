<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Hour extends Model
{
    use SoftDeletes;
    protected $table='hour';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
       'id','code', 'email', 'name', 'last_name', 'birthdate', 'address', 'phone', 'level', 'career', 'educarional_center', 'shirt_size', 'state', 'coming', 'user_id'
    ];
    
    public function Days()
    {
        return $this->belongsToMany('App\Day', 'days_hours', 'hour_id', 'day_id');
    }
}
