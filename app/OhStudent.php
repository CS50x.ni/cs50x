<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OhStudent extends Model
{
    protected $table='oh_student';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'oh_id', 'student_id', 'assistance', 'staff_id','reserved', 'comment', 'comment_id',
    ];
}
