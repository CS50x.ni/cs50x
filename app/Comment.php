<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;
    protected $table='comment';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        "description", 
    ];
}
