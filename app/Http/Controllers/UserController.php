<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Student;
use App\Cycle;
use Auth;
use Redirect;
use Caffeinated\Shinobi\Models\Role;


class UserController extends Controller
{
    public function index()
    {
        //return view('assistance/index');
        return view('user.index');        
    }
    
    public function changepassword(Request $data)
    {
        $user = Auth::User();
        if (Hash::check($data['password-old'], $user->password))
        {
            if($data['password-new'] == $data['password-ok'])
            {
                $user->password = Hash::make($data['password-new']);
                $user->save();
                return response()->json([
                    'code' => 200,
                    'message' => ['Se actualizo su contraseña correctamente.']
                ]);
            }
            else
            {
                return response()->json([
                    'code' => 0,
                    'message' => ['La contraseña y la confirmación no es la misma.']
                ]);
            }
        }
        else
        {
            return response()->json([
                'code' => 1,
                'message' => ['La contraseña actual no es correcta.']
            ]);
        }
    }

    public function makeuser()
    {
        /* Student::create([
            'name' => "Silvio",
            'last_name' => "Duarte",
            'code' => "Silfdv",
            'email' => "sduarte@code-fu.net.ni",
            'password' => Hash::make('123456'),
        ]); */
        /* Student::create([
            'name' => "Dewin",
            'last_name' => "Umaña",
            'code' => "tester0",
            'email' => "dumana@code-fu.net.ni",
            'password' => Hash::make('123456'),
        ]);
        Student::create([
            'name' => "Franklin",
            'last_name' => "Laínez",
            'code' => "tester1",
            'email' => "flainez@code-fu.net.ni",
            'password' => Hash::make('abcdef'),
        ]);
        Student::create([
            'name' => "Gustavo",
            'last_name' => "Chavarria",
            'code' => "tester2",
            'email' => "gchavarria@code-fu.net.ni",
            'password' => Hash::make('123abc'),
        ]);
        Student::create([
            'name' => "Danny",
            'last_name' => "Garcia",
            'code' => "tester3",
            'email' => "dgarcia@code-fu.net.ni",
            'password' => Hash::make('abc123'),
        ]);
        Student::create([
            'name' => "Margarita",
            'last_name' => "Valladares",
            'code' => "tester4",
            'email' => "mvalladares@code-fu.net.ni",
            'password' => Hash::make('654321'),
        ]);
        Student::create([
            'name' => "Axel",
            'last_name' => "Garcia",
            'code' => "tester5",
            'email' => "agarcia@code-fu.net.ni",
            'password' => Hash::make('010101'),
        ]);
        Student::create([
            'name' => "Camilo",
            'last_name' => "Treminio",
            'code' => "tester6",
            'email' => "ctreminio@code-fu.net.ni",
            'password' => Hash::make('111111'),
        ]); */
        /* Student::create([
            'name' => "Juan",
            'last_name' => "Ramon",
            'code' => "tester7",
            'email' => "jromero@code-fu.net.ni",
            'password' => Hash::make('kira123'),
        ]); */
        /* Student::create([
            'name' => "Maxwel",
            'last_name' => "Hernandez",
            'code' => "tester8",
            'email' => "mhernandez@code-fu.net.ni",
            'password' => Hash::make('000000'),
        ]); 
        Student::create([
            'name' => "Fabiola",
            'last_name' => "Ceron",
            'code' => "tester9",
            'email' => "fceron@code-fu.net.ni",
            'password' => Hash::make('ffffff'),
        ]); 
        Student::create([
            'name' => "Jeffrey",
            'last_name' => "Somarriba",
            'code' => "tester10",
            'email' => "jsomarriba@code-fu.net.ni",
            'password' => Hash::make('cccccc'),
        ]);  */


        $data = [
            [
                'aortiz@code-fu.net.ni',
                'amora@code-fu.net.ni',
                'adiaz@code-fu.net.ni',
                'acontreras@code-fu.net.ni',
                'areyes@code-fu.net.ni',
                'alarios@code-fu.net.ni',
                'asanchez@code-fu.net.ni',
                'anmorales@code-fu.net.ni',
                'arodriguez@code-fu.net.ni',
                'borozco@code-fu.net.ni',
                'carcia@code-fu.net.ni',
                'cgarcia@code-fu.net.ni',
                'dbojorquez@code-fu.net.ni',
                'dmorgan@code-fu.net.ni',
                'emorales@code-fu.net.ni',
                'emayorga@code-fu.net.ni',
                'emendoza@code-fu.net.ni',
                'fguzman@code-fu.net.ni',
                'glopez@code-fu.net.ni',
                'hcastro@code-fu.net.ni',
                'hespinoza@code-fu.net.ni',
                'hsaavedra@code-fu.net.ni',
                'hhuete@code-fu.net.ni',
                'ichavez@code-fu.net.ni',
                'jvaldivia@code-fu.net.ni',
                'jemejia@code-fu.net.ni',
                'jmejia@code-fu.net.ni',
                'jmojica@code-fu.net.ni',
                'kmolina@code-fu.net.ni',
                'kdesbas@code-fu.net.ni',
                'lescobar@code-fu.net.ni',
                'lmarcia@code-fu.net.ni',
                'mgoussen@code-fu.net.ni',
                'mhurtado@code-fu.net.ni',
                'mreyes@code-fu.net.ni',
                'naguilar@code-fu.net.ni',
                'orivera@code-fu.net.ni',
                'rgalan@code-fu.net.ni',
                'rchavez@code-fu.net.ni',
                'rgutierrez@code-fu.net.ni',
                'smena@code-fu.net.ni',
                'sramos@code-fu.net.ni',
                'scalvo@code-fu.net.ni',
                'usoza@code-fu.net.ni',
                'vruiz@code-fu.net.ni'
            ], 
            [
                'Adriel Abraham',
                'Ally Andrea',
                'Alvar Fernando ',
                'Alvaro Francisco',
                'Anderson David',
                'Angel Gerardo',
                'Angelo Snayder',
                'Anneke Paulina ',
                'Arnaldo José ',
                'Bryan Miguel ',
                'Carlos Fernando ',
                'Cristian Alberto',
                'Danny Alexander ',
                'Diana Julissa',
                'Edgard Jesús',
                'Eduardo Augusto',
                'Ernesto de Jesús ',
                'Felipe Genaro',
                'Geovania Madeley',
                'Hans Mijail',
                'Harold Benito',
                'Helmut Antonio',
                'Hugo Alberto',
                'Israel Cajina',
                'Jader Antonio',
                'Jeffry Antonio ',
                'Jerry Francisco ',
                'Joshua Alexander',
                'Karel Eduardo',
                'Katherine Valeska',
                'Lesly Mariela',
                'Luisángel Martín',
                'Mario Ernesto',
                'Meloddy Nanya ',
                'Moisés Jesús',
                'Noel Alejandro ',
                'Octavio Noé ',
                'Reyna Mercedes',
                'Ricardo René',
                'Roberto Ulises',
                'Sarahí Del Mar ',
                'Silvana Nayareth ',
                'Susan Guadalupe ',
                'Ulises Javier',
                'Víctor José'
            ],
            [
                'Ortiz Garcia',
                'Mora Valle',
                'Díaz Aragón',
                'Contreras Arriola',
                'Reyes Izaguirre',
                'Larios Valerio',
                'Sánchez Muñoz',
                'Morales López',
                'Rodríguez López',
                'Orozco Osorio',
                'Arcia Castro',
                'Garcia Cruz',
                'Bojorquez Davila',
                'Morgan Hernández',
                'Morales Vanegas',
                'Mayorga Téllez',
                'Mendoza Vallecillo',
                'Guzmán Vilchez',
                'Lopez Alfaro',
                'Castro Soza',
                'Espinoza Trujillo',
                'Saavedra García',
                'Huete Arróliga',
                'Chávez',
                'Valdivia Taleno',
                'Mejia Ocampos',
                'Mejia Roa',
                'Mojica Aguirre',
                'Molina Zuniga',
                'Desbas Miranda',
                'Escobar Garcia',
                'Marcia Palma',
                'Goussen Sequeira',
                'Hurtado Rodriguez ',
                'Reyes Ramirez',
                'Aguilera Cano',
                'Rivera Aguirre',
                'Galán Contreras',
                'Chávez Ríos',
                'Gutierrez Lacayo',
                'Mena Salazar',
                'Ramos Diaz',
                'Calvo Chamorro',
                'Soza Soza',
                'Ruíz Herrera'
            ],
            [
                'aortiz',
                'amora',
                'adiaz',
                'acontreras',
                'areyes',
                'alarios',
                'asanchez',
                'anmorales',
                'arodriguez',
                'borozco',
                'carcia',
                'cgarcia',
                'dbojorquez',
                'dmorgan',
                'emorales',
                'emayorga',
                'emendoza',
                'fguzman',
                'glopez',
                'hcastro',
                'hespinoza',
                'hsaavedra',
                'hhuete',
                'ichavez',
                'jvaldivia',
                'jemejia',
                'jmejia',
                'jmojica',
                'kmolina',
                'kdesbas',
                'lescobar',
                'lmarcia',
                'mgoussen',
                'mhurtado',
                'mreyes',
                'naguilar',
                'orivera',
                'rgalan',
                'rchavez',
                'rgutierrez',
                'smena',
                'sramos',
                'scalvo',
                'usoza',
                'vruiz',
            ]
        ];
        
        //Creamos el usuaario y al mismo tiempo asignamos el rol de staff
        /* for($i = 0; $i<count($data[0]); $i++)
        {
            $staff = Student::create([
                'name' => $data[1][$i],
                'last_name' => $data[2][$i],
                'code' => "Staff_".$data[3][$i],
                'email' => $data[0][$i],
                'password' => Hash::make('StaffCS50X.ni'),
            ]);  
            
            $staff->assignRoles('y20c1staff');
        }

        return "Exito"; */
        
        
        /* $student = Student::create([
            'name' => "CS50's Student",
            'last_name' => '',
            'code' => "CS50_Student",
            'email' => "CS50_Student@code-fu.net.ni",
            'password' => Hash::make('StudentCS50X.ni'),
        ]);   
        $student->assignRoles('y20c1student');*/

        //Asiganamos los permisos de los ciclos a los que ha pertenecido cada estudiante
        /* $aux = 0;
        $cycles = Cycle::all();
        $role = null;
        foreach($cycles as $cycle)
        {
            $role = strtolower($cycle->name)."student";
            $students = $cycle->Students()->get();
            if($students)
            {
                foreach($students as $student)
                {
                    $student->assignRoles($role);
                }
            }
        }
        
        return "Plis"; */
        /* echo "<table><thead><th></th><th></th><th></th></thead><tbody>";
        $students = Role::where('slug','y20c1student')->first()->users()->select('student.id','email','name','last_name')->get();
        foreach($students as $student)
        { */
            /* if($student->password == null)
            {
                $student->password = Hash::make("123456");
                $student->save();

            } */
            /* echo "<tr><td>".$student->name."</td><td>".$student->last_name."</td><td>".$student->email."</td></tr>";
        }
        echo "</tbody></table>"; */

    }


    public function configuration()
    {
        $cycles = Cycle::select("id", "name")->where("id", ">", "19")->orderBy("id", "desc")->get();
        return view("user.configuration", compact("cycles"));
    }

    public function changecycle(Request $data)
    {
        // Buscamos el cyclo seleccionado
        $cycle = Cycle::find($data->cycle);
        // Existe?
        if($cycle)
        {
            // El id es mayor a 19 lo cual indica que es >= Y20C1
            if($cycle->id > 19)
            {
                // Actualizamos la variable de sesión
                session(["cycle" => $cycle->id]);
                if($cycle->id == 20)
                    session(["student_role"=>"y20c1student"]);
                if($cycle->id == 22)
                    session(["student_role"=>"y21c1student"]);
                    
                return response()->json([
                    'code' => 200,
                    'message' => ['Cambio de ciclo exitoso']
                ]);
            }
            else
            {
                return response()->json([
                    'code' => 1,
                    'message' => ['El cyclo no es valido.']
                ]);
            }
        }
        else
        {
            return response()->json([
                'code' => 1,
                'message' => ['El cyclo no es valido.']
            ]);
        }
    } 
}
