<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Student;
use App\Cycle;
use Auth;
use Redirect;

class LoginController extends Controller
{
    public function index()
    {
        
        $user = Auth::user();
        $logo = "";
        if($user)
        {
            $cycle = Cycle::select("id")->where("active","=", "1")->first();
            session(["cycle" => $cycle['id']]);
            session(["student_role" => "y21c1student"]);
            return view('inicio');
        }
        if (!Auth::check()) 
            return view('login.index');
        
    }
    public function login(Request $request)
    {
        if($request['email'] && $request['password'])
        {
           
            //return $request;
            //Buscamos al usuario con email
            $user = Student::where('email','like', $request['email'])->first();
            //return $user;
            //si existe entonces ...
            if($user)
            {
                //Verificamos si la contraseña es valida
                if (Hash::check($request['password'], $user->password))
                {
                    Auth::login($user);
                    return response()->json([
                        'code' => 1,
                    ]);
                }
                else
                {
                    return response()->json([
                        'code' => 2,
                        'message' => ['El correo o contraseña no coinciden'],
                    ]);
                }
            } 
            else
            {
                return response()->json([
                    'code' => 2,
                    'message' => ['El correo o contraseña no coinciden'],
                ]);
            }
        }
        else
        {
            return response()->json([
                'code' => 0,
                'message' => '¡El campo es requerido!',
            ]);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
