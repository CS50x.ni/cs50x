<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Auth;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{

    public function update(Request $data, $id)
    {
        $student = Student::find($id);
        
        if($student)
        {
            $student->code = $data["code"];
            $student->email = $data["email"];
            $student->name = $data["name"];
            $student->last_name = $data["last_name"];
            $student->gender = $data["gender"];
            $student->birthdate = $data["birthdate"];
            $student->address = $data["address"];
            $student->phone = $data["phone"];
            $student->level = $data["level"];
            $student->career = $data["career"];
            $student->educational_center = $data["educational_center"];
            $student->shirt_size = $data["shirt_size"];
            $student->state = $data["state"];
            $student->coming = $data["coming"];
            
            $student->save();
            return response()->json([
                "code" => 200,
                "message" => ["¡Registro actualizado correctamente!"]
            ]);
        }
        else
        {
            return response()->json([
                "code" => 500,
                "message" => ["No se edito el registro, no se encontro al estudiante"]
            ]);
        }
        
    }

    public function password_update($id)
    {
        $student = Student::find($id);
        if($student)
        {
            //Sobreescribimos la contraseña del alumno por 123456
            $student->password = Hash::make('123456');
            $student->save();
            return response()->json([
                "code" => 200,
                "message" => ["¡Contraseña reestablecida!"]
            ]);
        }
        else
        {
            return response()->json([
                "code" => 500,
                "message" => ["No se cambio la contraseña, no se encontro al estudiante"]
            ]);
        }
    }

    public function show($id)
    {
        $student = Student::Select("code","email","name","last_name","gender","birthdate","address","phone","level","career","educational_center","shirt_size","state","coming","created_at")->find($id);
        if($student)
            return response()->json([
                'code' => 200,
                "data" => $student,
            ]);
        else
            return response()->json([
                'code' => 500,
                'message' => ["Ocurrio un error... no se encontro al estudiante"]
            ]);
    }

    public function destroy($id)
    {
        $student = Student::find($id);
        if($student)
        {
            $student->delete();
            return response()->json([
                'code' => 200,
                "message" => ["¡Registro eliminado!"]
            ]);
        }
        else
            return response()->json([
                'code' => 500,
                'message' => ["Ocurrio un error... no se encontro el registro"]
            ]);
    }
}
