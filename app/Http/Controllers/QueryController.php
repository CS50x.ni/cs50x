<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cycle;
use App\Oh;
use App\Location;
use Carbon;
use App\Student;
use App\Date;
use App\Day;
use App\Hour;
use App\Week;
use App\ClassAssistance;
use App\LessonStudent;
use App\OhStudent;
use App\Comment;
use Caffeinated\Shinobi\Models\Role;
use Auth;

class QueryController extends Controller
{
    public function comments_class()
    {
        $cycle = Cycle::find(session("cycle"));
        $lessons = $cycle->Lessons()->get();
        $assistances = [];
        foreach($lessons as $key => $class)
            $assistances[$key] = $class->Students()->select("name","comment")->withPivot("id")->wherePivot("comment","!=","")->get();
            
        echo "comments = [<br/>";
        foreach($assistances as $key => $class)
        {
            foreach($class as $key => $data)
            {
                $new_comment = Comment::create([
                    'description' => $data->comment,
                ]); 
                $assistencia = LessonStudent::find($data->pivot->id);
                $assistencia->comment_id = $new_comment->id;
                $assistencia->save();
            }
        }
        return "perfecto!";
    }

    public function update_comments_class()
    {
        $comments = [
            ['Llegada 15 mins tarde', 6140],
            ['Llegada 15 mins tarde', 6141],
            ['15 mins tarde', 6143],
            ['Llegada 15 mins tarde', 6144],
            ['Llegada 15 mins tarde', 6145],
            ['Llegada 1 hora tarde', 6146],
            ['Vino 40 minutos tarde', 6168],
            ['Vino 50 minutos tarde', 6169],
            ['Orlin Josue Saravia flores', 6267],
            ['Justificó por correo', 6304],
            ['Justificó por correo', 6305],
            ['Justificó por correo', 6306],
            ['Justificó por correo', 6307],
            ['Se justificó por correo y se presentó presencialmente para notificarlo.', 6437],
            ['Se justificó por correo', 6438],
            ['hhhh', 8957],
            ['se justificó por correo.', 6337],
            ['asd', 6365],
            ['Se justificó por correo', 6439],
            ['Se retira a las 08:44 AM por motivos personales no especificados', 6460],
            ['Llegada 15 mins tarde', 6475],
            ['Llegada 15 mins tarde', 6476],
            ['15 mins tarde', 6478],
            ['Llegada 15 mins tarde', 6479],
            ['Llegada 15 mins tarde', 6480],
            ['Llegada 1 hora tarde', 6481],
            ['Vino 40 minutos tarde', 6503],
            ['Vino 50 minutos tarde', 6504],
            ['Orlin Josue Saravia flores', 6602],
            ['Justificó por correo', 6639],
            ['Justificó por correo', 6640],
            ['Justificó por correo', 6641],
            ['Justificó por correo', 6642],
            ['e equivoco de grupo, se quedo en el grupo A, pero se le indico que en la proxima clase tiene que ir a su grupo', 6658],
            ['Kevin autorizo que estuviera en el grupo A', 6682],
            ['Recibió clases en grupo E', 6781],
            ['Recibió clases en grupo E', 6782],
            ['Recibió clase en Grupo E', 6783],
            ['repuso en horario 4-6 pm', 6802],
            ['Justificado por coordinación', 7364],
            ['Se fue temprano, justificara luego', 6876],
            ['Recibió clase en grupo D.', 6900],
            ['Recibió clase en el Grupo H', 6986],
            ['Recibió clase en grupo F', 7359],
            ['Recibió clase en grupo F.', 7361],
            ['Kevin dijo que lo justificara', 7362],
            ['Justificado por Kevin', 7399],
            ['Kevin dijo que lo justificara', 7363],
            ['Recibió clase en group D.', 7436],
            ['Recibió clases en grupo E', 7485],
            ['Esta persona en member tiene como segundo nombre "Contreras", cuando debería ser "Aldir"', 7694],
            ['Recibio clases en el grupo C', 7740],
            ['Kevin solicitó justificación', 8208],
            ['Kevin la mandó a justificar', 8619],
            ['dddd', 8959],
            ['Llegó 1 hora tarde', 7886],
            ['Recibió clase en grupo D.', 7894],
            ['Se presentó a clases y solo estuvo media hora, se retiró sin avisar y sin firmar.', 7913],
            ['Solo estuvo presente por una hora.', 7914],
            ['Recibió clase en Grupo E', 7951],
            ['Se justificó por correo.', 8024],
            ['Kevin lo mando a justificar.', 8025],
            ['Se justificó con una carta.', 8574],
            ['Kevin la mandó a justificar', 8620],
            ['Recibieron clase en el grupo A', 8154],
            ['Recibio clases en el grupo A', 8155],
            ['Esta viniendo a las 8:50 (Normalmente viene tarde)', 8176],
            ['Kevin solicitó justificación ', 8209],
            ['Clase realizada en Aula. Usó: Laptop 01', 8487],
            ['Clase realizada en Aula. Usó: Laptop 07', 8488],
            ['Clase realizada en Aula. Usó: Laptop 02', 8489],
            ['Clase realizada en Aula. Usó: Laptop 02', 8490],
            ['Clase realizada en Aula. Usó: Laptop 08', 8491],
            ['Clase realizada en Aula. Usó: Laptop 10', 8492],
            ['Clase realizada en Aula. Usó: Laptop 10', 8493],
            ['Clase realizada en Aula. Usó: Laptop 15', 8494],
            ['Clase realizada en Aula. Usó: Laptop 06', 8495],
            ['Clase se realizó en Aula. Usó: Laptop 06', 8496],
            ['Clase se realizó en Aula. Usó: Laptop 04', 8497],
            ['Clase se realizó en Aula. Usó: Laptop 04', 8498],
            ['Clase se realizó en Aula. Usó: Laptop 12', 8499],
            ['Clase se realizó en Aula. Usó: Laptop 12', 8500],
            ['Clase se realizó en Aula. Usó: Laptop 14', 8501],
            ['Clase se realizó en Aula. Usó: Laptop 14', 8502],
            ['Clase se realizó en Aula. Usó: Laptop 03', 8503],
            ['Clase se realizó en Aula. Usó: Laptop 02', 8504],
            ['Clase se realizó en Aula. Usó: Laptop 07', 8505],
            ['Clase se realizó en Aula. Usó: Laptop propia', 8506],
            ['Clase se realizó en Aula. Usó: Laptop 13', 8507],
            ['Clase se realizó en Aula. Usó: Laptop 01', 8508],
            ['Clase se realizó en Aula. Usó: ninguna', 8509],
            ['Clase se realizo en Aula. Usó: Laptop 03', 8510],
            ['Clase se realizó en Aula. Usó: ninguna', 8511],
            ['Clase se realizó en Aula. Usó: Laptop 09', 8512],
            ['Clase se realizó en Aula. Usó: Laptop 15', 8513],
            ['Clase se realizó en Aula. Usó: Laptop propia', 8514],
            ['recibio la clase en el grupo A', 8306],
            ['TARDE', 8331],
            ['Recibió clase en grupo F', 8401],
            ['repuso hora, porque no pudo venir en la mañana', 8455],
            ['Recibió clases en Grupo A', 8585],
            ['Parece haberse confundido de grupo', 8670],
            ['Se le evalúo en preguntas de control sobre PSET1 por ser recursante.', 8897],
            ['Se le evalúo preguntas de control referente a PSET1 por ser recursante.', 8898],
            ['Vino 30 minutos tarde', 8919],
        ];
        foreach($comments as $comment)
        { 
            $new_comment = Comment::create([
                'description' => $comment[0],
            ]); 
            $assistencia = LessonStudent::find($comment[1]);
            $assistencia->comment_id = $new_comment->id;
            $assistencia->save();
        }
    }

    public function comments_oh()
    {
        $cycle = Cycle::find(session("cycle"));
        $Ohs = $cycle->Ohs()->get();
        $assistances = [];
        foreach($Ohs as $key => $oh)
            $assistances[$key] = $oh->Students()->select("name","comment")->withPivot("id")->wherePivot("comment","!=","")->get();
            
        //echo "comments = [<br/>";
        foreach($assistances as $key => $oh)
        {
            foreach($oh as $key => $data)
            {
                $new_comment = Comment::create([
                    'description' => $data->comment,
                ]); 
                $data = OhStudent::find($data->pivot->id);
                $data->comment_id = $new_comment->id;
                $data->save();
            }
        }
        return "perfecto!";
        
    }

    public function student_role()
    {
        $student = Student::find(1602);
        $student->assignRoles('y21c1student');
    }
}
