<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cycle;
use App\Oh;
use App\Location;
use Carbon;
use App\Student;
use App\Day;
use App\Hour;
use App\Comment;
use App\Week;
use Auth;
use Caffeinated\Shinobi\Models\Role;

class StaffController extends Controller
{
    public function index($week = null)
    {
        
        $object = $this->get_actual_week($week);
        if($object == null)
            return "¡No se ha encontrado la semana especificada!";
            
        if(!isset($object['week']))
            return "¡No se ha encontrado la semana especificada!";

        
        $actual_week = $object["week"];
        $start_week = date("d/m/Y",$object["start"]);
        $close_week = date("d/m/Y",$object["close"]);   


        //obtenermos los días de la semana
        $days = $actual_week->Days()->get();
        
        $hours = [];
        //Recorremos cada uno de los díás para crear un objeto "$hour" con los id del día, hora, nombre del bloque, y si esta activo o no.
        foreach($days as $day)
        {
            $active_hours = $day->Hours()->withPivot("active_lt","active_llp","active_al","active_alp")->get(); 
            foreach($active_hours as $hour)
            {
                if(array_key_exists($day->name, $hours))
                {
                    array_push($hours[$day->name]["hour_id"], $hour->id);
                    array_push($hours[$day->name]["block"], $hour->name);
                    array_push($hours[$day->name]["active_lt"], $hour->pivot->active_lt);
                    array_push($hours[$day->name]["active_llp"], $hour->pivot->active_llp);
                    array_push($hours[$day->name]["active_al"], $hour->pivot->active_al);
                    array_push($hours[$day->name]["active_alp"], $hour->pivot->active_alp);
                }
                else
                {
                    $hours[$day->name]["day_id"] = $day->id;
                    $hours[$day->name]["hour_id"] = [$hour->id];
                    $hours[$day->name]["block"] = [$hour->name];
                    $hours[$day->name]["active_lt"] = [$hour->pivot->active_lt];
                    $hours[$day->name]["active_llp"] = [$hour->pivot->active_llp];
                    $hours[$day->name]["active_al"] = [$hour->pivot->active_al];
                    $hours[$day->name]["active_alp"] = [$hour->pivot->active_alp];
                }
            }
        }
        //La semana actual la utilizareos para no mostrar semanas del futuro
        $last_week = $this->get_actual_week();
        //return substr($last_week["week"]->name,1);
        $all_weeks = Week::all();
        if($week == null)
            $week = $last_week["week"]->id; 
        return view('assistance/index', compact("start_week","close_week", "hours", "week", "last_week", "all_weeks"));

    }
    public function get_actual_week($week_selected = null)
    {
        if($week_selected == null)
        {
            //Obtenemos la fecha del momento
            $today = Carbon\Carbon::now();
            //$today = "2021-01-26 00:00:00";
            //Con formato full_year-month-day
            $today = strtotime($today);
            //Obtenemos todas las fechas establecidas del ciclo y20c1
            $dates = Cycle::find(session("cycle"))->Dates()->get();
            $actual_week = null;
            $open_week = "";
            $start_week = "";
            $close_week = "";
            $week = null;
            //Recorremos todas las fechas para poder identificar en cual estamos actualmente
            foreach($dates as $date)
            {
                $week["open"] = strtotime($date->open);
                $week["close"] = strtotime($date->close);
                //Cuando la fecha de hoy se encuentra en un rango de fechas es porque se ha encontrado la semana actual del ciclo
                if(($today >= $week["open"]) && ($today <= $week["close"]))
                {
                    //Almacenamos la semana actual
                    $week["week"] = $date->Weeks()->first();
                    $week["start"] = strtotime($date->start);
                    break;
                }
            }
        }
        else
        {
            $week["week"] = Week::find($week_selected);
            if($week)
            {
                $dates = $week["week"]->Dates()->first();
                $week["start"] = strtotime($dates->start);
                $week["close"] = strtotime($dates->close);
            }
            else
                $week = null;
        }
        if($week == null)
        {
            $week["week"] = Week::all()->last();
            $dates = $week["week"]->Dates()->first();
            $week["start"] = strtotime($dates->start);
            $week["close"] = strtotime($dates->close);
        }
        return $week;
    }

    public function list(Request $data, $week = null)
    {
        if($data->option != 4 && $data->option != 5)
        {
            $object = $this->get_actual_week($week);
            if($object == null)
                return "¡No se ha encontrado la semana especificada!";

            $actual_week = $object["week"];
            $location_id = Location::where("name",$data->location)->first();
            //return $location_id;
            if($location_id)
            {
                $location_id = $location_id->id;
                $oh = Oh::Where("week_id", $actual_week->id)
                ->Where("day_id", $data->day)
                ->Where("hour_id", $data->hour)
                ->Where("location_id",$location_id)
                ->Where("cycle_id",session('cycle'))->first();
                
                $students = [];
                if($oh)
                {
                    //Si le dio click al boton agregar (1), editar(2), eliminar(3)
                    if($data->option == 1)
                        $students = $oh->Students()->Select("student.id","email","name","last_name","oh_student.assistance")->wherePivot("assistance",null)->wherePivot("reserved",true)->get();
                    else if($data->option == 2 || $data->option == 3)
                        $students = $oh->Students()->Select("student.id","email","name","last_name","oh_student.assistance","oh_student.comment_id")->wherePivot("assistance","!=",null)->get();
                    else 
                    {
                        return response()->json([
                            'message' => ['Ocurrio un error, opción incorrecta'],
                            'code' => 0
                        ]);
                    }
                    foreach($students as $key => $student)
                    {
                        if($student->comment_id != null)
                            $students[$key]["comment"] = Comment::find($student->comment_id)->description;
                        else
                            $students[$key]["comment"] = "";
                    }
                    return response()->json([
                        'data' => $students,
                        'code' => 200
                    ]);
                }
                else
                {
                    //Ningun estudiante ha reservado en ese horario o la oh no existe, es lo mismo
                    return response()->json([
                        'code' => 0,
                        'message' => ['La OH no existe, nadie reservo en ella'],
                    ]);
                }
            }
            else
            {
                //No existe entre las 4 opciones de locaciones
                return response()->json([
                    "code" => 0,
                    "message" => ["No se ha encontrado la locación seleccionada"],
                ]);
            }
        }
        else
        {
            //Si no es listar por hora y día retornara todos los estudiantes del ciclo Y20C1
            if($data->option != 5)
            {
                $role = Role::where('slug',session("student_role"))->first();
                $students = [];
                if($role)
                    $students = $role->users()->select('student.id','email','name','last_name')->get();
                return response()->json([
                    'code' => 200,
                    "data" => $students,
                ]);
            }
            //De lo contrario retornara todos los estudiantes que reservaron en ese bloque
            else
            {
                
                $object = $this->get_actual_week($week);
                $actual_week = $object["week"];
                //Retornamos todas las oh existentes segun la semana, día, hora(son max 4)
                $ohs = Oh::Where("week_id", $actual_week->id)->Where("day_id", $data->day)->Where("hour_id", $data->hour)->Where("cycle_id",session('cycle'))->get();
                $assistances = [];
                $aux = 0;
                //Si existe almenos un registro de OH
                if(count($ohs)>0)
                {
                    //Recorremos todas los registros devueltos (entre 1 a 4 registros)
                    foreach($ohs as $oh)
                    {
                        //Cada OH posee una cantidad de estudiantes relacionados
                        $students = $oh->Students()->Select("student.id","email","name","last_name","oh_student.assistance","oh_student.comment_id", "oh_student.staff_id")->wherePivot("assistance","!=",null)->get();
                        //Recorremos cada uno de los estudiantes para
                        foreach($students as $student)
                        {
                            //Almacenar los datos de cada uno en el arreglo $assistances y ademas sumarle el codigo del staff que subio su asistencia 
                            $assistances[$aux] = $student;
                            $assistances[$aux]->staff = Student::find($student->staff_id)->code;
                            if($student->comment_id != null)
                                $assistances[$aux]->comment = Comment::find($student->comment_id)->description;
                            else
                                $assistances[$aux]->comment = "";

                            
                            $aux++;
                        } 
                    }
                    return response()->json([
                        'code' => 200,
                        "data" => $assistances,
                    ]);
                }
                else
                {
                    return response()->json([
                        'code' => 0,
                        "message" => "Ningun estudiante reservo en este horario",
                    ]);
                }
            }
        }
    }
    public function comment_validation($data, $lesson)
    {
        $comment = null;

        //PROCEDIMIENTO PARA EL COMENTARIO
        //La asistencia de clase ya tenia un comentario?
        $comment_id = null;
        if(isset($lesson->pivot->comment_id ))
            $exist = $lesson->pivot->comment_id;

        if($comment_id != null)
        {
            //Buscamos el comentario
            $old_comment = Comment::find($comment_id);
            //Se ha ingresado un comntario?
            if($data->comment != "")
            {
                //El comentario original es diferente al comentario actual?
                if($old_comment->description != $data->comment)
                {
                    //Actualizamos el comentario
                    $old_comment->description = $data->comment;
                    $old_comment->save();
                }
                $comment = $old_comment->id;
            }
            else
            {
                //Al existir previamente un comentario pero actualmente se detecta el comentario como vacio
                //Significa que se ha removido el comentario, por lo que se debe eliminar el registro y la relación.
                $old_comment->delete();
            }
        }
        else
        {
            //Se ha ingresado un comntario?
            if($data->comment != "")
            {
                //Previamente no existia comentario por lo que se debe crear
                $comment = Comment::create([
                    'description' => $data->comment,
                ]);
                //Solo necesitaremos su id
                $comment = $comment->id;
            }
        }
        return $comment;
    }

    public function store(Request $data, $week = null)
    {
     
        if($data->quantity == 0.5 || $data->quantity == 1 || $data->quantity == 1.5 || $data->quantity == 2)
        {
            //Obtenemos la semana actual o la especificada
            $object = $this->get_actual_week($week);
            //Si es null significa que no se ha encontrado la semana
            if($object == null)
                return "¡No se ha encontrado la semana especificada!";

            $actual_week = $object["week"];
            //Obtenemos el registro de la locacion seleccionada
            $location_id = Location::where("name",$data->location)->first();
            if($location_id)
            {
                $location_id = $location_id->id;
                //Seleccionamos la oh a la que se le quiere agregar una asistencia
                $oh = Oh::Where("week_id", $actual_week->id)->Where("day_id", $data->day)->Where("hour_id", $data->hour)->Where("location_id",$location_id)->Where("cycle_id",session('cycle'))->first();
                
                //Staff actual
                $staff = Auth::user();
                $staff_id = $staff->id;
                //Si la agregación de asistencia no es manual
                if($data->option != 4)
                {    
                    if($oh)
                    {
                        $student = $oh->Students()->wherePivot('student_id',$data->student)->select('student.id','email','name','last_name','oh_student.reserved')->withPivot("comment_id")->first();
                        if($student)
                        {
                            //Si es agregación o edición
                            if($data->option != 3)
                            {

                                $comment = $this->comment_validation($data, $student);

                                $oh->Students()->syncWithoutDetaching([$data->student =>['staff_id' => $staff_id, 'assistance' => $data->quantity, 'comment_id' => $comment]]);
                            
                                $msj = ['¡Asistencia subida!'];
                                //Edicion
                                if($data->option == 2 )
                                    $msj = ['¡Asistencia editada!'];

                                return response()->json([
                                    'message' => $msj,
                                    'code' => 200
                                ]);
                            }
                            //Si es eliminación
                            else
                            {
                                $today = Carbon\Carbon::now();
                                $comment = $this->comment_validation($data, $student);
                                if($student->reserved == true)
                                    $oh->Students()->syncWithoutDetaching([$data->student =>['staff_id' => $staff_id, 'assistance' => null, 'comment_id' => $comment]]);
                                else
                                    $oh->Students()->syncWithoutDetaching([$data->student =>['staff_id' => $staff_id, 'assistance' => null, 'deleted_at' => $today,  'comment_id' => $comment]]);

                                return response()->json([
                                    'message' => ['¡Asistencia removida!'],
                                    'code' => 200
                                ]);
                            }
                        }
                        else
                        {
                            return response()->json([
                                'message' => ['¡Estudiante no valido (no reservo o no existe)!'],
                                'code' => 0
                            ]);
                        }
                    }
                    return response()->json([
                        'message' => ['¡Opss, no se encontro la OH!'],
                        'code' => 0
                    ]);
                }
                //Si la agregación de asistencia es manual entonces
                else
                {
                    $student = Student::find($data->student);
                    if($student)
                    {
                        //Obtenermos las OH segun la semana, día y hora (son maximo 4 resultados)
                        $oh_all_options = Oh::Where("week_id", $actual_week->id)->Where("day_id", $data->day)->Where("hour_id", $data->hour)->get();
                        $reservation_exist = 0;
                        foreach($oh_all_options as $oh_option)
                        {
                            //Si retorna un resultado diferente a null significa que el alumno reservo en algun bloque de esa hora
                            $reservation_exist = $oh_option->Students()->wherePivot("student_id",$student->id)->count();
                            if($reservation_exist > 0)
                                break;
                                
                        }
                        //si $reservation_exist es igual a falso significa que el estudiante no habia reservado
                        if($reservation_exist == 0)
                        {
                            $comment = $this->comment_validation($data, $student);

                            //Verificamos si la OH ya existe
                            if($oh)
                            { 
                                //se sincroniza/asocia/agrega el registro de la OH existente con el id del studiante.
                                $oh->Students()->syncWithoutDetaching([$data->student =>['staff_id' => $staff_id, 'assistance' => $data->quantity, 'reserved' => false, 'comment_id' => $comment]]);
                            
                                return response()->json([
                                    'message' => ['¡Asistencia subida!'],
                                    'code' => 200
                                ]);
                                    
                            }
                            else
                            {
                                //Obtenemos el día 
                                $day_exist = Day::find($data->day);
                                //Obtenemos la hora
                                $hour_exist = Hour::find($data->day);
                                //Si ambos datos existen entonces creamos una nueva OH y la asignamos al estudiante.
                                if($day_exist && $hour_exist)
                                {

                                    $oh = Oh::create([
                                        'week_id'=>$actual_week->id, 
                                        'day_id'=> $data->day,
                                        'hour_id'=>$data->hour,
                                        'location_id'=>$location_id,
                                        'cycle_id'=>session("cycle"),
                                    ]);
                                    //Asignamos la OH con el estudiante
                                    $oh->Students()->syncWithoutDetaching([$data->student =>['staff_id' => $staff_id, 'assistance' => $data->quantity, 'reserved' => false, 'comment_id' => $comment]]);
                                    
                                    return response()->json([
                                        'message' => ['¡Asistencia subida!'],
                                        'code' => 200
                                    ]);
                                }
                                //Si alguno de estos datos no existe es porque nos enviaron desde la vista valores alterados y enviamos msj de error
                                else
                                {
                                    return response()->json([
                                        'message' => ['Error, el día o la hora es incorrecta'],
                                        'code' => 0
                                    ]);
                                }
                                
                            }
                        }
                        else
                        {
                            return response()->json([
                                'message' => ['El estudiante si reservo, ¡Búscalo bien!'],
                                'code' => 0
                            ]);
                        }
                    }
                    else
                    {
                        return response()->json([
                            'message' => ['¡Estudiante no valido (no existe)!'],
                            'code' => 0
                        ]);
                    }
                }
            }
            else
            {
                return response()->json([
                    'message' => ['Error, locación incorrecta'],
                    'code' => 0
                ]);
            }
        }
        else 
        {
            return response()->json([
                'message' => ['¡La cantidad de horas no es valida!'],
                'code' => 0
            ]);
        }
    }
}
