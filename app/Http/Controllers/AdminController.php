<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cycle;
use App\Oh;
use App\Location;
use Carbon;
use App\Student;
use App\Date;
use App\Day;
use App\Hour;
use App\Week;
use Auth;
use Caffeinated\Shinobi\Models\Role;

class AdminController extends Controller
{
    public function students()
    {
        $role = Role::where('slug', session("student_role"))->first();
        $students = [];
        if($role)
        {
            $students = $students = $role->users()
            ->select('student.id',"code","email","name","last_name","gender","birthdate",
            "address","phone","level","career","educational_center","shirt_size","state",
            "coming","student.created_at","group")->get();
        }
        return view("admin.students",compact("students"));
    }
    public function ohs_student($id)
    {
        //Buscamos al estudiante
        $student = Student::find($id);
        //Obtenemos todas las OH del estudiante
        $ohs = $student->Ohs()->get();
        
        $data = [];
        $days = ['Mo'=>'Lunes', 'Tu'=>'Martes', 'We'=>'Miércoles', 'Th'=>'Jueves', 'Fr'=>'Viernes', 'Sa'=>'Sábado'];
        $locations = ['LT'=>'Laboratorio terminales', 'LL'=>'Laboratorio laptop personal', 'AL'=>'Aula laptop CS50X.ni', 'ALP'=>'Aula laptop personal'];
        $total_hours = 0;
        //Recorremos cada una de las OH del estudiante
        foreach($ohs as $key=>$oh)
        {
            //Obtenemos el nombre de la semana en la OH y le quitamos la primer letra
            $data[$key]['week'] = substr($oh->Week()->first()->name,1);
            //Obtenemos el nombre del día en la OH utilizando el arreglo $days que posee los nombres completos del día
            $data[$key]['day'] = $days[$oh->Day()->first()->name];
            //Obtenemos el nombre de la hora en la OH.
            $data[$key]['hour'] = $oh->Hour()->first()->name;
            //Obtenemos la locación de la OH.
            $data[$key]['location'] = $locations[$oh->Location()->first()->name];

            //Obtenemos los datos de la tabla pivote de cada OH con el alumno.
            $oh_student = $oh->Students()->where("student_id",$student->id)->withPivot("reserved","assistance","staff_id")->first();
            //


            if($oh_student->pivot->assistance <= 0)
                $data[$key]['assistance'] = "No se presento";
            else
            {
                $data[$key]['assistance'] = $oh_student->pivot->assistance;
                $total_hours += $oh_student->pivot->assistance;
            }

            if($oh_student->pivot->reserved)
                $data[$key]['reserved'] = "Sí";
            else
                $data[$key]['reserved'] = "No";

            //$data[$key]["staff"] = Student::find($oh_student->pivot->staff_id)->code;
            
            $data[$key]["staff"] = "-";
            $temp = Student::find($oh_student->pivot->staff_id);
            if($temp)
                $data[$key]["staff"] = substr($temp->code, 6);

        }
        return view("admin.oh", compact("data", "student", "total_hours"));
    }

    public function class_student($id)
    {
        $student = Student::find($id);

        $lessons = $student->Lessons()->get();
        $data = [];
        $cycle = Cycle::find(session("cycle"));
        if(!$cycle)
            return "Opps.. ocurrio un error";

        $lessons = $cycle->Lessons()->get();
        
        //return $days['Mo'];
        $justifications = 0;
        $total_class = 0;
        foreach($lessons as $key=>$class)
        {
            $data_class = $class->Students()->wherePivot("student_id", $student->id)->withPivot("justified","class_group","staff_id","created_at")->first();
            
            $data[$key]['name'] = $class->name;
            $data[$key]['assistance'] = "No";
            $data[$key]["group"] = "-";
            $data[$key]["staff"] = "-";
            $data[$key]["date"] = "-";
            $temp = "";
            if($data_class)
            {   

                if($data_class->pivot->justified)
                {
                    $data[$key]['assistance'] = "Justificado";
                    $justifications++;
                }
                else
                {
                    $data[$key]['assistance'] = "Asistio";
                    $total_class++;
                }

                
                $temp = $data_class->pivot->class_group;
                if($temp != "")
                    $data[$key]["group"] = $temp;

                $temp = Student::find($data_class->pivot->staff_id);
                if($temp)
                    $data[$key]["staff"] = substr($temp->code, 6);

                
                $temp = $data_class->pivot->created_at;
                if($temp != "")
                    $data[$key]["date"] = $temp;
            }
        }
        return view("admin.class", compact("data", "student", "total_class", "justifications"));
    }

    public function ohs()
    {
        $ohs = Oh::all();
        $student = [];
        foreach($ohs as $key => $oh)
        {
            $students[$key] = $oh->Students()->Select("assistance","staff_id","reserved","comment_id")->get();
        }
        dd($students);
    }

    public function lessons()
    {

    }

    public function sumary()
    {
        //Ciclo actual
        $cycle = Cycle::find(session("cycle"));
        //Todas las clases
        $lessons = $cycle->Lessons()->get();
        $data = [];
        //Recorremos cada una de las clases
        foreach($lessons as $key => $class)
        {
            //variable que contra el total de la asistencia
            $total = 0;
            //Obtenemos el nombre de la clase
            $data[$key]["name"] = $class->name;
            //Obtenemos las cantidades de alumnos por clases.
            $total += $data[$key]["group_a"] = $class->Students()->wherePivot("class_group","A")->count(); 
            $total += $data[$key]["group_b"] = $class->Students()->wherePivot("class_group","B")->count(); 
            $total += $data[$key]["group_c"] = $class->Students()->wherePivot("class_group","C")->count(); 
            $total += $data[$key]["group_d"] = $class->Students()->wherePivot("class_group","D")->count(); 
            $total += $data[$key]["group_e"] = $class->Students()->wherePivot("class_group","E")->count(); 
            $total += $data[$key]["group_f"] = $class->Students()->wherePivot("class_group","F")->count(); 
            $total += $data[$key]["group_g"] = $class->Students()->wherePivot("class_group","G")->count(); 
            $total += $data[$key]["group_h"] = $class->Students()->wherePivot("class_group","H")->count(); 
            $total += $data[$key]["group_i"] = $class->Students()->wherePivot("class_group","I")->count(); 
            $total += $data[$key]["group_j"] = $class->Students()->wherePivot("class_group","J")->count(); 
            $total += $data[$key]["group_x"] = $class->Students()->wherePivot("class_group", null)->count(); 
            $data[$key]["total"] = $total;
            $active_cycle = session("cycle");
            // Y20C1
            if($active_cycle == 20)
            {
                $good = 200;
                $mid = 100;
            }
            // Y21C1
            if($active_cycle == 22)
            {
                $good = 95;
                $mid = 60;
            }
            if($total > $good)
                $data[$key]["color"] = "#b5f9b4"; 
            else if($total > $mid)
                $data[$key]["color"] = "#f9f7b4";
            else
                $data[$key]["color"] = "#f9b4b4";  
        }

        //$ohs = $cycle->Ohs()->get();
        $weeks = Week::all();
        $assistances = [];
        $reservations = [];

        foreach($weeks as $week)
        {
            $ohs = $week->Ohs()->where("cycle_id",session('cycle'))->get();
            foreach($ohs as $key => $oh)
            {
                $info_reservations = $oh->Students()->withPivot("assistance","reserved")->get();
                foreach($info_reservations as $re)
                {
                    $index = $week->name;
                    if(array_key_exists($index, $assistances))
                    {
                        if($re->pivot->assistance > 0)
                            $assistances[$index] += $re->pivot->assistance;
                    }
                    else
                    {
                        $assistances[$index] = 0;
                        if($re->pivot->assistance > 0)
                            $assistances[$index] = $re->pivot->assistance;
                    }
                    if(array_key_exists($index, $reservations))
                    {
                        if($re->pivot->reserved)
                            $reservations[$index] += 2;
                    }
                    else
                    {
                        $reservations[$index] = 0;
                        if($re->pivot->reserved)
                            $reservations[$index] = 2;
                    }
                }   
            }
        }
        return view("admin.sumary", compact("data","assistances","reservations"));
    }

    public function chart_class_week()
    {

        
        return response()->json([
            "state"=>true,
            "data"=>$data,
            "name"=> ["las semanas"],
            "title"=>"Asistencia a clases"
        ]);
    }

    public function ohs_duplicated()
    {
        //Todas las OH con sus respectivos estudiantes
        $ohs = OH::with("Students")->get();
        $duplicated = [];
        $count = 0;
        //Recorremos cada OH
        foreach($ohs as $oh)
        {
            //asignamos la coleccion de estudiantes que posea la actual OH
            $students = $oh->Students;
            //Recorremos cada estudiantes
            foreach($students as $index => $student)
            {
                //Tratamos de obtener las 4 posiblemos OH por Hora, para ello se ignora la locacion en la consulta
                $result = Oh::where("week_id",$oh->week_id)->where("day_id",$oh->day_id)->where("hour_id",$oh->hour_id)->get();
                //Recorremos las Ohs devueltas que fueron almacenadas en result (puede ser entre 0 a 4 registros)
                foreach($result as $re)
                {
                    //Tratamos de obtener los datos del estudiante actual mediante la OH
                    $data = $re->Students()->wherePivot("student_id", $student->id)->get();
                    $quantity = count($data);
                    //Si es 1, existe
                    if($quantity == 1)
                    {
                        if($count > 0)
                        {
                            $today = Carbon\Carbon::now();
                            $re->Students()->syncWithoutDetaching([$student->id =>['deleted_at' => $today]]);
                        }
                        $count++;
                    }
                    //Si es mayor a 1 es porque hay una grave duplicidad
                    else if($quantity > 1)
                    {
                        $count += $quantity;
                    }
                    
                }
                /* if($index == 3)
                {
                    $x = Student::find($student->id)->Ohs()->get();
                    foreach($x as $y)
                    {
                        echo $y."<br>";
                    } 
                    return $count;
                } */
                
                    /* $x = Student::find(873)->Ohs()->get();
                    foreach($x as $y)
                    {
                        echo $y."<br>";
                    } */
                array_push($duplicated, $count);
                $count = 0;
            }
        }
        return "Diosito pliz, elimina los duplicados correctamente!";
    }
}
