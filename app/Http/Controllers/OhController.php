<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Day;
use App\Hour;
use App\Week;
use App\Cycle;
use App\Oh;
use Caffeinated\Shinobi\Models\Role;
use Auth;
use Carbon;

class OhController extends Controller
{
    
    public function get_actual_week()
    {
        //Obtenemos la fecha del momento
        $today = Carbon\Carbon::now();
        //Con formato full_year-month-day
        $today = strtotime($today);
        //Obtenemos todas las fechas establecidas del ciclo y20c1
        $dates = Cycle::find(session("cycle"))->Dates()->get();
        $actual_week = null;
        $open_week = "";
        $start_week = "";
        $close_week = "";
        //Recorremos todas las fechas para poder identificar en cual estamos actualmente
        foreach($dates as $date)
        {
            $week["open"] = strtotime($date->open);
            $week["close"] = strtotime($date->close);
            //Cuando la fecha de hoy se encuentra en un rango de fechas es porque se ha encontrado la semana actual del ciclo
            if(($today >= $week["open"]) && ($today <= $week["close"]))
            {
                //Almacenamos la semana actual
                $week["week"] = $date->Weeks()->first();
                $week["start"] = strtotime($date->start);
                break;
            }
        }
        return $week;
    }

    public function oh_list($id = null)
    {
        //Obtenemos el ciclo y20c1
        $cycle = Cycle::find(session("cycle"));

        $object = $this->get_actual_week();
        $actual_week = $object["week"];
        $ohs = $cycle->Ohs()->Where("week_id", $actual_week->id)->get();
        return $ohs;



        //Si id es igual a null significa que no es una consulta ajax
        if($id == null)
            $oh = Oh::where("name", "C01")->first();
        else
            $oh = Oh::find($id);

        $ohs = [];
        //Si el ciclo y la primer clase existen
        //Obtenemos la primer clase del ciclo.
        if($cycle)
        {
            if($id == null)
                $oh = $cycle->Ohs()->first();
            else
                $oh = Oh::find($id);
            $ohs = $cycle->Ohs()->get();
        }
        else
        {
            return response()->json([
                'code' => 0,
                'message' => "¡La clase o ciclo no es valido!",
            ]);
        }

        $assistances = [];
        //$class_name = [];
        $all_assistances = [];
        
        $oh_options = Oh::where("week_id",$oh->week_id)->where("day_id",$oh->day_id)->where("hour_id",$oh->hour_id)->get();
        foreach($oh_options as $key => $option)
        {
            $all_assistances[$key] = $option->Students()->select("student.id as student", "email", "name", "last_name")
                                    ->withPivot("assistance","reserved","comment","staff_id","id")
                                    ->wherePivot('deleted_at',null)->get();
        }
        dd($all_assistances);
        
        if($id == null)
            return view("class.step_class", compact('all_assistances','ohs'));
        else
        {
            return view("class.template.class-table", compact('all_assistances','ohs'));
        }
    }
}
