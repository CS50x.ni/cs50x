<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Week;

class WeekController extends Controller
{
    public function show($id = null)
    {
        $week = Week::find($id);
        if(!$week)
        {
            return response()->json([
                'code' => 0,
            ]);
        }
        $name = $week["name"];
        $date = $week->Dates()->first();
        $start = date("d/m/Y", strtotime($date->start));
        $close = date("d/m/Y", strtotime($date->close));
        return response()->json([
            'name' => $name,
            'start' => $start,
            'close' => $close,
            'code' => 200,
        ]);
    }
}
