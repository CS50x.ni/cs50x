<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $table='student';
    public $primaryKey='id';
    
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function Cycles()
    {
        return $this->belongsToMany('App\Cycle', 'cycle_student', 'student_id', 'cycle_id');
    }
    public function OHs()
    {
        return $this->belongsToMany('App\Oh', 'oh_student', 'student_id', 'oh_id');
    }
    
}
