<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Caffeinated\Shinobi\Concerns\HasRolesAndPermissions;
use Caffeinated\Shinobi\Traits\ShinobiTrait;

class Student extends Authenticatable
{
    use SoftDeletes;
    use HasRolesAndPermissions;

    protected $table='student';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'deleted_at', 'id','code', 'email', 'password', 'name', 'last_name', 'birthdate', 
        'address', 'phone', 'level', 'career', 'educarional_center', 'shirt_size', 'state', 
        'coming', 'user_id','group', 'turn', 'github', 'dni'
    ];

    public function Cycles()
    {
        return $this->belongsToMany('App\Cycle', 'cycle_student', 'student_id', 'cycle_id');
    }
    public function OHs()
    {
        return $this->belongsToMany('App\Oh', 'oh_student', 'student_id', 'oh_id')->withTimestamps()->WherePivot('deleted_at',null);
    }
    public function Lessons()
    {
        return $this->belongsToMany('App\ClassAssistance', 'class_student', 'student_id', 'class_id')->withTimestamps()->WherePivot('deleted_at',null);
    }
}
