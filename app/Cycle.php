<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Cycle extends Model
{
    use SoftDeletes;
    protected $table='cycle';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
       'id','code', 'email', 'name', 'last_name', 'birthdate', 'address', 'phone', 'level', 'career', 'educarional_center', 'shirt_size', 'state', 'coming', 'user_id'
    ];

    public function Students()
    {
        return $this->belongsToMany('App\Student', 'cycle_student', 'cycle_id', 'student_id');
    }
    public function Dates()
    {
        return $this->hasMany('App\Date', 'cycle_id', 'id');
    }
    public function Lessons()
    {
        return $this->hasMany('App\ClassAssistance', 'cycle_id', 'id');
    }
    public function Ohs()
    {
        return $this->hasMany('App\Oh', 'cycle_id');
    }
}
