<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Oh extends Model
{
    
    use SoftDeletes;

    protected $table='oh';
    public $primaryKey = 'id';
    public $incrementing = true;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'week_id', 'day_id', 'hour_id', 'location_id','cycle_id',
    ];
    
    public function Students()
    {
        return $this->belongsToMany('App\Student', 'oh_student', 'oh_id', 'student_id')->withTimestamps()->WherePivot('deleted_at',null);
    }
    
    public function Week()
    {
        return $this->belongsTo('App\Week', 'week_id');
    }

    public function Day()
    {
        return $this->belongsTo('App\Day', 'day_id');
    }

    public function Hour()
    {
        return $this->belongsTo('App\Hour', 'hour_id');
    }

    public function Location()
    {
        return $this->belongsTo('App\Location', 'location_id');
    }
}
