<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use Caffeinated\Shinobi\Models\Permission;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Obtenemos el rol del estudiante al que necesitamos agregar permisos
        $role = Role::where("slug","y21c1estudent")->first();
        $perissions = Permission::where("slug","like",'%reservation%')->get();

        if($role)
        {
            foreach($perissions as $permission)
            {
                $role->givePermissionTo($permission->slug);
            }
        }

        // Obtenemos el rol del staff al que necesitamos agregar permisos
        $role = Role::where("slug","y21c1staff")->first();
        $perissions = Permission::where("slug","like",'%assistance%')->get();

        if($role)
        {
            foreach($perissions as $permission) 
            {
                $role->givePermissionTo($permission->slug);
            }
        }
    }
}
