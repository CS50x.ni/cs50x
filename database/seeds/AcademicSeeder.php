<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;
use Caffeinated\Shinobi\Models\Role;
use App\Student;

class AcademicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [];
        
        $permissions[0] = Permission::create([
            'name' => 'Ver asistencia clases',
            'slug' => 'assistance_class.index',
            'description' => 'Ver todas las asistencias de clases',
        ]);

        $permissions[1] = Permission::create([
            'name' => 'Ver detalle de asistencia clase',
            'slug' => 'assistance_class.show',
            'description' => 'Ver el detalle de las asistenciasa de clase',
        ]);
        $permissions[2] = Permission::create([
            'name' => 'Agregar asistencias clase',
            'slug' => 'assistance_class.create',
            'description' => 'Agregar registros a la asistencia de clase',
        ]);

        $permissions[3] = Permission::create([
            'name' => 'Edición asistencia clase',
            'slug' => 'assistance_class.edit',
            'description' => 'Editar los registros de las asistencias de clase',
        ]);

        $permissions[4] = Permission::create([
            'name' => 'Eliminar asistencia clase',
            'slug' => 'assistance_class.destroy',
            'description' => 'Eliminar registros de las asistencias de clase',
        ]);

        $academict = Role::create([
            'name'=>'Academic',
            'slug'=>'y20c1academic',
            'description'=>'Role para el equipo de academic del ciclo Y20C1'
        ]);

        for($i=0; $i < count($permissions); $i++)
        {
            $academict->givePermissionTo($permissions[$i]->slug);
        }

        $academic_list_id = [
            1562,
            1522,
            1512,
            1472,
            1442,
            1422,
            1412,
            1402,
            1392,
            1372,
            1362,
            1342,
            1332,
            1322,
            1302,
            1282,
            1272,
            1262,
            1182,
            1132,
            1122,
            1112,
            1102,
            1062,
            1042
        ];

        for($i=0; $i < count($academic_list_id); $i++)
        {
            $staff = Student::find($academic_list_id[$i]);
            $staff->assignRoles('y20c1academic');
        }
    }
}
