<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;
use App\Student;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name'=>'Admin',
            'slug'=>'admin',
            'description'=>'Role para el equipo de coordinacion de CS50X.ni',
            'special'=>'all-access'
        ]);
        
        Role::create([
            'name'=>'Estudiante',
            'slug'=>'y20c1student',
            'description'=>'Role para todos los estudiantes del ciclo Y20C1'
        ]);
        
        Role::create([
            'name'=>'Staff',
            'slug'=>'y20c1staff',
            'description'=>'Role para todo el staff del ciclo Y20C1'
        ]);

        $coordinator = Student::where('code','silfdv')->first();
        $coordinator->assignRoles('admin');
        $coordinator = Student::where('code','kavv')->first();
        $coordinator->assignRoles('admin');

        //Aqui falta especificar el ciclo
        $students = Student::where('code','!=','kavv')->where('code','!=','silfdv')->where('password',null)->get();

        foreach($students as $student)
        {
            $student->assignRoles('y20c1student');
        }

        $team = Student::where('code','!=','kavv')->where('code','!=','silfdv')->where('password','!=','')->get();

        foreach($team as $staff)
        {
            $staff->assignRoles('y20c1staff');
        }
    }
}
