<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Reservas
        Permission::create([
            'name' => 'Ver reservas',
            'slug' => 'reservation.index',
            'description' => 'Ver todas sus reservas',
        ]);
        Permission::create([
            'name' => 'Ver detalle de las reservas',
            'slug' => 'reservation.show',
            'description' => 'Ver el detalle de todas sus reservas',
        ]); 
        Permission::create([
            'name' => 'crear reservas',
            'slug' => 'reservation.create',
            'description' => 'Crear reservas',
        ]);

        Permission::create([
            'name' => 'Edición de reservas',
            'slug' => 'reservation.edit',
            'description' => 'Editar las reservas',
        ]);

        Permission::create([
            'name' => 'Eliminar reservas',
            'slug' => 'reservation.destroy',
            'description' => 'Eliminar cualquiera de sus reservas',
        ]);


        
        //Asistencia
        Permission::create([
            'name' => 'Ver asistencias',
            'slug' => 'assistance.index',
            'description' => 'Ver todas las asistencia de los estudiantes',
        ]);
        Permission::create([
            'name' => 'Ver detalle de las asistencias',
            'slug' => 'assistance.show',
            'description' => 'Ver el detalle de todas las asistencias',
        ]);

        Permission::create([
            'name' => 'crear asistencias',
            'slug' => 'assistance.create',
            'description' => 'Agregar asistencias',
        ]);

        Permission::create([
            'name' => 'Edición de asistencias',
            'slug' => 'assistance.edit',
            'description' => 'Editar las asistencias',
        ]);

        Permission::create([
            'name' => 'Eliminar asistencias',
            'slug' => 'assistance.destroy',
            'description' => 'Eliminar cualquiera de las asistencias',
        ]);

        //Roles
        Permission::create([
            'name' => 'Ver roles',
            'slug' => 'role.index',
            'description' => 'Ver todas los roles de los estudiantes',
        ]);

        Permission::create([
            'name' => 'Ver detalle de las roles',
            'slug' => 'role.show',
            'description' => 'Ver el detalle de todos las roles',
        ]);
        Permission::create([
            'name' => 'crear roles',
            'slug' => 'role.create',
            'description' => 'Agregar roles',
        ]);

        Permission::create([
            'name' => 'Edición de roles',
            'slug' => 'role.edit',
            'description' => 'Editar las roles',
        ]);

        Permission::create([
            'name' => 'Eliminar roles',
            'slug' => 'role.destroy',
            'description' => 'Eliminar cualquier role',
        ]);

    }
}
