<?php

use Illuminate\Database\Seeder;
use App\ClassAssistance;
use App\Cycle;

class ClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //La nomenclatura que se busca es C01, C02, etc.
        $decena = 2;
        $unidad = 1;
        for($i = 20; $i < 34; $i++)
        {
            ClassAssistance::create([
                'name' => "C".$decena.$unidad,
            ]);
            $unidad++;
            if($unidad == 10)
            {
                $unidad = 0;
                $decena++;
            }
        }
        $classes = ClassAssistance::all();
        foreach($classes as $class)
        {
            $class->Cycles()->syncWithoutDetaching(20);
        }
    }
}
