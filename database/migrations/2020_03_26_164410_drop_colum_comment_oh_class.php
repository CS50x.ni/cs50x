<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumCommentOhClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oh_student', function (Blueprint $table) {
            $table->dropColumn("comment");
        });
        Schema::table('class_student', function (Blueprint $table) {
            $table->dropColumn("comment");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oh_student', function (Blueprint $table) {
            //
        });
    }
}
