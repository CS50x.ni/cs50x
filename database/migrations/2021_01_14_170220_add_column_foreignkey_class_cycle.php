<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnForeignkeyClassCycle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Para el funcionamiento dinamico entre varios ciclos la relacion mucho a mucho no funciona
        // Por lo que se debe remover y agregar el comportamiento de muchos a uno
        /* Schema::table('class_cycle', function (Blueprint $table) {
            $table->dropForeign('class_cycle_class_id_foreign');
            $table->dropForeign('class_cycle_cycle_id_foreign');
        }); */

        Schema::table('class', function (Blueprint $table) {
            $table->integer('cycle_id')->nullable();
            $table->foreign('cycle_id')->references('id')->on('cycle');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
