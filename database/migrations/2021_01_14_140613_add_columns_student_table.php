<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student', function (Blueprint $table) {
            $table->string('group', 3)->nullable();
            $table->string('turn', 25)->nullable();
            $table->string('github', 30)->nullable();
            $table->string('dni', 30)->nullable();
            $table->string('career', 65)->default('N/A')->change();
            $table->string('educational_center', 50)->default('N/A')->change();
            $table->string('level', 30)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
